!classDefinition: #MarsRoverObserversTest category: 'MarsRover-Logger'!
TestCase subclass: #MarsRoverObserversTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-Logger'!

!MarsRoverObserversTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/30/2022 21:22:26'!
test01EmptyCommandsLogsNothing

	| marsRover logger |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	logger := MarsRoverLogger followPositionChangesOn: marsRover.
	
	marsRover process: ''.
	
	self assert: logger readStream atEnd.
! !

!MarsRoverObserversTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/30/2022 21:24:42'!
test02ForwardHeadingNorthLogs

	| marsRover logger logStream |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	logger := MarsRoverLogger followPositionChangesOn: marsRover.
	
	marsRover process: 'f'.
	
	logStream := logger readStream.
	self assert: '1@2' equals: logStream nextLine.
	self assert: logStream atEnd.
! !

!MarsRoverObserversTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/30/2022 21:24:51'!
test03BackwardHeadingNorthLogs

	| marsRover logger logStream |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	logger := MarsRoverLogger followPositionChangesOn: marsRover.
	
	marsRover process: 'b'.
	
	logStream := logger readStream.
	self assert: '1@0' equals: logStream nextLine.
	self assert: logStream atEnd.
! !

!MarsRoverObserversTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/30/2022 21:25:01'!
test04MultipleForwardBackwardHeadingEastLog

	| marsRover logger logStream |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingEast.
	logger := MarsRoverLogger followPositionChangesOn: marsRover.
	
	marsRover process: 'fb'.
	
	logStream := logger readStream.
	self assert: '2@1' equals: logStream nextLine.
	self assert: '1@1' equals: logStream nextLine.
	self assert: logStream atEnd.
! !

!MarsRoverObserversTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/30/2022 21:30:59'!
test05HeadingNorthTurnRightLogsCorrectly

	| marsRover logger logStream |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	logger := MarsRoverLogger followHeadingChangesOn: marsRover.
	
	marsRover process: 'r'.
	
	logStream := logger readStream.
	self assert: 'East' equals: logStream nextLine.
	self assert: logStream atEnd.
! !

!MarsRoverObserversTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/30/2022 21:46:29'!
test06HeadingEastTurnRightLogsCorrectly

	| marsRover logger logStream |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingEast.
	logger := MarsRoverLogger followHeadingChangesOn: marsRover.
	
	marsRover process: 'r'.
	
	logStream := logger readStream.
	self assert: 'South' equals: logStream nextLine.
	self assert: logStream atEnd.
! !

!MarsRoverObserversTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/30/2022 21:47:00'!
test07HeadingSouthTurnRightLogsCorrectly

	| marsRover logger logStream |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingSouth.
	logger := MarsRoverLogger followHeadingChangesOn: marsRover.
	
	marsRover process: 'r'.
	
	logStream := logger readStream.
	self assert: 'West' equals: logStream nextLine.
	self assert: logStream atEnd.
! !

!MarsRoverObserversTest methodsFor: 'as yet unclassified' stamp: 'iniesz 5/30/2022 21:49:30'!
test08HeadingWestTurnRightLogsCorrectly

	| marsRover logger logStream |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingWest.
	logger := MarsRoverLogger followHeadingChangesOn: marsRover.
	
	marsRover process: 'r'.
	
	logStream := logger readStream.
	self assert: 'North' equals: logStream nextLine.
	self assert: logStream atEnd.
! !

!MarsRoverObserversTest methodsFor: 'as yet unclassified' stamp: 'juan 6/1/2022 22:40:29'!
test09HeadingNorthMoveForwardAndTurnRightLogsCorrectly

	| marsRover logger1 logger2 logStream1 logStream2 |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	logger1 := MarsRoverLogger followHeadingChangesOn: marsRover.
	logger2 := MarsRoverLogger followPositionChangesOn: marsRover.
	
	marsRover process: 'rf'.
	
	logStream1 := logger1 readStream.
	logStream2 := logger2 readStream.
	self assert: 'East' equals: logStream1 nextLine.
	self assert: '2@1' equals: logStream2 nextLine.
	self assert: logStream1 atEnd.
	self assert: logStream2 atEnd.! !

!MarsRoverObserversTest methodsFor: 'as yet unclassified' stamp: 'ddo 6/2/2022 15:52:35'!
test10MarsRoverAtPosition11HeadingNorthHasCorrectWindowStatus

	| marsRover window |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	
	window := MarsRoverWindowStatus followPositionChangesOn: marsRover.
	
	marsRover process: 'fr'.
	
	self assert: 1@2 equals: window positionStatus.

	! !

!MarsRoverObserversTest methodsFor: 'as yet unclassified' stamp: 'ddo 6/2/2022 15:52:35'!
test11MarsRoverAtPosition11HeadingNorthHasCorrectWindowHeadingStatus

	| marsRover window |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	
	window := MarsRoverWindowStatus followHeadingChangesOn: marsRover.
	
	marsRover process: 'r'.
	
	self assert: 'East' equals: window headingStatus.

	! !


!classDefinition: #MarsRoverTest category: 'MarsRover-Logger'!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-Logger'!

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:21:23'!
test01DoesNotMoveWhenNoCommand

	self 
		assertIsAt: 1@2 
		heading: self north 
		afterProcessing: '' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:12'!
test02IsAtFailsForDifferentPosition

	| marsRover |
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	self deny: (marsRover isAt: 1@2 heading: self north)! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:31'!
test03IsAtFailsForDifferentHeading

	| marsRover |
	
	marsRover := MarsRover at: 1@1 heading: self north . 
	
	self deny: (marsRover isAt: 1@1 heading: self south)! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:30:17'!
test04IncrementsYAfterMovingForwardWhenHeadingNorth

	self 
		assertIsAt: 1@3 
		heading: self north 
		afterProcessing: 'f' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:30:11'!
test06DecrementsYAfterMovingBackwardsWhenHeadingNorth

	self 
		assertIsAt: 1@1 
		heading: self north 
		afterProcessing: 'b' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:59'!
test07PointToEashAfterRotatingRightWhenHeadingNorth

	self 
		assertIsAt: 1@2 
		heading: self east 
		afterProcessing: 'r' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:51'!
test08PointsToWestAfterRotatingLeftWhenPointingNorth

	self 
		assertIsAt: 1@2 
		heading: self west 
		afterProcessing: 'l' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:45'!
test09DoesNotProcessInvalidCommand

	| marsRover |
	
	marsRover := MarsRover at: 1@2 heading: self north.
	
	self 
		should: [ marsRover process: 'x' ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: marsRover invalidCommandErrorDescription.
			self assert: (marsRover isAt: 1@2 heading: self north) ]! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:39'!
test10CanProcessMoreThanOneCommand

	self 
		assertIsAt: 1@4 
		heading: self north 
		afterProcessing: 'ff' 
		whenStartingAt: 1@2 
		heading: self north 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:31'!
test11IncrementsXAfterMovingForwareWhenHeadingEast

	self 
		assertIsAt: 2@2 
		heading: self east 
		afterProcessing: 'f' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:19'!
test12DecrementsXAfterMovingBackwardWhenHeadingEast

	self 
		assertIsAt: 0@2 
		heading: self east 
		afterProcessing: 'b' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:14'!
test13PointsToSouthAfterRotatingRightWhenHeadingEast

		self 
		assertIsAt: 1@2 
		heading: self south 
		afterProcessing: 'r' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:05'!
test14PointsToNorthAfterRotatingLeftWhenPointingEast

		self 
		assertIsAt: 1@2 
		heading: self north 
		afterProcessing: 'l' 
		whenStartingAt: 1@2 
		heading: self east 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:29:00'!
test15ForwardBackwardsAndRotateRightWorkAsExpectedWhenPointingSouth

	self 
		assertIsAt: 1@1 
		heading: self west 
		afterProcessing: 'ffblrr' 
		whenStartingAt: 1@2 
		heading: self south 
! !

!MarsRoverTest methodsFor: 'tests' stamp: 'HAW 10/7/2021 20:28:52'!
test16ForwardBackwardsAndRotateRightWorkAsExpectedWhenPointingWest

	self 
		assertIsAt: 0@2 
		heading: self north 
		afterProcessing: 'ffblrr' 
		whenStartingAt: 1@2 
		heading: self west 
! !


!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:31'!
east

	^ MarsRoverHeadingEast ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:38'!
north

	^ MarsRoverHeadingNorth ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:45'!
south

	^ MarsRoverHeadingSouth ! !

!MarsRoverTest methodsFor: 'headings' stamp: 'HAW 10/7/2021 20:09:54'!
west

	^ MarsRoverHeadingWest ! !


!MarsRoverTest methodsFor: 'assertions' stamp: 'HAW 10/7/2021 20:20:47'!
assertIsAt: newPosition heading: newHeadingType afterProcessing: commands whenStartingAt: startPosition heading: startHeadingType

	| marsRover |
	
	marsRover := MarsRover at: startPosition heading: startHeadingType. 
	
	marsRover process: commands.
	
	self assert: (marsRover isAt: newPosition heading: newHeadingType)! !


!classDefinition: #MarsRoverWindowTest category: 'MarsRover-Logger'!
TestCase subclass: #MarsRoverWindowTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-Logger'!

!MarsRoverWindowTest methodsFor: 'as yet unclassified' stamp: 'juan 6/1/2022 16:16:36'!
test01EmptyCommandEmptyWindow

	|marsRover window windowsStream |
	
	marsRover := MarsRover at: 1@1 heading: MarsRoverHeadingNorth.
	window := MarsRoverWindow followPositionChangesOn: marsRover.
	
	marsRover process:''.
	
	windowsStream := window readStream.
	
	self assert: '' equals: windowsStream nextLine.
	self assert: '' equals: windowsStream nextLine.
	self assert: windowsStream atEnd.
! !


!classDefinition: #MarsRover category: 'MarsRover-Logger'!
Object subclass: #MarsRover
	instanceVariableNames: 'position head headingLogger positionObservers headingObservers'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-Logger'!

!MarsRover methodsFor: 'exceptions' stamp: 'HAW 6/30/2018 19:48:45'!
invalidCommandErrorDescription
	
	^'Invalid command'! !

!MarsRover methodsFor: 'exceptions' stamp: 'HAW 6/30/2018 19:50:26'!
signalInvalidCommand
	
	self error: self invalidCommandErrorDescription ! !


!MarsRover methodsFor: 'initialization' stamp: 'ddo 6/2/2022 16:45:40'!
initializeAt: aPosition heading: aHeadingType

	position := aPosition.
	head := aHeadingType for: self.
	positionObservers := OrderedCollection new.
	headingObservers := OrderedCollection new.! !


!MarsRover methodsFor: 'heading' stamp: 'ddo 6/2/2022 17:00:23'!
headEast
	
	self headTo: (MarsRoverHeadingEast for: self)
	! !

!MarsRover methodsFor: 'heading' stamp: 'ddo 6/2/2022 17:01:00'!
headNorth
	
	self headTo: (MarsRoverHeadingNorth for: self)! !

!MarsRover methodsFor: 'heading' stamp: 'ddo 6/2/2022 17:01:19'!
headSouth
	
	self headTo:(MarsRoverHeadingSouth for: self)! !

!MarsRover methodsFor: 'heading' stamp: 'ddo 6/2/2022 16:59:55'!
headTo: aHeadToRotate
	
	head := aHeadToRotate.
	headingObservers do:[:anObserver| anObserver headingChangedTo: head]! !

!MarsRover methodsFor: 'heading' stamp: 'ddo 6/2/2022 17:01:41'!
headWest
	
	self headTo: (MarsRoverHeadingWest for: self)! !

!MarsRover methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	head rotateLeft! !

!MarsRover methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	head rotateRight! !


!MarsRover methodsFor: 'testing' stamp: 'HAW 10/7/2021 20:16:32'!
isAt: aPosition heading: aHeadingType

	^position = aPosition and: [ head isHeading: aHeadingType ]! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:16:51'!
isBackwardCommand: aCommand

	^aCommand = $b! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:16:19'!
isForwardCommand: aCommand

	^aCommand = $f ! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:17:51'!
isRotateLeftCommand: aCommand

	^aCommand = $l! !

!MarsRover methodsFor: 'testing' stamp: 'HAW 7/6/2018 18:17:21'!
isRotateRightCommand: aCommand

	^aCommand = $r! !


!MarsRover methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	head moveBackward! !

!MarsRover methodsFor: 'moving' stamp: 'ddo 6/2/2022 17:03:08'!
moveEast
	
	self moveTo: 1@0.
	! !

!MarsRover methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	head moveForward! !

!MarsRover methodsFor: 'moving' stamp: 'ddo 6/2/2022 17:03:08'!
moveNorth
	
	self moveTo: 0@1.
	! !

!MarsRover methodsFor: 'moving' stamp: 'ddo 6/2/2022 17:03:08'!
moveSouth
	
	self moveTo: 0@-1.
	! !

!MarsRover methodsFor: 'moving' stamp: 'ddo 6/2/2022 17:03:42'!
moveTo: anOffSet

	position := position + anOffSet.
	positionObservers do:[:observer| observer positionChangedTo: position]! !

!MarsRover methodsFor: 'moving' stamp: 'ddo 6/2/2022 17:03:08'!
moveWest
	
	self moveTo: -1@0! !


!MarsRover methodsFor: 'command processing' stamp: 'HAW 6/30/2018 19:48:26'!
process: aSequenceOfCommands

	aSequenceOfCommands do: [:aCommand | self processCommand: aCommand ]
! !

!MarsRover methodsFor: 'command processing' stamp: 'HAW 8/22/2019 12:08:50'!
processCommand: aCommand

	(self isForwardCommand: aCommand) ifTrue: [ ^ self moveForward ].
	(self isBackwardCommand: aCommand) ifTrue: [ ^ self moveBackward ].
	(self isRotateRightCommand: aCommand) ifTrue: [ ^ self rotateRight ].
	(self isRotateLeftCommand: aCommand) ifTrue: [ ^ self rotateLeft ].

	self signalInvalidCommand.! !


!MarsRover methodsFor: 'as yet unclassified' stamp: 'ddo 6/2/2022 16:21:51'!
addHeadingObserver: anObserver

	headingObservers add: anObserver ! !

!MarsRover methodsFor: 'as yet unclassified' stamp: 'ddo 6/2/2022 16:18:59'!
addPositionObserver: anObserver

	positionObservers add: anObserver ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: 'MarsRover-Logger'!
MarsRover class
	instanceVariableNames: 'headings'!

!MarsRover class methodsFor: 'instance creation' stamp: 'HAW 10/7/2021 20:10:30'!
at: aPosition heading: aHeadingType
	
	^self new initializeAt: aPosition heading: aHeadingType! !


!classDefinition: #MarsRoverHeading category: 'MarsRover-Logger'!
Object subclass: #MarsRoverHeading
	instanceVariableNames: 'marsRover'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-Logger'!

!MarsRoverHeading methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft

	self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight

	self subclassResponsibility ! !


!MarsRoverHeading methodsFor: 'testing' stamp: 'HAW 10/7/2021 20:15:38'!
isHeading: aHeadingType

	^self isKindOf: aHeadingType ! !


!MarsRoverHeading methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward

	self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward

	self subclassResponsibility ! !


!MarsRoverHeading methodsFor: 'initialization' stamp: 'HAW 10/7/2021 20:11:59'!
initializeFor: aMarsRover 
	
	marsRover := aMarsRover.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverHeading class' category: 'MarsRover-Logger'!
MarsRoverHeading class
	instanceVariableNames: ''!

!MarsRoverHeading class methodsFor: 'instance creation' stamp: 'HAW 10/7/2021 20:11:35'!
for: aMarsRover 
	
	^self new initializeFor: aMarsRover ! !


!classDefinition: #MarsRoverHeadingEast category: 'MarsRover-Logger'!
MarsRoverHeading subclass: #MarsRoverHeadingEast
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-Logger'!

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveWest! !

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveEast! !


!MarsRoverHeadingEast methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headNorth! !

!MarsRoverHeadingEast methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headSouth! !


!MarsRoverHeadingEast methodsFor: 'as yet unclassified' stamp: 'ddo 6/2/2022 15:05:43'!
accept: anObserver

	^anObserver observeMarsRoverHeadingEast! !


!classDefinition: #MarsRoverHeadingNorth category: 'MarsRover-Logger'!
MarsRoverHeading subclass: #MarsRoverHeadingNorth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-Logger'!

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'ddo 6/2/2022 15:06:20'!
accept: anObserver

	^anObserver observeMarsRoverHeadingNorth.! !

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveSouth! !

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveNorth! !


!MarsRoverHeadingNorth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headWest! !

!MarsRoverHeadingNorth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headEast! !


!classDefinition: #MarsRoverHeadingSouth category: 'MarsRover-Logger'!
MarsRoverHeading subclass: #MarsRoverHeadingSouth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-Logger'!

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'ddo 6/2/2022 15:05:56'!
accept: anObserver

	^anObserver observeMarsRoverHeadingSouth.! !

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward
	
	^marsRover moveNorth! !

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveSouth! !


!MarsRoverHeadingSouth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headEast! !

!MarsRoverHeadingSouth methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headWest! !


!classDefinition: #MarsRoverHeadingWest category: 'MarsRover-Logger'!
MarsRoverHeading subclass: #MarsRoverHeadingWest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-Logger'!

!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:24'!
moveBackward

	^marsRover moveEast! !

!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'HAW 10/7/2021 20:13:53'!
moveForward
	
	^marsRover moveWest! !


!MarsRoverHeadingWest methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:20'!
rotateLeft
	
	^marsRover headSouth! !

!MarsRoverHeadingWest methodsFor: 'heading' stamp: 'HAW 10/7/2021 20:14:44'!
rotateRight
	
	^marsRover headNorth! !


!MarsRoverHeadingWest methodsFor: 'as yet unclassified' stamp: 'ddo 6/2/2022 15:14:38'!
accept: anObserver

	^anObserver observeMarsRoverHeadingWest.! !


!classDefinition: #MarsRoverObservers category: 'MarsRover-Logger'!
Object subclass: #MarsRoverObservers
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-Logger'!

!MarsRoverObservers methodsFor: 'as yet unclassified' stamp: 'ddo 6/2/2022 15:55:05'!
observerPositionChangesOf: aMarsRover 
	
	aMarsRover addPositionObserver: [ :newPosition | self positionChangedTo: newPosition ].
! !


!classDefinition: #MarsRoverLogger category: 'MarsRover-Logger'!
MarsRoverObservers subclass: #MarsRoverLogger
	instanceVariableNames: 'log'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-Logger'!

!MarsRoverLogger methodsFor: 'observe heading private' stamp: 'ddo 6/2/2022 15:05:43'!
observeMarsRoverHeadingEast 
	
	log nextPutAll: 'East'; newLine! !

!MarsRoverLogger methodsFor: 'observe heading private' stamp: 'ddo 6/2/2022 15:06:20'!
observeMarsRoverHeadingNorth 
	
	log nextPutAll: 'North'; newLine! !

!MarsRoverLogger methodsFor: 'observe heading private' stamp: 'ddo 6/2/2022 15:05:56'!
observeMarsRoverHeadingSouth
	
	log nextPutAll: 'South'; newLine! !

!MarsRoverLogger methodsFor: 'observe heading private' stamp: 'ddo 6/2/2022 15:06:04'!
observeMarsRoverHeadingWest
	
	log nextPutAll: 'West'; newLine! !


!MarsRoverLogger methodsFor: 'instance creation' stamp: 'ddo 6/2/2022 16:38:30'!
followHeadingChangesOn: aMarsRover 
	
	aMarsRover addHeadingObserver: self! !

!MarsRoverLogger methodsFor: 'instance creation' stamp: 'ddo 6/2/2022 16:38:43'!
followPositionChangesOn: aMarsRover 
	
	aMarsRover addPositionObserver: self! !

!MarsRoverLogger methodsFor: 'instance creation' stamp: 'HAW 5/30/2022 20:25:00'!
initialize

	log  := WriteStream on: ''! !


!MarsRoverLogger methodsFor: 'accessing' stamp: 'HAW 5/30/2022 20:46:00'!
headingChangedTo: aMarsRoverHeading
	
	aMarsRoverHeading accept: self! !

!MarsRoverLogger methodsFor: 'accessing' stamp: 'HAW 5/30/2022 20:22:16'!
positionChangedTo: aPosition

	log print: aPosition; newLine! !

!MarsRoverLogger methodsFor: 'accessing' stamp: 'HAW 5/30/2022 20:23:26'!
readStream
	
	^ReadStream on: log contents ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverLogger class' category: 'MarsRover-Logger'!
MarsRoverLogger class
	instanceVariableNames: ''!

!MarsRoverLogger class methodsFor: 'as yet unclassified' stamp: 'HAW 5/30/2022 20:42:45'!
followHeadingChangesOn: aMarsRover 
	
	^self new followHeadingChangesOn: aMarsRover ! !

!MarsRoverLogger class methodsFor: 'as yet unclassified' stamp: 'ddo 6/2/2022 13:26:50'!
followPositionAndHeadingChangesOn: aMarsRover 
	
	
	^ self new
		followPositionChangesOn: aMarsRover;
		followHeadingChangesOn: aMarsRover.! !

!MarsRoverLogger class methodsFor: 'as yet unclassified' stamp: 'HAW 5/30/2022 20:16:11'!
followPositionChangesOn: aMarsRover 
	
	^self new followPositionChangesOn: aMarsRover ! !


!classDefinition: #MarsRoverWindowStatus category: 'MarsRover-Logger'!
MarsRoverObservers subclass: #MarsRoverWindowStatus
	instanceVariableNames: 'marsRover position head'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-Logger'!

!MarsRoverWindowStatus methodsFor: 'instance creation' stamp: 'ddo 6/2/2022 16:39:19'!
initializeFollowHeadingChangesOn: aMarsRover 
	
	aMarsRover addHeadingObserver: self! !

!MarsRoverWindowStatus methodsFor: 'instance creation' stamp: 'ddo 6/2/2022 16:39:30'!
initializeFollowPositionChangesOn: aMarsRover 
	
	aMarsRover addPositionObserver: self! !


!MarsRoverWindowStatus methodsFor: 'accessing' stamp: 'ddo 6/2/2022 15:01:50'!
headingChangedTo: aMarsRoverHeading
	
	aMarsRoverHeading accept: self! !

!MarsRoverWindowStatus methodsFor: 'accessing' stamp: 'ddo 6/2/2022 14:45:47'!
headingStatus
	
	^head! !

!MarsRoverWindowStatus methodsFor: 'accessing' stamp: 'ddo 6/2/2022 14:33:30'!
positionChangedTo: aPosition

	position := aPosition ! !

!MarsRoverWindowStatus methodsFor: 'accessing' stamp: 'ddo 6/2/2022 14:22:14'!
positionStatus

	^position! !


!MarsRoverWindowStatus methodsFor: 'observe heading private' stamp: 'ddo 6/2/2022 15:08:41'!
observeMarsRoverHeadingEast 
	
	head:= 'East'! !

!MarsRoverWindowStatus methodsFor: 'observe heading private' stamp: 'ddo 6/2/2022 15:09:30'!
observeMarsRoverHeadingNorth 
	
	head:= 'North'! !

!MarsRoverWindowStatus methodsFor: 'observe heading private' stamp: 'ddo 6/2/2022 15:09:45'!
observeMarsRoverHeadingSouth
	
	head:= 'South'! !

!MarsRoverWindowStatus methodsFor: 'observe heading private' stamp: 'ddo 6/2/2022 15:09:58'!
observeMarsRoverHeadingWest
	
	head:= 'West'! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverWindowStatus class' category: 'MarsRover-Logger'!
MarsRoverWindowStatus class
	instanceVariableNames: ''!

!MarsRoverWindowStatus class methodsFor: 'instance creation' stamp: 'ddo 6/2/2022 14:43:14'!
followHeadingChangesOn: aMarsRover 
	
	^self new initializeFollowHeadingChangesOn: aMarsRover ! !

!MarsRoverWindowStatus class methodsFor: 'instance creation' stamp: 'ddo 6/2/2022 14:18:04'!
followPositionChangesOn: aMarsRover 

	^self new initializeFollowPositionChangesOn: aMarsRover ! !


!classDefinition: #MarsRoverWindow category: 'MarsRover-Logger'!
Object subclass: #MarsRoverWindow
	instanceVariableNames: 'window'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover-Logger'!

!MarsRoverWindow methodsFor: 'as yet unclassified' stamp: 'ddo 6/2/2022 16:42:09'!
followPositionChangesOn: aMarsRover 
	
	aMarsRover addPositionObserver: self! !

!MarsRoverWindow methodsFor: 'as yet unclassified' stamp: 'Juan 5/31/2022 18:55:50'!
initialize

	window  := WriteStream on: ''! !

!MarsRoverWindow methodsFor: 'as yet unclassified' stamp: 'Juan 5/31/2022 18:51:10'!
readStream
	
	^ReadStream on: window contents! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverWindow class' category: 'MarsRover-Logger'!
MarsRoverWindow class
	instanceVariableNames: 'window'!

!MarsRoverWindow class methodsFor: 'as yet unclassified' stamp: 'Juan 5/31/2022 18:52:43'!
followPositionChangesOn: aMarsRover 
	
	^self new followPositionChangesOn: aMarsRover ! !
