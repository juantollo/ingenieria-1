!classDefinition: #PortfolioTest category: 'Portfolio2'!
TestCase subclass: #PortfolioTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio2'!

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 11:16:26'!
test01BalanceOfPortfolioWithoutAccountsIsZero

	self assert: 0 equals: Portfolio new balance! !

!PortfolioTest methodsFor: 'tests' stamp: 'NR 5/27/2021 17:36:04'!
test02BalanceOfPortfolioWithAccountsIsSumOfAccountsBalance

	| account portfolio |
	
	account := ReceptiveAccount new.
	Deposit register: 100 on: account.
	portfolio := Portfolio with: account.
	
	self assert: account balance equals: portfolio balance! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 11:23:25'!
test03BalanceOfPortfolioIsCalculatedRecursivelyOnPortfolios

	| simplePortfolioAccount simplePortfolio composedPortfolioAccount composedPortofolio |
	
	simplePortfolioAccount := ReceptiveAccount new.
	Deposit register: 100 on: simplePortfolioAccount.
	simplePortfolio := Portfolio with: simplePortfolioAccount.
	
	composedPortfolioAccount := ReceptiveAccount new.
	Withdraw register: 50 on: composedPortfolioAccount.
	composedPortofolio := Portfolio with: simplePortfolio with: composedPortfolioAccount.
	
	self assert: (composedPortfolioAccount balance + simplePortfolio balance) equals: composedPortofolio balance! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 11:43:15'!
test04PortfolioWithoutAccountsHasNoRegisteredTransaction

	self deny: (Portfolio new hasRegistered: (Deposit for: 100))! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 11:43:11'!
test05PortfolioHasRegisteredItsAccountsTransactions

	| account portfolio deposit |
	
	account := ReceptiveAccount new.
	deposit := Deposit register: 100 on: account.
	portfolio := Portfolio with: account.
	
	self assert: (portfolio hasRegistered: deposit)! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 11:58:06'!
test06PortfolioLooksForRegisteredTransactionsRecursively

	| simplePortfolioAccount simplePortfolio composedPortfolioAccount composedPortfolio composedPortfolioAccountWithdraw simplePortfolioAccountDeposit |
	
	simplePortfolioAccount := ReceptiveAccount new.
	simplePortfolioAccountDeposit := Deposit register: 100 on: simplePortfolioAccount.
	simplePortfolio := Portfolio with: simplePortfolioAccount.
	
	composedPortfolioAccount := ReceptiveAccount new.
	composedPortfolioAccountWithdraw := Withdraw register: 50 on: composedPortfolioAccount.
	composedPortfolio := Portfolio with: simplePortfolio with: composedPortfolioAccount.
	
	self assert: (composedPortfolio hasRegistered: simplePortfolioAccountDeposit).
	self assert: (composedPortfolio hasRegistered: composedPortfolioAccountWithdraw)! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 11:58:10'!
test07PortfolioHasNoTransactionWhenHasNoAccounts

	self assert: Portfolio new transactions isEmpty! !

!PortfolioTest methodsFor: 'tests' stamp: 'NR 6/22/2020 07:31:19'!
test08PortfolioTransactionsIncludesAllItsAccountsTransactions

	| account portfolio accountDeposit anotherAccount portfolioTransactions anotherAccountWithdraw |
	
	account := ReceptiveAccount new.
	accountDeposit := Deposit register: 100 on: account.
	anotherAccount := ReceptiveAccount new.
	anotherAccountWithdraw := Withdraw register: 100 on: account.
	portfolio := Portfolio with: account.
	
	portfolioTransactions := portfolio transactions.
	
	self assert: 2 equals: portfolioTransactions size.
	self assert: (portfolioTransactions includes: accountDeposit).
	self assert: (portfolioTransactions includes: anotherAccountWithdraw)! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 11:58:20'!
test09PortfolioTransactionsAreCalculatedRecursively

	| simplePortfolioAccount simplePortfolio composedPortfolioAccount composedPortfolio composedPortfolioAccountWithdraw simplePortfolioAccountDeposit composedPortfolioTransactions |
	
	simplePortfolioAccount := ReceptiveAccount new.
	simplePortfolioAccountDeposit := Deposit register: 100 on: simplePortfolioAccount.
	simplePortfolio := Portfolio with: simplePortfolioAccount.
	
	composedPortfolioAccount := ReceptiveAccount new.
	composedPortfolioAccountWithdraw := Withdraw register: 50 on: composedPortfolioAccount.
	composedPortfolio := Portfolio with: simplePortfolio with: composedPortfolioAccount.
	
	composedPortfolioTransactions := composedPortfolio transactions.
	self assert: 2 equals: composedPortfolioTransactions size.
	self assert: (composedPortfolioTransactions includes: simplePortfolioAccountDeposit).
	self assert: (composedPortfolioTransactions includes: composedPortfolioAccountWithdraw)! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 11:58:24'!
test10PortfolioCanNotIncludeTheSameAccountMoreThanOnce

	| account portfolio |
	
	account := ReceptiveAccount new.
	portfolio := Portfolio with: account.
	
	self 
		should: [ portfolio add: account ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: Portfolio canNotAddAccountErrorMessage equals: anError messageText.
			self assert: 1 equals: portfolio accountsSize.
			self assert: (portfolio accountsIncludes: account) ]! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 11:58:28'!
test11PortfolioCanNotIncludeAccountOfItsPortfolios

	| account simplePortfolio composedPortfolio |
	
	account := ReceptiveAccount new.
	simplePortfolio := Portfolio with: account.
	composedPortfolio := Portfolio with: simplePortfolio.
	
	self 
		should: [ composedPortfolio add: account ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: Portfolio canNotAddAccountErrorMessage equals: anError messageText.
			self assert: 1 equals: composedPortfolio accountsSize.
			self assert: (composedPortfolio accountsIncludes: simplePortfolio) ]! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 11:58:32'!
test12PortfolioCanNotIncludeItself

	| account simplePortfolio |
	
	account := ReceptiveAccount new.
	simplePortfolio := Portfolio with: account.
	
	self 
		should: [ simplePortfolio add: simplePortfolio ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: Portfolio canNotAddAccountErrorMessage equals: anError messageText.
			self assert: 1 equals: simplePortfolio accountsSize.
			self assert: (simplePortfolio accountsIncludes: account) ]! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 12:01:51'!
test13ComposedPortfolioCanNotHaveParentPortfolioAccount

	| account simplePortfolio composedPortfolio |
	
	account := ReceptiveAccount new.
	simplePortfolio := Portfolio new.
	composedPortfolio := Portfolio with: simplePortfolio.
	composedPortfolio add: account.
	
	self 
		should: [ simplePortfolio add: account ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: Portfolio canNotAddAccountErrorMessage equals: anError messageText.
			self assert: simplePortfolio accountsIsEmpty ]! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/25/2019 12:12:16'!
test14ComposedPortfolioCanNotHaveAccountOfAnyRootParentRecursively

	| account leftParentPortfolio leftRootParentPortfolio portfolio rightParentPortfolio rightRootParentPortfolio |
	
	account := ReceptiveAccount new.
	portfolio := Portfolio new.
	leftParentPortfolio := Portfolio with: portfolio .
	leftRootParentPortfolio := Portfolio with: leftParentPortfolio.
	leftRootParentPortfolio add: account.
	
	rightParentPortfolio := Portfolio with: portfolio .
	rightRootParentPortfolio := Portfolio with: rightParentPortfolio.
	rightRootParentPortfolio add: account.

	self 
		should: [ portfolio add: account ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: Portfolio canNotAddAccountErrorMessage equals: anError messageText.
			self assert: portfolio accountsIsEmpty ]! !

!PortfolioTest methodsFor: 'tests' stamp: 'HAW 5/29/2019 16:31:18'!
test15PortfolioCanNotIncludeAnyOfTheComposedAccountOfPortfolioToAdd

	| portfolioToAdd portfolioToModify rootPortfolio sharedAccount |
	
	sharedAccount := ReceptiveAccount new.
	portfolioToModify := Portfolio new.
	rootPortfolio := Portfolio with: sharedAccount with: portfolioToModify.
	portfolioToAdd := Portfolio with: sharedAccount.
	
	self 
		should: [ portfolioToModify add: portfolioToAdd ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError | 
			self assert: Portfolio canNotAddAccountErrorMessage equals: anError messageText.
			self assert: portfolioToModify accountsIsEmpty ]! !


!classDefinition: #ReceptiveAccountTest category: 'Portfolio2'!
TestCase subclass: #ReceptiveAccountTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio2'!

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:19:48'!
test01ReceptiveAccountHaveZeroAsBalanceWhenCreated 

	| account |
	
	account := ReceptiveAccount new.

	self assert: 0 equals: account balance .
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:19:54'!
test02DepositIncreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount  new.
	Deposit register: 100 on: account.
		
	self assert: 100 equals: account balance .
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:02'!
test03WithdrawDecreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount new.
	Deposit register: 100 on: account.
	Withdraw register: 50 on: account.
		
	self assert: 50 equals: account balance .
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:32'!
test04WithdrawValueMustBePositive 

	| account withdrawValue |
	
	account := ReceptiveAccount new.
	withdrawValue := 50.
	
	self assert: withdrawValue equals: (Withdraw register: withdrawValue on: account) value
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:46'!
test05ReceptiveAccountKnowsRegisteredTransactions 

	| account deposit withdraw |
	
	account := ReceptiveAccount new.
	deposit := Deposit register: 100 on: account.
	withdraw := Withdraw register: 50 on: account.
		
	self assert: (account hasRegistered: deposit).
	self assert: (account hasRegistered: withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:20:54'!
test06ReceptiveAccountDoNotKnowNotRegisteredTransactions

	| account deposit withdraw |
	
	account := ReceptiveAccount new.
	deposit :=  Deposit for: 100.
	withdraw := Withdraw for: 50.
		
	self deny: (account hasRegistered: deposit).
	self deny: (account hasRegistered:withdraw).
! !

!ReceptiveAccountTest methodsFor: 'tests' stamp: 'HAW 5/23/2019 15:21:24'!
test07AccountKnowsItsTransactions 

	| account1 deposit1 |
	
	account1 := ReceptiveAccount new.
	
	deposit1 := Deposit register: 100 on: account1.
		
	self assert: 1 equals: account1 transactions size .
	self assert: (account1 transactions includes: deposit1).
! !


!classDefinition: #ReportsTest category: 'Portfolio2'!
TestCase subclass: #ReportsTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio2'!

!ReportsTest methodsFor: 'setup' stamp: 'DDO 5/29/2022 18:56:14'!
emptyReceptiveAccount

	^ ReceptiveAccount new! !


!ReportsTest methodsFor: 'testing' stamp: 'DDO 5/29/2022 18:56:14'!
test01AccountSummaryforAnEmptyReceptiveAccount

	|account accountSummary transactionLines|
  
	account := self emptyReceptiveAccount.
  	accountSummary := AccountSummary for: account.
	transactionLines := accountSummary transactionsLines.
	
	self assert: 1 equals: transactionLines size.
	self assert: 'Balance = 0' equals: transactionLines first.
  ! !

!ReportsTest methodsFor: 'testing' stamp: 'DDO 5/29/2022 19:50:47'!
test02AccountSummaryforReceptiveAccountWithTransactions

	|account accountSummmary transactionsLines |
  
	account := self emptyReceptiveAccount.
 	 Deposit register: 100 on: account.
	
	accountSummmary := AccountSummary for:account .
	transactionsLines :=accountSummmary transactionsLines.
	
	self assert: 2 equals: transactionsLines size.
	self assert: 'Dep�sito por 100.' equals: transactionsLines first.
	self assert: 'Balance = 100' equals: transactionsLines second.
  
	! !

!ReportsTest methodsFor: 'testing' stamp: 'DDO 5/29/2022 19:59:12'!
test03AccountSummaryforReceptiveAccountWithMultiplyTransactions

	|transactionsLines accountSummmary account1 account2 |
  
	account1 := self emptyReceptiveAccount.
	account2 := self emptyReceptiveAccount.
 	Deposit register: 100 on: account1.
	Withdraw register: 100 on: account1.
	Transfer register: 50 from: account1 to: account2.
	Transfer register: 100 from: account2 to: account1.
	
	accountSummmary := AccountSummary for:account1 .
	transactionsLines :=accountSummmary transactionsLines.
	
	self assert: 5 equals: transactionsLines size.
	self assert: 'Dep�sito por 100.' equals: transactionsLines first.
	self assert: 'Extracci�n por 100.' equals: (transactionsLines at:2).
	self assert: 'Salida por transferencia de 50.' equals: (transactionsLines at:3).
	self assert: 'Entrada por transferencia de 100.' equals: (transactionsLines at:4).
	self assert: 'Balance = 50' equals: (transactionsLines at:5).
  	! !

!ReportsTest methodsFor: 'testing' stamp: 'DDO 5/30/2022 00:19:39'!
test04AccountSummaryForPortfoliosWorksAsEspected

	| account1 account2 accountSummary portfolio transactionsLines|
	
	account1 := self emptyReceptiveAccount.
	account2 := self emptyReceptiveAccount.
	Deposit register: 100 on: account1.
	Withdraw register: 50 on: account2.
	Transfer register: 50 from: account1 to: account2.
	portfolio := Portfolio with: account1 with: account2.
	
	accountSummary := AccountSummary for: portfolio .
	transactionsLines :=accountSummary transactionsLines.
	
	self assert: 5 equals: transactionsLines size.
	self assert: 'Dep�sito por 100.' equals: transactionsLines first.
	self assert: 'Salida por transferencia de 50.' equals: (transactionsLines at:2).
	self assert: 'Extracci�n por 50.' equals: (transactionsLines at:3).
	self assert: 'Entrada por transferencia de 50.' equals: (transactionsLines at:4).
	self assert: 'Balance = 50' equals: (transactionsLines at:5).
! !

!ReportsTest methodsFor: 'testing' stamp: 'DDO 5/29/2022 20:15:39'!
test05TransferNetforAnEmptyReceptiveAccount

	|account transferNet |
  
	account := self emptyReceptiveAccount.
  	transferNet := TransferNet for: account.
	
	self assert: 0 equals: transferNet netValue.
  ! !

!ReportsTest methodsFor: 'testing' stamp: 'DDO 5/30/2022 00:01:20'!
test06TransferNetForReceptiveAccountWithTransactions

	|account1 account2 transferNet |
  
	account1 := self emptyReceptiveAccount.
	account2 := self emptyReceptiveAccount.
	Transfer register: 50 from: account1 to: account2.
	transferNet := TransferNet for:account1.
	
	
	self assert: -50 equals: transferNet netValue.
  
	! !

!ReportsTest methodsFor: 'testing' stamp: 'DDO 5/30/2022 00:17:35'!
test07TransferNetForPortfolioWithTransactions

	|account1 account2 account3 portfolio transferNet |
  
	account1 := self emptyReceptiveAccount.
	account2 := self emptyReceptiveAccount.
	account3 := self emptyReceptiveAccount.
	Transfer register: 50 from: account1 to: account2.
	Transfer register: 20 from: account2 to: account3.
	portfolio := Portfolio with: account1 with: account2.
	transferNet := TransferNet for:portfolio.
	
	
	self assert: -20 equals: transferNet netValue.
  
	! !


!classDefinition: #TransferTest category: 'Portfolio2'!
TestCase subclass: #TransferTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio2'!

!TransferTest methodsFor: 'testing' stamp: 'DDO 5/30/2022 16:12:11'!
test01AssertAccountBalanceAfterATransfer


	| account1 account2 |
	
	account1 := self createEmptyReceptiveAccount.
	
	account2 := self createEmptyReceptiveAccount.
	 
	account1 register: (Deposit for: 50).
	
	Transfer register: 50 from:account1 to: account2.
	
	self assert: account2 balance equals: 50.
	self assert: account1 balance equals: 0.! !

!TransferTest methodsFor: 'testing' stamp: 'DDO 5/30/2022 16:27:56'!
test02TransferIsRegisteredInBothAccounts


	| account1 account2 transfer |
	
	account1 := self createEmptyReceptiveAccount.
	
	account2 := self createEmptyReceptiveAccount.
	 
	account1 register: (Deposit for: 50).
	
	transfer := Transfer register: 50 from:account1 to: account2.
	
	self assert: (account1 hasRegistered: transfer withdrawLeg).
	self assert: (account2 hasRegistered: transfer depositLeg).
	self assert: account2 balance equals: 50.
	self assert: account1 balance equals: 0.! !

!TransferTest methodsFor: 'testing' stamp: 'DDO 5/30/2022 16:12:11'!
test03bothLegsOfTransferBelongToSame


	| account1 account2 transfer1 |
	
	account1 := self createEmptyReceptiveAccount.
	
	account2 := self createEmptyReceptiveAccount.
	 
	account1 register: (Deposit for: 50).
	
	transfer1 := Transfer register: 50 from:account1 to: account2.
	
	self assert: transfer1 withdrawLeg transfer equals: transfer1.
	self assert: transfer1 depositLeg transfer equals: transfer1.! !

!TransferTest methodsFor: 'testing' stamp: 'DDO 5/30/2022 16:12:11'!
test04CanGoFromALegToTheOtherLeg


	| account1 account2 transfer |
	
	account1 := self createEmptyReceptiveAccount.
	
	account2 := self createEmptyReceptiveAccount.
	
	transfer := Transfer register: 50 from:account1 to: account2.
	
	self assert: transfer withdrawLeg otherLeg equals: transfer depositLeg .
	self assert: transfer depositLeg otherLeg equals: transfer withdrawLeg .! !

!TransferTest methodsFor: 'testing' stamp: 'DDO 5/30/2022 16:12:11'!
test05CanNotTransferZeroAmount

	| account1 account2 transfer |
	
	account1 := self createEmptyReceptiveAccount.
	
	account2 := self createEmptyReceptiveAccount.
	
	self
	
	should:[Transfer register: 0 from:account1 to: account2.]  
	raise: Error - MessageNotUnderstood  
	withExceptionDo: [ :anError | 	
					self assert: Transfer CanNotTransferErrorMessege equals: anError messageText.
					self assert: account1 transactions isEmpty.
					self assert: account2 transactions isEmpty]! !

!TransferTest methodsFor: 'testing' stamp: 'DDO 5/30/2022 16:13:50'!
test06TransferLegCanReplyItsValue

	| account1 account2 transfer |
	
	account1 := self createEmptyReceptiveAccount.
	
	account2 := self createEmptyReceptiveAccount.
	
	transfer := Transfer register: 100 from:account1 to: account2.
					
	self assert: (transfer depositLeg value) equals: (transfer withdrawLeg value).
			! !

!TransferTest methodsFor: 'testing' stamp: 'DDO 5/30/2022 16:14:45'!
test07CanNotTransferToItSelfAccount

	| account1|
  
	account1 := self createEmptyReceptiveAccount.
  
	self
  		should:[Transfer register: 50 from:account1 to: account1.]  
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: Transfer CanNotTransferErrorMessege equals: anError messageText.
			self assert: account1 transactions isEmpty.]! !


!TransferTest methodsFor: 'setup' stamp: 'DDO 5/30/2022 16:12:10'!
createEmptyReceptiveAccount

	^ ReceptiveAccount new! !


!classDefinition: #Account category: 'Portfolio2'!
Object subclass: #Account
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio2'!

!Account methodsFor: 'testing' stamp: 'HAW 5/25/2019 12:23:47'!
hasRegistered: aTransaction

	self subclassResponsibility ! !

!Account methodsFor: 'testing' stamp: 'HAW 5/25/2019 12:24:25'!
isComposedBy: anAccount

	self subclassResponsibility ! !


!Account methodsFor: 'balance' stamp: 'HAW 5/25/2019 12:23:40'!
balance

	self subclassResponsibility ! !


!Account methodsFor: 'transactions' stamp: 'HAW 5/25/2019 12:23:27'!
addTransactionsTo: aCollectionOfTransactions

	self subclassResponsibility ! !

!Account methodsFor: 'transactions' stamp: 'HAW 5/25/2019 12:23:15'!
transactions

	self subclassResponsibility ! !


!Account methodsFor: 'composition' stamp: 'HAW 5/25/2019 12:24:04'!
addedTo: aPortfolio

	self subclassResponsibility ! !


!classDefinition: #Portfolio category: 'Portfolio2'!
Account subclass: #Portfolio
	instanceVariableNames: 'accounts parents'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio2'!

!Portfolio methodsFor: 'accounts management' stamp: 'HAW 5/25/2019 11:49:20'!
accountsIncludes: anAccount

	^accounts includes: anAccount ! !

!Portfolio methodsFor: 'accounts management' stamp: 'HAW 5/25/2019 12:05:04'!
accountsIsEmpty
	
	^accounts isEmpty ! !

!Portfolio methodsFor: 'accounts management' stamp: 'HAW 5/25/2019 11:49:06'!
accountsSize
	
	^accounts size! !

!Portfolio methodsFor: 'accounts management' stamp: 'HAW 5/25/2019 12:19:20'!
add: accountToAdd

	self assertCanAdd: accountToAdd.
		
	accounts add: accountToAdd.
	accountToAdd addedTo: self 
	! !

!Portfolio methodsFor: 'accounts management' stamp: 'HAW 5/25/2019 12:17:31'!
rootParents
	
	| rootParents |
	
	rootParents := Set new.
	self addRootParentsTo: rootParents.
	
	^ rootParents! !


!Portfolio methodsFor: 'initialization' stamp: 'HAW 5/25/2019 12:03:18'!
initialize

	accounts := OrderedCollection new.
	parents := OrderedCollection new.! !


!Portfolio methodsFor: 'balance' stamp: 'HAW 5/25/2019 11:19:36'!
balance
	
	^accounts sum: [ :anAccount | anAccount balance ] ifEmpty: [ 0 ]! !


!Portfolio methodsFor: 'transactions' stamp: 'HAW 5/25/2019 11:42:55'!
addTransactionsTo: aCollectionOfTransactions

	accounts do: [ :anAccount | anAccount addTransactionsTo: aCollectionOfTransactions ]! !

!Portfolio methodsFor: 'transactions' stamp: 'HAW 5/25/2019 11:38:32'!
transactions
	
	| transactions |
	
	transactions := OrderedCollection new.
	accounts do: [ :anAccount | anAccount addTransactionsTo: transactions ].
	
	^transactions ! !


!Portfolio methodsFor: 'composition' stamp: 'HAW 5/25/2019 12:02:59'!
addedTo: aPortfolio 
	
	parents add: aPortfolio ! !


!Portfolio methodsFor: 'testing' stamp: 'HAW 5/25/2019 12:20:56'!
anyRootParentIsComposedBy: accountToAdd

	^self rootParents anySatisfy: [ :aParent | aParent isComposedBy: accountToAdd]! !

!Portfolio methodsFor: 'testing' stamp: 'HAW 5/25/2019 11:28:29'!
hasRegistered: aTransaction

	^accounts anySatisfy: [ :anAccount | anAccount hasRegistered: aTransaction ]! !

!Portfolio methodsFor: 'testing' stamp: 'HAW 5/29/2019 16:24:54'!
isComposedBy: anAccount

	^ self = anAccount or: [ accounts anySatisfy: [ :composedAccount | (composedAccount isComposedBy: anAccount) or: [ anAccount isComposedBy: composedAccount ]]]! !


!Portfolio methodsFor: 'account management - private' stamp: 'HAW 5/25/2019 12:17:31'!
addRootParentsTo: rootParents

	parents 
		ifEmpty: [ rootParents add: self ] 
		ifNotEmpty: [ parents do: [ :aParent | aParent addRootParentsTo: rootParents ]]! !

!Portfolio methodsFor: 'account management - private' stamp: 'HAW 5/25/2019 12:20:36'!
assertCanAdd: accountToAdd

	(self anyRootParentIsComposedBy: accountToAdd) ifTrue: [ self signalCanNotAddAccount ].
! !

!Portfolio methodsFor: 'account management - private' stamp: 'HAW 5/25/2019 11:48:34'!
signalCanNotAddAccount
	
	self error: self class canNotAddAccountErrorMessage! !


!Portfolio methodsFor: 'reporting' stamp: 'DDO 5/29/2022 23:10:10'!
reportTo: aNetValue

	accounts do:[:account| account reportTo: aNetValue].
	^aNetValue ! !

!Portfolio methodsFor: 'reporting' stamp: 'DDO 5/29/2022 23:33:48'!
reportTransactionTo: aReporter
	
	accounts do:[:account | account reportTransactionTo: aReporter]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Portfolio class' category: 'Portfolio2'!
Portfolio class
	instanceVariableNames: ''!

!Portfolio class methodsFor: 'as yet unclassified' stamp: 'HAW 5/25/2019 11:48:55'!
canNotAddAccountErrorMessage
	
	^'Can not add repeated account to a portfolio'! !

!Portfolio class methodsFor: 'as yet unclassified' stamp: 'HAW 5/25/2019 11:18:21'!
with: anAccount

	^self new 
		add: anAccount;
		yourself! !

!Portfolio class methodsFor: 'as yet unclassified' stamp: 'HAW 5/25/2019 11:23:59'!
with: anAccount with: anotherAccount

	^self new 
		add: anAccount;
		add: anotherAccount;
		yourself! !


!classDefinition: #ReceptiveAccount category: 'Portfolio2'!
Account subclass: #ReceptiveAccount
	instanceVariableNames: 'transactions'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio2'!

!ReceptiveAccount methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:35'!
initialize

	super initialize.
	transactions := OrderedCollection new.! !


!ReceptiveAccount methodsFor: 'transactions' stamp: 'HAW 5/25/2019 11:38:52'!
addTransactionsTo: aCollectionOfTransactions

	aCollectionOfTransactions addAll: transactions ! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
register: aTransaction

	transactions add: aTransaction 
! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
transactions 

	^ transactions copy! !


!ReceptiveAccount methodsFor: 'balance' stamp: 'HAW 5/25/2019 11:24:46'!
balance

	^transactions 
		inject: 0
		into: [ :currentBalance :transaction | transaction affectBalance: currentBalance ]! !


!ReceptiveAccount methodsFor: 'testing' stamp: 'NR 10/21/2019 18:55:56'!
hasRegistered: aTransaction

	^ transactions includes: aTransaction 
! !

!ReceptiveAccount methodsFor: 'testing' stamp: 'HAW 5/25/2019 11:54:51'!
isComposedBy: anAccount

	^self = anAccount ! !


!ReceptiveAccount methodsFor: 'composition' stamp: 'HAW 5/25/2019 12:03:32'!
addedTo: aPortfolio 
	
	! !


!ReceptiveAccount methodsFor: 'reporting' stamp: 'DDO 5/29/2022 23:33:07'!
reportTransactionTo: aReporter
	
	transactions do:[:transaction | transaction reportTransactionTo: aReporter]! !


!classDefinition: #AccountTransaction category: 'Portfolio2'!
Object subclass: #AccountTransaction
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio2'!

!AccountTransaction methodsFor: 'value' stamp: 'HernanWilkinson 9/12/2011 12:25'!
value 

	self subclassResponsibility ! !


!AccountTransaction methodsFor: 'balance' stamp: 'HAW 5/25/2019 11:25:39'!
affectBalance: aBalance

	self subclassResponsibility ! !


!AccountTransaction methodsFor: 'reporting' stamp: 'DDO 5/29/2022 20:41:14'!
reportTransactionTo: aSummary

	^self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountTransaction class' category: 'Portfolio2'!
AccountTransaction class
	instanceVariableNames: ''!

!AccountTransaction class methodsFor: 'instance creation' stamp: 'NR 10/21/2019 18:54:27'!
register: aValue on: account

	| transaction |
	
	transaction := self for: aValue.
	account register: transaction.
		
	^ transaction! !


!classDefinition: #Deposit category: 'Portfolio2'!
AccountTransaction subclass: #Deposit
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio2'!

!Deposit methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:45'!
initializeFor: aValue

	value := aValue ! !


!Deposit methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:38'!
value

	^ value! !


!Deposit methodsFor: 'balance' stamp: 'HAW 5/25/2019 11:25:02'!
affectBalance: aBalance

	^aBalance + value ! !


!Deposit methodsFor: 'reporting' stamp: 'DDO 5/29/2022 23:37:47'!
reportTransactionTo: aReporter
	
	aReporter reportDeposit: self
					! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Deposit class' category: 'Portfolio2'!
Deposit class
	instanceVariableNames: ''!

!Deposit class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:38'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #TransferLeg category: 'Portfolio2'!
AccountTransaction subclass: #TransferLeg
	instanceVariableNames: 'transfer'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio2'!

!TransferLeg methodsFor: 'instance creation' stamp: 'ddo 5/26/2022 17:31:29'!
initiliazeFor: aTransfer 

	transfer := aTransfer ! !


!TransferLeg methodsFor: 'reporting' stamp: 'DDO 5/29/2022 20:39:25'!
reportTransactionTo: aSummary

	^self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TransferLeg class' category: 'Portfolio2'!
TransferLeg class
	instanceVariableNames: ''!

!TransferLeg class methodsFor: 'as yet unclassified' stamp: 'ddo 5/26/2022 17:15:41'!
for: aTransfer

	^self new initiliazeFor: aTransfer ! !


!classDefinition: #DepositLeg category: 'Portfolio2'!
TransferLeg subclass: #DepositLeg
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio2'!

!DepositLeg methodsFor: 'balance' stamp: 'ddo 5/26/2022 18:31:53'!
affectBalance: aBalance

	^aBalance + transfer value! !


!DepositLeg methodsFor: 'accessing' stamp: 'ddo 5/26/2022 20:30:57'!
otherLeg
	
	^transfer withdrawLeg ! !

!DepositLeg methodsFor: 'accessing' stamp: 'ddo 5/26/2022 20:16:44'!
transfer
	
	^transfer! !

!DepositLeg methodsFor: 'accessing' stamp: 'j 5/27/2022 13:30:20'!
value

	^transfer value! !


!DepositLeg methodsFor: 'reporting' stamp: 'DDO 5/29/2022 23:37:33'!
reportTransactionTo: aReporter

	aReporter reportDepositLeg: self! !


!classDefinition: #WithdrawLeg category: 'Portfolio2'!
TransferLeg subclass: #WithdrawLeg
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio2'!

!WithdrawLeg methodsFor: 'balance' stamp: 'ddo 5/26/2022 18:32:07'!
affectBalance: aBalance

	^aBalance - transfer value ! !


!WithdrawLeg methodsFor: 'accessing' stamp: 'ddo 5/26/2022 20:30:38'!
otherLeg
	
	^transfer depositLeg ! !

!WithdrawLeg methodsFor: 'accessing' stamp: 'ddo 5/26/2022 20:16:17'!
transfer
	^transfer ! !

!WithdrawLeg methodsFor: 'accessing' stamp: 'j 5/27/2022 13:31:05'!
value

	^transfer value.! !


!WithdrawLeg methodsFor: 'reporting' stamp: 'DDO 5/29/2022 23:38:29'!
reportTransactionTo: aReporter

	aReporter reportWithdrawLeg: self! !


!classDefinition: #Withdraw category: 'Portfolio2'!
AccountTransaction subclass: #Withdraw
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio2'!

!Withdraw methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:46'!
initializeFor: aValue

	value := aValue ! !


!Withdraw methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:33'!
value

	^ value! !


!Withdraw methodsFor: 'balance' stamp: 'HAW 5/25/2019 11:25:15'!
affectBalance: aBalance

	^aBalance - value! !


!Withdraw methodsFor: 'reporting' stamp: 'DDO 5/29/2022 23:38:59'!
reportTransactionTo: aReporter

	aReporter reportWithdraw: self! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Withdraw class' category: 'Portfolio2'!
Withdraw class
	instanceVariableNames: ''!

!Withdraw class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:33'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #TransactionsReport category: 'Portfolio2'!
Object subclass: #TransactionsReport
	instanceVariableNames: 'account'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio2'!

!TransactionsReport methodsFor: 'reporting' stamp: 'DDO 5/30/2022 16:15:54'!
reportDeposit: aDeposit
	
	^self subclassResponsibility ! !

!TransactionsReport methodsFor: 'reporting' stamp: 'DDO 5/30/2022 16:16:50'!
reportDepositLeg: aDepositLeg
	
	^self subclassResponsibility ! !

!TransactionsReport methodsFor: 'reporting' stamp: 'DDO 5/30/2022 16:16:28'!
reportWithdraw: aWithdraw
	
	^self subclassResponsibility ! !

!TransactionsReport methodsFor: 'reporting' stamp: 'DDO 5/30/2022 16:16:36'!
reportWithdrawLeg: aWithdrawLeg
	
	^self subclassResponsibility ! !


!TransactionsReport methodsFor: 'instance creation' stamp: 'DDO 5/30/2022 16:21:11'!
initializeFor: anAccount


	account := anAccount ! !


!classDefinition: #AccountSummary category: 'Portfolio2'!
TransactionsReport subclass: #AccountSummary
	instanceVariableNames: 'lines'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio2'!

!AccountSummary methodsFor: 'reporting' stamp: 'DDO 5/29/2022 23:52:44'!
reportDeposit: aDeposit
	
	lines add: ('Dep�sito por ', aDeposit value asString, '.')! !

!AccountSummary methodsFor: 'reporting' stamp: 'DDO 5/29/2022 23:52:58'!
reportDepositLeg: aDepositLeg
	
	lines add: ('Entrada por transferencia de ', aDepositLeg value asString, '.')! !

!AccountSummary methodsFor: 'reporting' stamp: 'DDO 5/29/2022 23:53:06'!
reportWithdraw: aWithdraw
	
	lines add: ('Extracci�n por ', aWithdraw value asString, '.')! !

!AccountSummary methodsFor: 'reporting' stamp: 'DDO 5/29/2022 23:53:13'!
reportWithdrawLeg: aWithdrawLeg
	
	lines add: ('Salida por transferencia de ', aWithdrawLeg value asString, '.')! !


!AccountSummary methodsFor: 'report value' stamp: 'DDO 5/29/2022 23:51:55'!
transactionsLines
	
	lines:= OrderedCollection new.
	account reportTransactionTo: self.
	lines add: ('Balance = ', account balance asString).
	^lines! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountSummary class' category: 'Portfolio2'!
AccountSummary class
	instanceVariableNames: ''!

!AccountSummary class methodsFor: 'instance creation' stamp: 'DDO 5/29/2022 19:19:37'!
for: anAccount 
	
	^self new initializeFor: anAccount ! !


!classDefinition: #TransferNet category: 'Portfolio2'!
TransactionsReport subclass: #TransferNet
	instanceVariableNames: 'netValue'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio2'!

!TransferNet methodsFor: 'reporting' stamp: 'DDO 5/29/2022 23:41:26'!
reportDeposit: aDeposit

	! !

!TransferNet methodsFor: 'reporting' stamp: 'DDO 5/29/2022 23:43:44'!
reportDepositLeg: aDepositLeg

	netValue := netValue + aDepositLeg value

	! !

!TransferNet methodsFor: 'reporting' stamp: 'DDO 5/29/2022 23:41:37'!
reportWithdraw: aDeposit

	! !

!TransferNet methodsFor: 'reporting' stamp: 'DDO 5/30/2022 00:05:35'!
reportWithdrawLeg: aWithdrawLeg

	netValue := netValue - aWithdrawLeg value

	! !


!TransferNet methodsFor: 'report value' stamp: 'DDO 5/30/2022 00:02:32'!
netValue
	
	netValue := 0.
	account reportTransactionTo: self.
	^netValue! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TransferNet class' category: 'Portfolio2'!
TransferNet class
	instanceVariableNames: ''!

!TransferNet class methodsFor: 'instance creation' stamp: 'DDO 5/29/2022 20:16:24'!
for: anAccount 

	^self new initializeFor: anAccount ! !


!classDefinition: #Transfer category: 'Portfolio2'!
Object subclass: #Transfer
	instanceVariableNames: 'amount withdrawLeg depositLeg'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio2'!

!Transfer methodsFor: 'instance creation' stamp: 'DDO 5/28/2022 20:00:38'!
initializeRegister: anAmount from: aOriginAccount to: aReceiverAccount 
	
	(anAmount <= 0 or:[ aOriginAccount = aReceiverAccount ]) ifTrue: [self signalCanNotTransfer]. 
	amount := anAmount.
	withdrawLeg := WithdrawLeg for: self.
	depositLeg := DepositLeg for: self.
	aOriginAccount register: withdrawLeg .
	aReceiverAccount register: depositLeg .! !


!Transfer methodsFor: 'accessing' stamp: 'ddo 5/26/2022 18:39:01'!
depositLeg
	
	^depositLeg! !

!Transfer methodsFor: 'accessing' stamp: 'ddo 5/26/2022 18:33:02'!
value
	
	^amount ! !

!Transfer methodsFor: 'accessing' stamp: 'ddo 5/26/2022 18:38:42'!
withdrawLeg
	
	^withdrawLeg! !


!Transfer methodsFor: 'signal errors' stamp: 'j 5/27/2022 13:13:50'!
signalCanNotTransfer

	^self error: self class CanNotTransferErrorMessege. ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Transfer class' category: 'Portfolio2'!
Transfer class
	instanceVariableNames: ''!

!Transfer class methodsFor: 'instance creation' stamp: 'j 5/27/2022 13:11:24'!
CanNotTransferErrorMessege

	^'Can not transfer'! !

!Transfer class methodsFor: 'instance creation' stamp: 'DDO 5/26/2022 11:58:12'!
register: anAmount from: aOriginAccount to: aReceiverAccount

	^self new initializeRegister: anAmount from: aOriginAccount to: aReceiverAccount ! !
