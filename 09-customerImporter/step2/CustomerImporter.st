!classDefinition: #CustomerImporterTest category: 'CustomerImporter'!
TestCase subclass: #CustomerImporterTest
	instanceVariableNames: 'session'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!CustomerImporterTest methodsFor: 'tests' stamp: 'DDO 6/27/2022 01:07:54'!
assertCustomerImporterFrom: anInput failWith: anErrorBlock
		
		self should: [CustomerImporter valueFrom: anInput into: session]
		raise:  Error - MessageNotUnderstood 
		withExceptionDo: [:anError|
			self assert: anErrorBlock value equals: anError messageText]

	! !

!CustomerImporterTest methodsFor: 'tests' stamp: 'DDO 6/26/2022 23:59:11'!
test01ImportCustomersWithValidData

	CustomerImporter valueFrom: self validImportData into: session.

	self assertImportedRightNumberOfCustomers.
	self assertPepeSanchezWasImportedCorrecty.
	self assertJuanPerezWasImportedCorrectly ! !

!CustomerImporterTest methodsFor: 'tests' stamp: 'DDO 6/27/2022 01:09:57'!
test02CanNotStartRecordWithInvalidType
	
	self assertCustomerImporterFrom: self invalidInputOnType 
		failWith: [CustomerImporter invalidRecordTypeErrorMessage]! !

!CustomerImporterTest methodsFor: 'tests' stamp: 'DDO 6/27/2022 01:10:51'!
test03CanNotHaveAnInvalidIdType

	self assertCustomerImporterFrom: self invalidInputOnTypeId 
		failWith: CustomerImporter invalidInputErrorMessage! !

!CustomerImporterTest methodsFor: 'tests' stamp: 'DDO 6/27/2022 01:11:46'!
test04CanNotHaveMoreOrLessFieldsThanExpectedCustomer
	
	self assertCustomerImporterFrom: self invalidFieldsSizeOnCustomer 
		failWith: CustomerImporter invalidInputErrorMessage
	! !

!CustomerImporterTest methodsFor: 'tests' stamp: 'DDO 6/27/2022 01:13:00'!
test05CanNotHaveMoreOrLessFieldsThanExpectedAdress

	
	self assertCustomerImporterFrom: self invalidFieldsSizeOnAdress 
		failWith: CustomerImporter invalidInputErrorMessage! !

!CustomerImporterTest methodsFor: 'tests' stamp: 'DDO 6/27/2022 01:13:52'!
test06CanNotHaveLettersOnStreetNumber
	
	self assertCustomerImporterFrom: self invalidFieldsStreetNumberOnAdress 
		failWith: CustomerImporter invalidInputErrorMessage
	! !

!CustomerImporterTest methodsFor: 'tests' stamp: 'DDO 6/27/2022 01:14:37'!
test07CanNotHaveLettersOnZipCodeAdress
	
	self assertCustomerImporterFrom: self invalidFieldsZipCodeOnAdress 
	failWith: CustomerImporter invalidInputErrorMessage! !

!CustomerImporterTest methodsFor: 'tests' stamp: 'DDO 6/27/2022 01:15:24'!
test08CanNotImportAdressWhenThereIsNotACustomer

	self assertCustomerImporterFrom: self invalidInputAddressWithoutACustomer 
		failWith: CustomerImporter canNotImportAddressWithoutACustomerErrorMessage

	! !

!CustomerImporterTest methodsFor: 'tests' stamp: 'DDO 6/27/2022 01:16:15'!
test09CanNotImportEmptyFields
	
	self assertCustomerImporterFrom: self invalidInputEmptyFields 
		failWith: CustomerImporter invalidInputErrorMessage	! !

!CustomerImporterTest methodsFor: 'tests' stamp: 'DDO 6/27/2022 01:17:02'!
test10CanNotImportEmptyLines

	self assertCustomerImporterFrom: self invalidInputEmptyLines 
		failWith: CustomerImporter invalidInputErrorMessage! !


!CustomerImporterTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:22:05'!
assertAddressOf: importedCustomer at: aStreetName hasNumber: aNumber town: aTown zipCode: aZipCode province: aProvince

	| importedAddress |

	importedAddress := importedCustomer addressAt: aStreetName ifNone: [ self fail ].
	self assert: aStreetName equals: importedAddress streetName.
	self assert: aNumber equals: importedAddress streetNumber.
	self assert: aTown equals: importedAddress town.
	self assert: aZipCode equals: importedAddress zipCode.
	self assert: aProvince equals: importedAddress province.

	! !

!CustomerImporterTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:27:57'!
assertCustomerWithIdentificationType: anIdType number: anIdNumber hasFirstName: aFirstName lastName: aLastName

	| importedCustomer |

	importedCustomer := self customerWithIdentificationType: anIdType number: anIdNumber.

	self assert: aFirstName equals: importedCustomer firstName.
	self assert: aLastName equals: importedCustomer lastName.
	self assert: anIdType equals: importedCustomer identificationType.
	self assert: anIdNumber equals: importedCustomer identificationNumber.

	^importedCustomer

	! !

!CustomerImporterTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:12:18'!
assertImportedRightNumberOfCustomers

	^ self assert: 2 equals: (session selectAllOfType: Customer) size! !

!CustomerImporterTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:28:45'!
assertJuanPerezWasImportedCorrectly

	| importedCustomer |

	importedCustomer := self assertCustomerWithIdentificationType: 'C' number: '23-25666777-9' hasFirstName: 'Juan' lastName: 'Perez'.
	self assertAddressOf: importedCustomer at: 'Alem' hasNumber: 1122 town: 'CABA' zipCode: 1001 province: 'CABA'
	! !

!CustomerImporterTest methodsFor: 'assertions' stamp: 'HAW 5/22/2022 18:28:05'!
assertPepeSanchezWasImportedCorrecty

	| importedCustomer |

	importedCustomer := self assertCustomerWithIdentificationType: 'D' number: '22333444' hasFirstName: 'Pepe' lastName: 'Sanchez'.
	self assertAddressOf: importedCustomer at: 'San Martin' hasNumber: 3322 town: 'Olivos' zipCode: 1636 province: 'BsAs'.
	self assertAddressOf: importedCustomer at: 'Maipu' hasNumber: 888 town: 'Florida' zipCode: 1122 province: 'Buenos Aires'.


	! !


!CustomerImporterTest methodsFor: 'setUp/tearDown' stamp: 'HAW 5/22/2022 00:27:50'!
setUp

	session := DataBaseSession for: (Array with: Address with: Customer).
	session beginTransaction.
! !

!CustomerImporterTest methodsFor: 'setUp/tearDown' stamp: 'HAW 5/22/2022 00:28:23'!
tearDown

	session commit.
	session close.
	! !


!CustomerImporterTest methodsFor: 'customer' stamp: 'HAW 5/22/2022 18:14:22'!
customerWithIdentificationType: anIdType number: anIdNumber

	^ (session
		select: [ :aCustomer | aCustomer identificationType = anIdType and: [ aCustomer identificationNumber = anIdNumber ]]
		ofType: Customer) anyOne! !


!CustomerImporterTest methodsFor: 'test data' stamp: 'DDO 6/27/2022 00:20:36'!
invalidFieldsSizeOnAdress

	^ ReadStream on: self pepeSanchezCustomerRecord,'
A,San Martin,3322,Olivos,1636,BsAs,A'! !

!CustomerImporterTest methodsFor: 'test data' stamp: 'DDO 6/27/2022 00:17:47'!
invalidFieldsSizeOnCustomer

	^ ReadStream on:'C,Pepe,Sanchez,D, D,22333444'! !

!CustomerImporterTest methodsFor: 'test data' stamp: 'DDO 6/27/2022 00:22:44'!
invalidFieldsStreetNumberOnAdress

	^ ReadStream on:
self pepeSanchezCustomerRecord,'
A,San Martin,A3322,Olivos,1636,BsAs'! !

!CustomerImporterTest methodsFor: 'test data' stamp: 'DDO 6/27/2022 00:23:04'!
invalidFieldsZipCodeOnAdress

	^ ReadStream on: self pepeSanchezCustomerRecord,'
A,San Martin,3322,Olivos,1636A,BsAs'! !

!CustomerImporterTest methodsFor: 'test data' stamp: 'DDO 6/26/2022 23:42:02'!
invalidInputAddressWithoutACustomer

	^ ReadStream on:'A,San Martin,3322,Olivos,1636,BsAs'! !

!CustomerImporterTest methodsFor: 'test data' stamp: 'DDO 6/27/2022 00:30:16'!
invalidInputEmptyFields

	^ ReadStream on: 'C,,Sanchez,D,22333444'! !

!CustomerImporterTest methodsFor: 'test data' stamp: 'DDO 6/27/2022 00:40:05'!
invalidInputEmptyLines

	^ ReadStream on: self pepeSanchezCustomerRecord,'

A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !

!CustomerImporterTest methodsFor: 'test data' stamp: 'DDO 6/26/2022 23:43:53'!
invalidInputOnType

	^ ReadStream on: 'CC,Pepe,Sanchez,D,22333444'! !

!CustomerImporterTest methodsFor: 'test data' stamp: 'DDO 6/26/2022 23:43:28'!
invalidInputOnTypeId

	^ ReadStream on: 'C,Pepe,Sanchez,invalidId,22333444'! !

!CustomerImporterTest methodsFor: 'test data' stamp: 'DDO 6/26/2022 23:46:14'!
pepeSanchezCustomerRecord

	^'C,Pepe,Sanchez,D,22333444'! !

!CustomerImporterTest methodsFor: 'test data' stamp: 'HAW 5/22/2022 18:08:08'!
validImportData

	^ ReadStream on:
'C,Pepe,Sanchez,D,22333444
A,San Martin,3322,Olivos,1636,BsAs
A,Maipu,888,Florida,1122,Buenos Aires
C,Juan,Perez,C,23-25666777-9
A,Alem,1122,CABA,1001,CABA'! !


!classDefinition: #Address category: 'CustomerImporter'!
Object subclass: #Address
	instanceVariableNames: 'id streetName streetNumber town zipCode province'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province

	^province! !

!Address methodsFor: 'province' stamp: 'HAW 5/22/2022 00:19:29'!
province: aProvince

	province := aProvince
	! !


!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 17:55:46'!
isAt: aStreetName

	^streetName = aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName

	^streetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetName: aStreetName

	streetName := aStreetName ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber

	^streetNumber ! !

!Address methodsFor: 'street' stamp: 'HAW 5/22/2022 00:19:29'!
streetNumber: aStreetNumber

	streetNumber := aStreetNumber ! !


!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town

	^town! !

!Address methodsFor: 'twon' stamp: 'HAW 5/22/2022 00:19:29'!
town: aTown

	town := aTown! !


!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode

	^zipCode! !

!Address methodsFor: 'zip code' stamp: 'HAW 5/22/2022 00:19:29'!
zipCode: aZipCode

	zipCode := aZipCode! !


!classDefinition: #Customer category: 'CustomerImporter'!
Object subclass: #Customer
	instanceVariableNames: 'id firstName lastName identificationType identificationNumber addresses'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addAddress: anAddress

	addresses add: anAddress ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 17:55:17'!
addressAt: aStreetName ifNone: aNoneBlock

	^addresses detect: [ :address | address isAt: aStreetName ] ifNone: aNoneBlock ! !

!Customer methodsFor: 'addresses' stamp: 'HAW 5/22/2022 00:19:29'!
addresses

	^ addresses! !


!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName

	^firstName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
firstName: aName

	firstName := aName! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName

	^lastName ! !

!Customer methodsFor: 'name' stamp: 'HAW 5/22/2022 00:19:29'!
lastName: aLastName

	lastName := aLastName
! !


!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber

	^identificationNumber ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationNumber: anIdentificationNumber

	identificationNumber := anIdentificationNumber! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType

	^identificationType ! !

!Customer methodsFor: 'identification' stamp: 'HAW 5/22/2022 00:19:29'!
identificationType: anIdentificationType

	identificationType := anIdentificationType! !


!Customer methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initialize

	super initialize.
	addresses := OrderedCollection new.! !


!classDefinition: #CustomerImporter category: 'CustomerImporter'!
Object subclass: #CustomerImporter
	instanceVariableNames: 'session readStream record newCustomer line'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!CustomerImporter methodsFor: 'initialization' stamp: 'HAW 5/22/2022 18:06:47'!
initializeFrom: aReadStream into: aSession
	session := aSession.
	readStream := aReadStream.! !


!CustomerImporter methodsFor: 'importing' stamp: 'DDO 6/23/2022 20:05:16'!
createRecord

	^ record := line findTokens: $,! !

!CustomerImporter methodsFor: 'importing' stamp: 'DDO 6/23/2022 20:04:41'!
hasLineToImport

	line := readStream nextLine.
		 ^line notNil! !

!CustomerImporter methodsFor: 'importing' stamp: 'DDO 6/26/2022 23:35:57'!
importAddress

	|newAddress|
	self assertThereIsACustomerBefore.
	self assertAdressRecordIsAsExpected.

			newAddress := Address new.
			newCustomer addAddress: newAddress.
			newAddress streetName: record second.
			newAddress streetNumber: record third asNumber .
			newAddress town: record fourth.
			newAddress zipCode: record fifth asNumber .
			newAddress province: record sixth! !

!CustomerImporter methodsFor: 'importing' stamp: 'juan 6/26/2022 18:17:06'!
importCustomer

	self assertCostumerRecordIsAsExpected.

	newCustomer := Customer new.
			newCustomer firstName: record second.
			newCustomer lastName: record third.
			newCustomer identificationType: record fourth.
			newCustomer identificationNumber: record fifth.
			session persist: newCustomer! !

!CustomerImporter methodsFor: 'importing' stamp: 'DDO 6/27/2022 00:56:55'!
importRecord

	self assertHasNotEmptyLines.
	self assertHasNotEmptyFields.
	
	self isCustomerRecord ifTrue: [ ^self importCustomer ].
	self isAddressRecord ifTrue: [ ^self importAddress ].
	
	self signalInvalidRecordType.! !

!CustomerImporter methodsFor: 'importing' stamp: 'DDO 6/27/2022 00:56:48'!
isAddressRecord

	^ record first = 'A'! !

!CustomerImporter methodsFor: 'importing' stamp: 'DDO 6/27/2022 00:56:07'!
isCustomerRecord

	^ record first = 'C'! !

!CustomerImporter methodsFor: 'importing' stamp: 'DDO 6/26/2022 23:56:57'!
value
 
	[self hasLineToImport ] whileTrue: [ 
		self createRecord.
		self importRecord ].

	! !


!CustomerImporter methodsFor: 'assertions' stamp: 'DDO 6/27/2022 00:49:45'!
assertAdressRecordIsAsExpected

	^(record size = 6 	and:[self assertStreetNumberHasNotLetters and:[self assertZipCodeHasNotLetters]]) ifFalse:[self signalInvalidInput]
	
	! !

!CustomerImporter methodsFor: 'assertions' stamp: 'DDO 6/27/2022 00:45:53'!
assertCostumerRecordIsAsExpected

	^ (record size = 5 and:[record fourth = 'C' or:[record fourth = 'D']]) ifFalse:[self signalInvalidInput]! !

!CustomerImporter methodsFor: 'assertions' stamp: 'DDO 6/27/2022 00:48:03'!
assertHasNotEmptyFields

	record do:[:aField | (self assertIsNotABlankField: aField) ifFalse:[self signalInvalidInput ]]! !

!CustomerImporter methodsFor: 'assertions' stamp: 'DDO 6/27/2022 00:45:53'!
assertHasNotEmptyLines

	^ record ifEmpty:[self signalInvalidInput ]! !

!CustomerImporter methodsFor: 'assertions' stamp: 'DDO 6/27/2022 00:51:30'!
assertHasNotLetters: aField
	
	^(aField allSatisfy: [:aChar | aChar isDigit])! !

!CustomerImporter methodsFor: 'assertions' stamp: 'DDO 6/26/2022 22:45:01'!
assertIsNotABlankField: aField

	^(aField = '') not! !

!CustomerImporter methodsFor: 'assertions' stamp: 'juan 6/26/2022 21:02:50'!
assertStreetNumberHasNotLetters

	^self assertHasNotLetters: record third.! !

!CustomerImporter methodsFor: 'assertions' stamp: 'DDO 6/26/2022 22:51:31'!
assertThereIsACustomerBefore

	^ newCustomer ifNil:[self error: self class canNotImportAddressWithoutACustomerErrorMessage]! !

!CustomerImporter methodsFor: 'assertions' stamp: 'DDO 6/27/2022 00:49:45'!
assertZipCodeHasNotLetters

	^self assertHasNotLetters: record fifth.! !


!CustomerImporter methodsFor: 'signals' stamp: 'DDO 6/27/2022 00:45:53'!
signalInvalidInput

	^ self error: self class invalidInputErrorMessage! !

!CustomerImporter methodsFor: 'signals' stamp: 'DDO 6/27/2022 00:05:45'!
signalInvalidRecordType

	^ self error: self class invalidRecordTypeErrorMessage! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CustomerImporter class' category: 'CustomerImporter'!
CustomerImporter class
	instanceVariableNames: ''!

!CustomerImporter class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 18:06:47'!
from: aReadStream into: aSession
	^self new initializeFrom: aReadStream into: aSession! !


!CustomerImporter class methodsFor: 'importing' stamp: 'HAW 5/22/2022 18:11:27'!
valueFrom: aReadStream into: aSession

	^(self from: aReadStream into: aSession) value! !


!CustomerImporter class methodsFor: 'errors' stamp: 'DDO 6/26/2022 22:49:02'!
canNotImportAddressWithoutACustomerErrorMessage
	
	^'can not import an address without a customer'! !

!CustomerImporter class methodsFor: 'errors' stamp: 'DDO 6/23/2022 20:13:36'!
invalidInputErrorMessage
		
		^'invalid input'! !

!CustomerImporter class methodsFor: 'errors' stamp: 'DDO 6/27/2022 00:09:27'!
invalidRecordTypeErrorMessage
	
	^'invalid record type'! !


!classDefinition: #DataBaseSession category: 'CustomerImporter'!
Object subclass: #DataBaseSession
	instanceVariableNames: 'configuration tables id'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'CustomerImporter'!

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 00:19:29'!
beginTransaction

	! !

!DataBaseSession methodsFor: 'transaction management' stamp: 'HAW 5/22/2022 19:17:36'!
commit

	(tables at: Customer ifAbsent: [#()]) do: [ :aCustomer | self persistAddressesOf: aCustomer ]
	! !


!DataBaseSession methodsFor: 'closing' stamp: 'HAW 5/22/2022 00:19:29'!
close

	! !


!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
defineIdOf: anObject

	anObject instVarNamed: 'id' put: (self newIdFor: anObject).! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
delay

	(Delay forMilliseconds: 100) wait! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 19:29:06'!
objectsOfType: aType

	^ tables at: aType ifAbsent: [ #() ]! !

!DataBaseSession methodsFor: 'persistence - private' stamp: 'HAW 5/22/2022 00:19:29'!
persistAddressesOf: anObjectWithAddresses

	anObjectWithAddresses addresses do: [ :anAddress | self persist: anAddress ]
	! !


!DataBaseSession methodsFor: 'initialization' stamp: 'HAW 5/22/2022 00:19:29'!
initializeFor: aConfiguration

	configuration := aConfiguration.
	tables := Dictionary new.
	id := 0.! !


!DataBaseSession methodsFor: 'id' stamp: 'HAW 5/22/2022 00:19:29'!
newIdFor: anObject

	id := id + 1.
	^id! !


!DataBaseSession methodsFor: 'persistance' stamp: 'HAW 5/22/2022 00:19:29'!
persist: anObject

	| table |

	self delay.
	table := tables at: anObject class ifAbsentPut: [ Set new ].

	self defineIdOf: anObject.
	table add: anObject.

	(anObject isKindOf: Customer) ifTrue: [ self persistAddressesOf: anObject ].! !


!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
select: aCondition ofType: aType

	self delay.
	^(self objectsOfType: aType) select: aCondition ! !

!DataBaseSession methodsFor: 'selecting' stamp: 'HAW 5/22/2022 19:29:06'!
selectAllOfType: aType

	self delay.
	^(self objectsOfType: aType) copy ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'DataBaseSession class' category: 'CustomerImporter'!
DataBaseSession class
	instanceVariableNames: ''!

!DataBaseSession class methodsFor: 'instance creation' stamp: 'HAW 5/22/2022 00:19:29'!
for: aConfiguration

	^self new initializeFor: aConfiguration! !
