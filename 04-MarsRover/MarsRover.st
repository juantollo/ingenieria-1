!classDefinition: #MarsRoverTest category: 'MarsRover'!
TestCase subclass: #MarsRoverTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRoverTest methodsFor: 'testing' stamp: 'DDO 5/15/2022 14:16:09'!
test01ProcessAnEmptyInstructionDoesNotMove

	|marsRover|
	
	marsRover := self marsRoverAt00HeadingSouth.
	
	self assert:((marsRover process:'') isAt: 0@0 heading: #South)! !

!MarsRoverTest methodsFor: 'testing' stamp: 'DDO 5/15/2022 14:16:09'!
test02ProcessFowardWhenHeadingSouthMovesToSouth

	|marsRover|
	
	marsRover := self marsRoverAt00HeadingSouth.
	
	self assert:((marsRover process:'f') isAt: 0@-1 heading: #South)! !

!MarsRoverTest methodsFor: 'testing' stamp: 'DDO 5/15/2022 14:16:09'!
test03ProcessBackwardWhenHeadingSouthMovesToNorth

	|marsRover|
	
	marsRover := self marsRoverAt00HeadingSouth.
	
	self assert:((marsRover process:'b') isAt: 0@1 heading: #South)! !

!MarsRoverTest methodsFor: 'testing' stamp: 'DDO 5/15/2022 14:16:09'!
test04ProcessRightWhenHeadingSouthHeadToWest

	|marsRover|
	
	marsRover := self marsRoverAt00HeadingSouth.
	
	self assert:((marsRover process:'r') isAt: 0@0 heading: #West)! !

!MarsRoverTest methodsFor: 'testing' stamp: 'DDO 5/15/2022 14:16:09'!
test05ProcessLeftWhenHeadingSouthHeadToEast

	|marsRover|
	
	marsRover := self marsRoverAt00HeadingSouth.
	
	self assert:((marsRover process:'l') isAt: 0@0 heading: #East)! !

!MarsRoverTest methodsFor: 'testing' stamp: 'DDO 5/15/2022 14:17:30'!
test06CanNotProcessAnInvalidInstruction

	|marsRover|
	
	marsRover := self marsRoverAt00HeadingSouth.
	
	self
		should: [ marsRover process:'X' ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = MarsRover invalidCommandErrorDescription.
			self assert: (marsRover isAt: 0@0 heading: #South)
			]
! !

!MarsRoverTest methodsFor: 'testing' stamp: 'DDO 5/15/2022 14:16:09'!
test07ProcessForwardTwoTimesWhenHeadingSouthMoveTwoPlaces
		
	|marsRover|
	
	marsRover  := self marsRoverAt00HeadingSouth.
	
	self assert: ((marsRover process:'ff') isAt: 0@-2 heading: #South)! !

!MarsRoverTest methodsFor: 'testing' stamp: 'DDO 5/15/2022 14:16:09'!
test08ProcessBackwardTwoTimesWhenHeadingSouthMoveTwoPlaces
		
	|marsRover|
	
	marsRover  := self marsRoverAt00HeadingSouth.
	
	self assert: ((marsRover process:'bb') isAt: 0@2 heading: #South)! !

!MarsRoverTest methodsFor: 'testing' stamp: 'DDO 5/15/2022 14:28:46'!
test09ProcessRightTwoTimesWhenHeadingSouthHeadToNorth
		
	|marsRover|
	
	marsRover  := self marsRoverAt00HeadingSouth.
	
	self assert: ((marsRover process:'rr') isAt: 0@0 heading: #North)! !

!MarsRoverTest methodsFor: 'testing' stamp: 'DDO 5/15/2022 14:29:00'!
test10ProcessLeftTwoTimesWhenHeadingSouthHeadToNorth
		
	|marsRover|
	
	marsRover  := self marsRoverAt00HeadingSouth.
	
	self assert: ((marsRover process:'ll') isAt: 0@0 heading: #North)! !

!MarsRoverTest methodsFor: 'testing' stamp: 'DDO 5/15/2022 14:33:01'!
test11ProcessLeftTwoTimesWhenHeadingNorthHeadToSouth
		
	|marsRover|
	
	marsRover  := self marsRoverAt00HeadingNorth.
	
	self assert: ((marsRover process:'ll') isAt: 0@0 heading: #South)! !

!MarsRoverTest methodsFor: 'testing' stamp: 'DDO 5/15/2022 14:35:35'!
test12ProcessRightTwoTimesWhenHeadingNorthHeadToSouth
		
	|marsRover|
	
	marsRover  := self marsRoverAt00HeadingNorth.
	
	self assert: ((marsRover process:'rr') isAt: 0@0 heading: #South)! !

!MarsRoverTest methodsFor: 'testing' stamp: 'DDO 5/15/2022 14:51:22'!
test13ProcessFowardWhenHeadingNorthMovesToNorth

	|marsRover|
	
	marsRover := self marsRoverAt00HeadingNorth .
	
	self assert:((marsRover process:'f') isAt: 0@1 heading: #North)! !

!MarsRoverTest methodsFor: 'testing' stamp: 'DDO 5/15/2022 20:32:55'!
test14ProcessFowardWhenHeadingEastMovesToEast

	|marsRover|
	
	marsRover := self marsRoverAt00HeadingEast.
	
	self assert:((marsRover process:'f') isAt: 1@0 heading: #East)! !

!MarsRoverTest methodsFor: 'testing' stamp: 'DDO 5/15/2022 20:33:05'!
test15ProcessFowardWhenHeadingWestMovesToWest

	|marsRover|
	
	marsRover := self marsRoverAt00HeadingWest.
	
	self assert:((marsRover process:'f') isAt: -1@0 heading: #West)! !

!MarsRoverTest methodsFor: 'testing' stamp: 'DDO 5/15/2022 15:39:00'!
test16ProcessBackwardWhenHeadingNorthMovesToSouth

	|marsRover|
	
	marsRover := self marsRoverAt00HeadingNorth .
	
	self assert:((marsRover process:'b') isAt: 0@-1 heading: #North)! !

!MarsRoverTest methodsFor: 'testing' stamp: 'DDO 5/15/2022 15:41:13'!
test17ProcessBackwardWhenHeadingEastMovesToWest

	|marsRover|
	
	marsRover := self marsRoverAt00HeadingEast.
	
	self assert:((marsRover process:'b') isAt: -1@0 heading: #East)! !

!MarsRoverTest methodsFor: 'testing' stamp: 'DDO 5/15/2022 15:47:29'!
test18ProcessBackwardWhenHeadingWestMovesToEast

	|marsRover|
	
	marsRover := self marsRoverAt00HeadingWest.
	
	self assert:((marsRover process:'b') isAt: 1@0 heading: #West)! !


!MarsRoverTest methodsFor: 'setUp/tearDown' stamp: 'DDO 5/15/2022 14:58:40'!
marsRoverAt00HeadingEast
	
	^MarsRover at: 0@0 heading: #East.! !

!MarsRoverTest methodsFor: 'setUp/tearDown' stamp: 'DDO 5/15/2022 14:34:46'!
marsRoverAt00HeadingNorth

	 ^MarsRover at: 0@0 heading:#North.! !

!MarsRoverTest methodsFor: 'setUp/tearDown' stamp: 'DDO 5/15/2022 14:16:09'!
marsRoverAt00HeadingSouth

	^ MarsRover at: 0@0 heading: #South! !

!MarsRoverTest methodsFor: 'setUp/tearDown' stamp: 'DDO 5/15/2022 15:02:06'!
marsRoverAt00HeadingWest
	
	^MarsRover at: 0@0 heading:#West! !


!classDefinition: #MarsRover category: 'MarsRover'!
Object subclass: #MarsRover
	instanceVariableNames: 'position heading'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRover methodsFor: 'instance creation' stamp: 'DDO 5/15/2022 01:45:00'!
initializeAt: aPosition heading: aDirection 

	position := aPosition.
	heading := aDirection.! !


!MarsRover methodsFor: 'processing' stamp: 'DDO 5/15/2022 02:54:54'!
process: aListOfCommands
	
	aListOfCommands do:[:aCommand| self processACommand:aCommand]! !

!MarsRover methodsFor: 'processing' stamp: 'DDO 5/15/2022 20:39:54'!
processACommand: aCommand

	(self isForward: aCommand) ifTrue:[ ^self moveForward].
	(self isBackward: aCommand) ifTrue:[ ^self moveBack].
	(self isRight: aCommand) 			ifTrue:[ ^self turnRight].
	(self isLeft: aCommand) 			ifTrue:[ ^self turnLeft].
	self signalInvalidInstruction! !

!MarsRover methodsFor: 'processing' stamp: 'DDO 5/15/2022 20:39:54'!
signalInvalidInstruction

	^ self error: self class invalidCommandErrorDescription! !


!MarsRover methodsFor: 'testing' stamp: 'DDO 5/15/2022 04:00:53'!
isAt: aPosition heading: aDirection

	^ position = aPosition  and:[ heading isHeading: aDirection ]
	! !


!MarsRover methodsFor: 'rotation private' stamp: 'DDO 5/15/2022 04:24:28'!
turnEast

	heading := MarsRoverHeadingEast new.! !

!MarsRover methodsFor: 'rotation private' stamp: 'DDO 5/15/2022 04:23:07'!
turnNorth

	heading := MarsRoverHeadingNorth new.! !

!MarsRover methodsFor: 'rotation private' stamp: 'DDO 5/15/2022 04:22:42'!
turnSouth

	heading := MarsRoverHeadingSouth new.! !

!MarsRover methodsFor: 'rotation private' stamp: 'DDO 5/15/2022 04:24:16'!
turnWest

	heading := MarsRoverHeadingWest new.! !


!MarsRover methodsFor: 'rotation' stamp: 'DDO 5/15/2022 19:03:54'!
turnLeft

	^ heading turnLeft: self! !

!MarsRover methodsFor: 'rotation' stamp: 'DDO 5/15/2022 19:03:33'!
turnRight

	^ heading turnRight: self! !


!MarsRover methodsFor: 'moving private' stamp: 'DDO 5/15/2022 20:10:19'!
moveEast

	position := position + (1@0)! !

!MarsRover methodsFor: 'moving private' stamp: 'DDO 5/15/2022 20:11:14'!
moveNorth

	position := position + (0@1)! !

!MarsRover methodsFor: 'moving private' stamp: 'DDO 5/15/2022 20:11:23'!
moveSouth

	position := position + (0@-1)! !

!MarsRover methodsFor: 'moving private' stamp: 'DDO 5/15/2022 20:10:51'!
moveWest

	position := position + (-1@0)! !


!MarsRover methodsFor: 'processing private' stamp: 'DDO 5/15/2022 20:39:02'!
isBackward: aCommand

	^ aCommand = $b! !

!MarsRover methodsFor: 'processing private' stamp: 'DDO 5/15/2022 20:41:29'!
isForward: aCommand

	^ aCommand = $f! !

!MarsRover methodsFor: 'processing private' stamp: 'DDO 5/15/2022 20:39:34'!
isLeft: aCommand

	^ aCommand = $l! !

!MarsRover methodsFor: 'processing private' stamp: 'DDO 5/15/2022 20:39:17'!
isRight: aCommand

	^ aCommand = $r! !


!MarsRover methodsFor: 'moving' stamp: 'DDO 5/15/2022 20:31:34'!
moveBack

	^heading moveBack:self.! !

!MarsRover methodsFor: 'moving' stamp: 'DDO 5/15/2022 20:31:25'!
moveForward

	^heading moveForward:self.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRover class' category: 'MarsRover'!
MarsRover class
	instanceVariableNames: ''!

!MarsRover class methodsFor: 'instance creation' stamp: 'DDO 5/15/2022 03:34:59'!
at: aPosition heading: aDirection
	
	^self new initializeAt: aPosition heading: (MarsRoverHeading heading: aDirection)! !


!MarsRover class methodsFor: 'errors descriptions' stamp: 'DDO 5/15/2022 02:26:54'!
invalidCommandErrorDescription

	^'Can not process an invalid instruction'! !


!classDefinition: #MarsRoverHeading category: 'MarsRover'!
Object subclass: #MarsRoverHeading
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRoverHeading methodsFor: 'rotating' stamp: 'DDO 5/15/2022 17:08:44'!
turnLeft: aMarsRover

	^self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'rotating' stamp: 'DDO 5/15/2022 17:08:55'!
turnRight: aMarsRover

	^self subclassResponsibility ! !


!MarsRoverHeading methodsFor: 'accessing' stamp: 'DDO 5/15/2022 03:58:50'!
isHeading: aDirection

	^self class canHandle: aDirection! !


!MarsRoverHeading methodsFor: 'moving' stamp: 'DDO 5/15/2022 20:23:10'!
moveBack: aMarsRover

	^self subclassResponsibility ! !

!MarsRoverHeading methodsFor: 'moving' stamp: 'DDO 5/15/2022 20:26:46'!
moveForward: aMarsRover

	^self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverHeading class' category: 'MarsRover'!
MarsRoverHeading class
	instanceVariableNames: ''!

!MarsRoverHeading class methodsFor: 'instance creation' stamp: 'DDO 5/15/2022 03:39:05'!
canHandle: aDirection

	^self subclassResponsibility.! !

!MarsRoverHeading class methodsFor: 'instance creation' stamp: 'DDO 5/15/2022 03:44:43'!
heading: aDirection

	^(MarsRoverHeading allSubclasses detect:[:aHeading | aHeading canHandle: aDirection ]) new! !


!classDefinition: #MarsRoverHeadingEast category: 'MarsRover'!
MarsRoverHeading subclass: #MarsRoverHeadingEast
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRoverHeadingEast methodsFor: 'rotating' stamp: 'DDO 5/15/2022 04:28:37'!
turnLeft: aMarsRover

	^aMarsRover turnNorth! !

!MarsRoverHeadingEast methodsFor: 'rotating' stamp: 'DDO 5/15/2022 04:06:35'!
turnRight: aMarsRover

	^aMarsRover turnSouth! !


!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'DDO 5/15/2022 20:23:33'!
moveBack: aMarsRover

	^ aMarsRover moveWest.! !

!MarsRoverHeadingEast methodsFor: 'moving' stamp: 'DDO 5/15/2022 20:27:38'!
moveForward: aMarsRover

	^ aMarsRover moveEast.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverHeadingEast class' category: 'MarsRover'!
MarsRoverHeadingEast class
	instanceVariableNames: ''!

!MarsRoverHeadingEast class methodsFor: 'instance creation' stamp: 'DDO 5/15/2022 03:47:34'!
canHandle: aDirection

	^ aDirection = #East! !


!classDefinition: #MarsRoverHeadingNorth category: 'MarsRover'!
MarsRoverHeading subclass: #MarsRoverHeadingNorth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRoverHeadingNorth methodsFor: 'rotating' stamp: 'DDO 5/15/2022 04:28:14'!
turnLeft: aMarsRover

	^aMarsRover turnWest! !

!MarsRoverHeadingNorth methodsFor: 'rotating' stamp: 'DDO 5/15/2022 04:20:15'!
turnRight: aMarsRover

	^aMarsRover turnEast! !


!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'DDO 5/15/2022 20:23:48'!
moveBack: aMarsRover

	^ aMarsRover moveSouth.! !

!MarsRoverHeadingNorth methodsFor: 'moving' stamp: 'DDO 5/15/2022 20:27:38'!
moveForward: aMarsRover

	^ aMarsRover moveNorth.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverHeadingNorth class' category: 'MarsRover'!
MarsRoverHeadingNorth class
	instanceVariableNames: ''!

!MarsRoverHeadingNorth class methodsFor: 'instance creation' stamp: 'DDO 5/15/2022 03:47:52'!
canHandle: aDirection

	^ aDirection = #North! !


!classDefinition: #MarsRoverHeadingSouth category: 'MarsRover'!
MarsRoverHeading subclass: #MarsRoverHeadingSouth
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRoverHeadingSouth methodsFor: 'rotation' stamp: 'DDO 5/15/2022 04:27:56'!
turnLeft: aMarsRover

	^aMarsRover turnEast! !

!MarsRoverHeadingSouth methodsFor: 'rotation' stamp: 'DDO 5/15/2022 04:20:40'!
turnRight: aMarsRover

	^aMarsRover turnWest! !


!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'DDO 5/15/2022 20:24:07'!
moveBack: aMarsRover

	^ aMarsRover moveNorth.! !

!MarsRoverHeadingSouth methodsFor: 'moving' stamp: 'DDO 5/15/2022 20:27:38'!
moveForward: aMarsRover

	^ aMarsRover moveSouth.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverHeadingSouth class' category: 'MarsRover'!
MarsRoverHeadingSouth class
	instanceVariableNames: ''!

!MarsRoverHeadingSouth class methodsFor: 'instance creation' stamp: 'DDO 5/15/2022 03:48:07'!
canHandle: aDirection

	^ aDirection = #South! !


!classDefinition: #MarsRoverHeadingWest category: 'MarsRover'!
MarsRoverHeading subclass: #MarsRoverHeadingWest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'MarsRover'!

!MarsRoverHeadingWest methodsFor: 'rotating' stamp: 'DDO 5/15/2022 04:27:01'!
turnLeft: aMarsRover

	^aMarsRover turnSouth! !

!MarsRoverHeadingWest methodsFor: 'rotating' stamp: 'DDO 5/15/2022 04:21:12'!
turnRight: aMarsRover

	^aMarsRover turnNorth! !


!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'DDO 5/15/2022 20:24:20'!
moveBack: aMarsRover

	^ aMarsRover moveEast! !

!MarsRoverHeadingWest methodsFor: 'moving' stamp: 'DDO 5/15/2022 20:27:38'!
moveForward: aMarsRover

	^ aMarsRover moveWest.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'MarsRoverHeadingWest class' category: 'MarsRover'!
MarsRoverHeadingWest class
	instanceVariableNames: ''!

!MarsRoverHeadingWest class methodsFor: 'instance creation' stamp: 'DDO 5/15/2022 03:48:22'!
canHandle: aDirection

	^ aDirection = #West! !
