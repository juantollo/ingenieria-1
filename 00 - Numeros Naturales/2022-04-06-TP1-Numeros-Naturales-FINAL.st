!classDefinition: #I category: 'Numeros-Naturales'!
DenotativeObject subclass: #I
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Naturales'!
!I commentStamp: '<historical>' prior: 0!
III - II!


"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'I class' category: 'Numeros-Naturales'!
I class
	instanceVariableNames: ''!

!I class methodsFor: 'next & previous' stamp: 'ARM 3/31/2022 21:18:47'!
next

	^II! !

!I class methodsFor: 'next & previous' stamp: 'JDS 4/4/2022 21:18:43'!
previous

	^ O! !


!I class methodsFor: 'operations' stamp: 'JDS 4/4/2022 14:38:10'!
* aNatural

	^aNatural! !

!I class methodsFor: 'operations' stamp: 'ARM 3/31/2022 21:19:14'!
+ aNatural

	^aNatural next! !

!I class methodsFor: 'operations' stamp: 'JDS 4/6/2022 10:18:58'!
- aSubstrahend

	^ aSubstrahend substractTo: self! !

!I class methodsFor: 'operations' stamp: 'JDS 4/6/2022 10:19:45'!
/ aDivider

	^ aDivider divideToOne
	! !


!I class methodsFor: 'private' stamp: 'JDS 4/6/2022 10:18:42'!
divideTo: aDividend

	^ aDividend! !

!I class methodsFor: 'private' stamp: 'JDS 4/6/2022 08:20:24'!
divideToOne

	^ self! !

!I class methodsFor: 'private' stamp: 'JDS 4/6/2022 10:35:08'!
negated

	"Usado para resolver la divisi�n recursiva en el caso que el divisor es mayor al dividendo"

	^ NegativeNatural! !

!I class methodsFor: 'private' stamp: 'JDS 4/6/2022 10:18:29'!
substractTo: aMinuend

	^ aMinuend previous! !


!classDefinition: #II category: 'Numeros-Naturales'!
DenotativeObject subclass: #II
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'II class' category: 'Numeros-Naturales'!
II class
	instanceVariableNames: 'next previous'!

!II class methodsFor: 'operations' stamp: 'JDS 4/6/2022 10:04:42'!
* aNatural

	^previous * aNatural + aNatural! !

!II class methodsFor: 'operations' stamp: 'JDS 4/6/2022 10:04:34'!
+ aNatural

	^previous + aNatural next! !

!II class methodsFor: 'operations' stamp: 'JDS 4/6/2022 10:04:23'!
- aSubstrahend

	^ aSubstrahend substractTo: self
! !

!II class methodsFor: 'operations' stamp: 'JDS 4/6/2022 10:20:14'!
/ aDivider

	^ aDivider divideTo: self! !


!II class methodsFor: 'next & previous' stamp: 'JDS 4/6/2022 10:06:43'!
next

	next ifNotNil:[^next].
	next _ II createChildNamed: (self name , 'I').
	next previous: self.
	^next! !

!II class methodsFor: 'next & previous' stamp: 'JDS 4/6/2022 10:07:11'!
previous

	^ previous! !

!II class methodsFor: 'next & previous' stamp: 'JDS 4/6/2022 10:06:58'!
previous: aNatural 
	
	previous _ aNatural! !


!II class methodsFor: 'private' stamp: 'JDS 4/6/2022 10:51:59'!
divideTo: aDividend
	
	^ ((aDividend - self) / self) + I! !

!II class methodsFor: 'private' stamp: 'JDS 4/6/2022 08:21:30'!
divideToOne

	^ O! !

!II class methodsFor: 'private' stamp: 'JDS 4/6/2022 10:35:30'!
negated

	"Usado para resolver la divisi�n recursiva en el caso que el divisor es mayor al dividendo"

	^NegativeNatural! !

!II class methodsFor: 'private' stamp: 'JDS 4/6/2022 10:16:37'!
substractTo: aMinuend

	^aMinuend previous - previous! !


!II class methodsFor: '--** private fileout/in **--' stamp: 'JDS 4/6/2022 11:14:38'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := III.
	previous := I.! !


!classDefinition: #III category: 'Numeros-Naturales'!
II subclass: #III
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'III class' category: 'Numeros-Naturales'!
III class
	instanceVariableNames: ''!

!III class methodsFor: '--** private fileout/in **--' stamp: 'JDS 4/6/2022 11:14:38'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIII.
	previous := II.! !


!classDefinition: #IIII category: 'Numeros-Naturales'!
II subclass: #IIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIII class' category: 'Numeros-Naturales'!
IIII class
	instanceVariableNames: ''!

!IIII class methodsFor: '--** private fileout/in **--' stamp: 'JDS 4/6/2022 11:14:39'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIIII.
	previous := III.! !


!classDefinition: #IIIII category: 'Numeros-Naturales'!
II subclass: #IIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIII class' category: 'Numeros-Naturales'!
IIIII class
	instanceVariableNames: ''!

!IIIII class methodsFor: '--** private fileout/in **--' stamp: 'JDS 4/6/2022 11:14:39'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIIIII.
	previous := IIII.! !


!classDefinition: #IIIIII category: 'Numeros-Naturales'!
II subclass: #IIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIII class' category: 'Numeros-Naturales'!
IIIIII class
	instanceVariableNames: ''!

!IIIIII class methodsFor: '--** private fileout/in **--' stamp: 'JDS 4/6/2022 11:14:39'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIIIIII.
	previous := IIIII.! !


!classDefinition: #IIIIIII category: 'Numeros-Naturales'!
II subclass: #IIIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIIII class' category: 'Numeros-Naturales'!
IIIIIII class
	instanceVariableNames: ''!

!IIIIIII class methodsFor: '--** private fileout/in **--' stamp: 'JDS 4/6/2022 11:14:39'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIIIIIII.
	previous := IIIIII.! !


!classDefinition: #IIIIIIII category: 'Numeros-Naturales'!
II subclass: #IIIIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIIIII class' category: 'Numeros-Naturales'!
IIIIIIII class
	instanceVariableNames: ''!

!IIIIIIII class methodsFor: '--** private fileout/in **--' stamp: 'JDS 4/6/2022 11:14:39'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IIIIIIIII.
	previous := IIIIIII.! !


!classDefinition: #IIIIIIIII category: 'Numeros-Naturales'!
II subclass: #IIIIIIIII
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IIIIIIIII class' category: 'Numeros-Naturales'!
IIIIIIIII class
	instanceVariableNames: ''!

!IIIIIIIII class methodsFor: '--** private fileout/in **--' stamp: 'JDS 4/6/2022 11:14:39'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := nil.
	previous := IIIIIIII.! !


!classDefinition: #NegativeNatural category: 'Numeros-Naturales'!
DenotativeObject subclass: #NegativeNatural
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'NegativeNatural class' category: 'Numeros-Naturales'!
NegativeNatural class
	instanceVariableNames: ''!

!NegativeNatural class methodsFor: 'private' stamp: 'JDS 4/6/2022 10:52:48'!
+ aNatural

	"Solo usado para resolver el caso base de la divisi�n recursiva"

	^ O! !

!NegativeNatural class methodsFor: 'private' stamp: 'JDS 4/6/2022 10:24:28'!
/ aDivider

	"Solo usado para resolver el caso base de la divisi�n recursiva"

	^ self! !


!classDefinition: #O category: 'Numeros-Naturales'!
DenotativeObject subclass: #O
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Naturales'!
!O commentStamp: '<historical>' prior: 0!
III - II!


"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'O class' category: 'Numeros-Naturales'!
O class
	instanceVariableNames: ''!

!O class methodsFor: 'next' stamp: 'JDS 4/6/2022 09:07:25'!
next

	^ I! !


!O class methodsFor: 'operations' stamp: 'JDS 4/4/2022 21:13:04'!
* aNatural

	^ self! !

!O class methodsFor: 'operations' stamp: 'JDS 4/4/2022 21:12:43'!
+ aNatural

	^ aNatural! !

!O class methodsFor: 'operations' stamp: 'JDS 4/6/2022 10:03:21'!
- aSubstrahend

	^ aSubstrahend negated! !

!O class methodsFor: 'operations' stamp: 'JDS 4/6/2022 10:54:32'!
/ aDivider

	^ aDivider divideTo: self	! !


!O class methodsFor: 'private' stamp: 'JDS 4/6/2022 10:15:04'!
divideTo: aDividend

	self error: 'No se puede dividir por cero!!!!!!'
	! !

!O class methodsFor: 'private' stamp: 'JDS 4/6/2022 10:49:02'!
divideToOne

	self error: 'No se puede dividir por cero!!!!!!'
	! !

!O class methodsFor: 'private' stamp: 'JDS 4/6/2022 10:32:10'!
negated
 
	"Usado para resolver la divisi�n recursiva en el caso que el divisor y el dividendo son iguales"

	^ self! !

!O class methodsFor: 'private' stamp: 'JDS 4/6/2022 10:16:07'!
substractTo: aMinuend

	^ aMinuend! !

II initializeAfterFileIn!
III initializeAfterFileIn!
IIII initializeAfterFileIn!
IIIII initializeAfterFileIn!
IIIIII initializeAfterFileIn!
IIIIIII initializeAfterFileIn!
IIIIIIII initializeAfterFileIn!
IIIIIIIII initializeAfterFileIn!