!classDefinition: #I category: 'Numeros-Naturales'!
DenotativeObject subclass: #I
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Naturales'!
!I commentStamp: '<historical>' prior: 0!
III - II!


"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'I class' category: 'Numeros-Naturales'!
I class
	instanceVariableNames: ''!

!I class methodsFor: 'next & previous' stamp: 'ARM 3/31/2022 21:18:47'!
next

	^II! !

!I class methodsFor: 'next & previous' stamp: 'JDS 4/4/2022 21:18:43'!
previous

	^ O! !


!I class methodsFor: 'operations' stamp: 'ARM 4/6/2022 12:10:21'!
* multiplicando

	^multiplicando! !

!I class methodsFor: 'operations' stamp: 'ARM 4/6/2022 12:10:29'!
+ sumando

	^sumando next! !

!I class methodsFor: 'operations' stamp: 'ARM 4/6/2022 12:01:38'!
- aSubstrahend

	^ aSubstrahend substractToOne! !

!I class methodsFor: 'operations' stamp: 'ARM 4/6/2022 12:18:05'!
/ divisor

	^ divisor divideOne
	! !


!I class methodsFor: 'error description' stamp: 'ARM 4/6/2022 12:20:35'!
negativeNumbersNotSupportedErrorDescription
	^II negativeNumbersNotSupportedErrorDescription! !


!I class methodsFor: 'private' stamp: 'ARM 4/6/2022 12:18:05'!
divideOne

	^ self! !

!I class methodsFor: 'private' stamp: 'JDS 4/6/2022 10:18:42'!
divideTo: aDividend

	^ aDividend! !

!I class methodsFor: 'private' stamp: 'ARM 4/6/2022 12:19:09'!
divideZero

	^O! !

!I class methodsFor: 'private' stamp: 'JDS 4/6/2022 10:18:29'!
substractTo: aMinuend

	^ aMinuend previous! !

!I class methodsFor: 'private' stamp: 'ARM 4/6/2022 12:01:59'!
substractToOne

	^ O! !

!I class methodsFor: 'private' stamp: 'ARM 4/6/2022 12:20:06'!
substractToZero

	^ self error: II negativeNumbersNotSupportedErrorDescription! !


!classDefinition: #II category: 'Numeros-Naturales'!
DenotativeObject subclass: #II
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'II class' category: 'Numeros-Naturales'!
II class
	instanceVariableNames: 'next previous'!

!II class methodsFor: 'operations' stamp: 'ARM 4/6/2022 12:10:10'!
* multiplicando

	^previous * multiplicando + multiplicando! !

!II class methodsFor: 'operations' stamp: 'ARM 4/6/2022 12:10:00'!
+ sumando

	^previous + sumando next! !

!II class methodsFor: 'operations' stamp: 'JDS 4/6/2022 10:04:23'!
- aSubstrahend

	^ aSubstrahend substractTo: self
! !

!II class methodsFor: 'operations' stamp: 'ARM 4/6/2022 12:09:47'!
/ divisor

	^ divisor divideTo: self! !


!II class methodsFor: 'next & previous' stamp: 'ddo 4/10/2022 10:22:05'!
nameOfNext

	(self name endsWith: 'III') ifTrue:[^(self name withoutSuffix: 'III'), 'IV'].
	(self name endsWith: 'IV') ifTrue:[^(self name withoutSuffix: 'IV'), 'V'].
	(self name endsWith: 'VIII') ifTrue:[^(self name withoutSuffix: 'VIII'), 'IX'].
	(self name endsWith: 'IX') ifTrue:[^(self name withoutSuffix: 'IX'), 'X'].
	(self name endsWith: 'XXXIX') ifTrue:[^(self name withoutSuffix: 'XXXIX'), 'XL'].
	(self name endsWith: 'XLIX') ifTrue:[^(self name withoutSuffix: 'XLIX'), 'L'].
	(self name endsWith: 'LXXXIX') ifTrue:[^(self name withoutSuffix: 'LXXXIX'), 'XC'].
	(self name endsWith: 'XCIX') ifTrue:[^(self name withoutSuffix: 'XCIX'), 'C'].
	(self name endsWith: 'CCCXCIX') ifTrue:[^(self name withoutSuffix: 'CCCXCIX'), 'CD'].
	(self name endsWith: 'CDXCIX') ifTrue:[^(self name withoutSuffix: 'CDXCIX'), 'D'].
	
	^ self name, 'I'! !

!II class methodsFor: 'next & previous' stamp: 'ARM 4/6/2022 12:12:04'!
next

	next ifNotNil:[^next].
	next _ II createChildNamed: self nameOfNext.
	next previous: self.
	^next! !

!II class methodsFor: 'next & previous' stamp: 'JDS 4/6/2022 10:07:11'!
previous

	^ previous! !

!II class methodsFor: 'next & previous' stamp: 'JDS 4/6/2022 10:06:58'!
previous: aNatural 
	
	previous _ aNatural! !


!II class methodsFor: 'remove next' stamp: 'ARM 4/6/2022 12:13:40'!
removeAllNext

	next ifNotNil:
	[
		next removeAllNext.
		next removeFromSystem.
		next _ nil.
	]! !


!II class methodsFor: 'error description' stamp: 'ARM 4/6/2022 12:20:06'!
negativeNumbersNotSupportedErrorDescription

	^ 'El sustraendo no puede ser mayor que el minuendo'! !


!II class methodsFor: 'private' stamp: 'ARM 4/6/2022 12:18:05'!
divideOne

	^ O! !

!II class methodsFor: 'private' stamp: 'JDS 4/6/2022 10:51:59'!
divideTo: aDividend
	
	^ ((aDividend - self) / self) + I! !

!II class methodsFor: 'private' stamp: 'ARM 4/6/2022 12:18:47'!
divideZero
	^O! !

!II class methodsFor: 'private' stamp: 'JDS 4/6/2022 10:16:37'!
substractTo: aMinuend

	^aMinuend previous - previous! !

!II class methodsFor: 'private' stamp: 'ARM 4/6/2022 12:20:54'!
substractToOne

	^ self error: self negativeNumbersNotSupportedErrorDescription! !

!II class methodsFor: 'private' stamp: 'ARM 4/6/2022 12:20:06'!
substractToZero

	^ self error: self negativeNumbersNotSupportedErrorDescription! !


!II class methodsFor: '--** private fileout/in **--' stamp: 'ddo 4/10/2022 10:22:27'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := III.
	previous := I.! !


!classDefinition: #III category: 'Numeros-Naturales'!
II subclass: #III
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'III class' category: 'Numeros-Naturales'!
III class
	instanceVariableNames: ''!

!III class methodsFor: '--** private fileout/in **--' stamp: 'ddo 4/10/2022 10:22:27'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := IV.
	previous := II.! !


!classDefinition: #IV category: 'Numeros-Naturales'!
II subclass: #IV
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Naturales'!

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'IV class' category: 'Numeros-Naturales'!
IV class
	instanceVariableNames: ''!

!IV class methodsFor: '--** private fileout/in **--' stamp: 'ddo 4/10/2022 10:22:28'!
initializeCollaboratorsFromFile
	"Auto generated method for loading purposes - Do not modify it"

	next := nil.
	previous := III.! !


!classDefinition: #O category: 'Numeros-Naturales'!
DenotativeObject subclass: #O
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Numeros-Naturales'!
!O commentStamp: '<historical>' prior: 0!
III - II!


"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'O class' category: 'Numeros-Naturales'!
O class
	instanceVariableNames: ''!

!O class methodsFor: 'next' stamp: 'JDS 4/6/2022 09:07:25'!
next

	^ I! !


!O class methodsFor: 'operations' stamp: 'ARM 4/6/2022 12:11:03'!
* multiplicando

	^ self! !

!O class methodsFor: 'operations' stamp: 'ARM 4/6/2022 12:10:57'!
+ sumando

	^ sumando! !

!O class methodsFor: 'operations' stamp: 'ARM 4/6/2022 12:05:32'!
- aSubstrahend

	^aSubstrahend substractToZero! !

!O class methodsFor: 'operations' stamp: 'ARM 4/6/2022 12:17:44'!
/ divisor

	^ divisor divideZero! !


!O class methodsFor: 'error description' stamp: 'ARM 4/6/2022 12:23:46'!
canNotDivideByZeroErrorDescription

	^ 'No se puede dividir por cero!!!!!!'! !


!O class methodsFor: 'private' stamp: 'ARM 4/6/2022 12:23:46'!
divideOne

	self error: self canNotDivideByZeroErrorDescription
	! !

!O class methodsFor: 'private' stamp: 'ARM 4/6/2022 12:23:46'!
divideTo: aDividend

	self error: self canNotDivideByZeroErrorDescription
	! !

!O class methodsFor: 'private' stamp: 'ARM 4/6/2022 12:22:02'!
divideZero

	self error: self canNotDivideByBiggerNumberErrorDescription
	! !

!O class methodsFor: 'private' stamp: 'JDS 4/6/2022 10:16:07'!
substractTo: aMinuend

	^ aMinuend! !

!O class methodsFor: 'private' stamp: 'ARM 4/6/2022 12:11:20'!
substractToOne

	^I! !

!O class methodsFor: 'private' stamp: 'ARM 4/6/2022 12:05:15'!
substractToZero

	^self

	! !

II initializeAfterFileIn!
III initializeAfterFileIn!
IV initializeAfterFileIn!