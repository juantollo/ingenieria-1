!classDefinition: #TDDBabyStepsRot13Test category: 'TDDBabyStepsGameRot13'!
TestCase subclass: #TDDBabyStepsRot13Test
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TDDBabyStepsGameRot13'!

!TDDBabyStepsRot13Test methodsFor: 'tests' stamp: 'ddo 5/9/2022 17:47:24'!
test01TheEmptyStringDoesntChange
	
	self assert: '' equals: '' asRot13
	! !

!TDDBabyStepsRot13Test methodsFor: 'tests' stamp: 'ddo 5/9/2022 17:51:09'!
test02CorrectlyEncodesALowercaseOneCharStringFromTheFirstPartOfTheAlphabet
	
	self assert: 'n' equals: 'a' asRot13
	! !

!TDDBabyStepsRot13Test methodsFor: 'tests' stamp: 'ddo 5/9/2022 17:58:37'!
test03CorrectlyEncodesALowercaseOneCharStringFromTheFirstPartOfTheAlphabet
	
	self assert: 'z' equals: 'm' asRot13
	! !

!TDDBabyStepsRot13Test methodsFor: 'tests' stamp: 'ddo 5/9/2022 18:00:15'!
test04CorrectlyEncodesALowercaseOneCharStringFromTheSecondPartOfTheAlphabet
	
	self assert: 'a' equals: 'n' asRot13
	! !

!TDDBabyStepsRot13Test methodsFor: 'tests' stamp: 'ddo 5/9/2022 18:01:28'!
test05CorrectlyEncodesALowercaseOneCharStringFromTheSecondPartOfTheAlphabet
	
	self assert: 'm' equals: 'z' asRot13
	! !

!TDDBabyStepsRot13Test methodsFor: 'tests' stamp: 'ddo 5/9/2022 18:02:31'!
test06CorrectlyEncodesAnUppercaseOneCharStringFromTheFirstPartOfTheAlphabet
	
	self assert: 'N' equals: 'A' asRot13.
	self assert: 'Z' equals: 'M' asRot13
	! !

!TDDBabyStepsRot13Test methodsFor: 'tests' stamp: 'ddo 5/9/2022 18:36:35'!
test07CorrectlyEncodesAnUppercaseOneCharStringFromTheSecondPartOfTheAlphabet
	
	self assert: 'A' equals: 'N' asRot13.
	self assert: 'M' equals: 'Z' asRot13
	! !

!TDDBabyStepsRot13Test methodsFor: 'tests' stamp: 'ddo 5/9/2022 18:48:16'!
test08CorrectlyEncodesATwoCharStringWithNoSpecialCharacters
	
	self assert: 'nm' equals: 'az' asRot13
	! !

!TDDBabyStepsRot13Test methodsFor: 'tests' stamp: 'ddo 5/9/2022 18:56:22'!
test09CorrectlyEncodesMoreThanTwoCharStringWithNoSpecialCharacters
	
	self assert: 'uByN' equals: 'hOlA' asRot13
	! !

!TDDBabyStepsRot13Test methodsFor: 'tests' stamp: 'ddo 5/9/2022 18:58:29'!
test10CorrectlyEncodesSpecialCharactersWithNoChange
	
	self assert: '&�n%( �4' equals: '&�a%( �4' asRot13
	! !

!TDDBabyStepsRot13Test methodsFor: 'tests' stamp: 'ddo 5/9/2022 19:42:38'!
testExtraToCheckGeneralizationOnKata
		
	self assert: 'Ju3er j3 4e3 t01at j3 q0ag a3rq e04q$...' equals: 'Wh3re w3 4r3 g01ng w3 d0nt n3ed r04d$...' asRot13
	! !
