!classDefinition: #CartTest category: 'TusLibros'!
TestCase subclass: #CartTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'as yet unclassified' stamp: 'DDO 6/3/2022 14:02:33'!
createEmptyCartWith2BooksInCatalogue

	| catalogue |
	catalogue := OrderedCollection new.
	catalogue add:1.
	catalogue add:2.
	
	^Cart for: catalogue.! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'DDO 6/3/2022 14:02:52'!
test01carIsEmptyWhenCreated

	|aCart|
	
	aCart := self createEmptyCartWith2BooksInCatalogue.
	
	self assert: aCart isEmpty! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'DDO 6/3/2022 14:02:33'!
test02aCartWithBooksIsNotEmpty

	|aCart |
	
	aCart := self createEmptyCartWith2BooksInCatalogue.
	aCart add: 1.
	
	self deny: aCart isEmpty! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'DDO 6/3/2022 14:02:33'!
test03canNotAddBooksWhichAreNotListedInCatalogue

	|aCart aIsbn| 
	
	aCart := self createEmptyCartWith2BooksInCatalogue.
	aIsbn := 3.
	
	self 
		should: [ aCart add: aIsbn]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText equals: aCart invalidISBNErrorMessage.
			self assert: aCart isEmpty]! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'DDO 6/3/2022 20:16:44'!
test04canAddManyOfTheSameBook

	|aCart| 
	
	aCart := self createEmptyCartWith2BooksInCatalogue.
	
	aCart add: 1 times: 2.
	
	self assert: 2 equals: (aCart occurrencesOf: 1).! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'DDO 6/3/2022 14:44:01'!
test05canNotAddZeroOrNegativeNumberOfItems

	|aCart| 
	
	aCart := self createEmptyCartWith2BooksInCatalogue.
	
	self should:[aCart add: 1 times: 0]  
		raise: Error-MessageNotUnderstood 
		withExceptionDo: [:anError| 
			self assert:  anError messageText equals: aCart invalidItemAmountErrorMessage.
			self assert: aCart isEmpty ]! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'DDO 6/3/2022 15:03:09'!
test06cartNotIncludeNonAddedItems

	|aCart| 
	
	aCart := self createEmptyCartWith2BooksInCatalogue.
	
	self deny: (aCart includes:1)! !

!CartTest methodsFor: 'as yet unclassified' stamp: 'DDO 6/3/2022 20:16:44'!
test07canConsultAmountOfAddedItems

	|aCart| 
	
	aCart := self createEmptyCartWith2BooksInCatalogue.
	
	aCart add:1 times: 	3.
	
	self assert: 3 equals: (aCart occurrencesOf: 1).
	! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'items catalogue'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'category-name' stamp: 'DDO 6/3/2022 14:17:28'!
add: anItem
	
	self assertCatalogIncludesItem: anItem.

	items add: anItem! !

!Cart methodsFor: 'category-name' stamp: 'DDO 6/3/2022 14:17:28'!
assertCatalogIncludesItem: anItem

	^ (catalogue includes: anItem) ifFalse: [ self error: self invalidISBNErrorMessage ]! !

!Cart methodsFor: 'category-name' stamp: 'DDO 6/3/2022 13:28:20'!
initializeFor:aCatalogue

	items := OrderedCollection new.
	
	catalogue:= aCatalogue.
	! !

!Cart methodsFor: 'category-name' stamp: 'DDO 6/3/2022 13:58:01'!
invalidISBNErrorMessage
	
	^'Invalid ISBN'! !

!Cart methodsFor: 'category-name' stamp: 'DDO 6/3/2022 13:28:20'!
isEmpty
	
	^items isEmpty.! !


!Cart methodsFor: 'accessing' stamp: 'DDO 6/3/2022 14:50:06'!
add: anItem times: aQuantity
	
	| counter |
	
	self assertQuantityPositive: aQuantity.
	
	counter := aQuantity.
	[counter > 0] whileTrue:[self add: anItem. 
					counter := counter -1]
	! !

!Cart methodsFor: 'accessing' stamp: 'DDO 6/3/2022 14:55:46'!
assertQuantityPositive: aQuantity

	^ (aQuantity > 0) ifFalse:[self error: self invalidItemAmountErrorMessage]! !

!Cart methodsFor: 'accessing' stamp: 'DDO 6/3/2022 15:05:10'!
includes: anItem

	^items includes:anItem! !

!Cart methodsFor: 'accessing' stamp: 'DDO 6/3/2022 14:49:22'!
invalidItemAmountErrorMessage

	^'Can Not Add a non positive amount of Item'! !

!Cart methodsFor: 'accessing' stamp: 'DDO 6/3/2022 20:16:44'!
occurrencesOf: anItem
	
	^items occurrencesOf: anItem ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'as yet unclassified' stamp: 'ASDF 6/2/2022 21:40:22'!
for: aCatalague

	^self new initializeFor: aCatalague ! !
