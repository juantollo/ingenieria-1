!classDefinition: #CartTest category: 'TusLibros'!
TestCase subclass: #CartTest
	instanceVariableNames: 'testsObjects'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'tests' stamp: 'DDO 6/8/2022 01:10:07'!
test01NewCartsAreCreatedEmpty

	self assert: testsObjects createCart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'DDO 6/8/2022 01:10:36'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := testsObjects createCart.
	
	self 
		should: [ cart add: testsObjects itemNotSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'DDO 6/9/2022 00:08:45'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := testsObjects createCart.
	
	cart add: testsObjects item1SellByTheStore.
	self deny: cart isEmpty ! !

!CartTest methodsFor: 'tests' stamp: 'DDO 6/9/2022 00:08:45'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := testsObjects createCart.
	
	self 
		should: [cart add: 0 of: testsObjects item1SellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'DDO 6/8/2022 01:11:37'!
test05CanNotAddMoreThanOneItemNotSellByTheStore

	| cart |
	
	cart := testsObjects createCart.
	
	self 
		should: [cart add: 2 of: testsObjects itemNotSellByTheStore  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'DDO 6/9/2022 00:08:45'!
test06CartRemembersAddedItems

	| cart |
	
	cart := testsObjects createCart.
	
	cart add: testsObjects item1SellByTheStore.
	self assert: (cart includes: testsObjects item1SellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'DDO 6/9/2022 00:08:45'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := testsObjects createCart.
	
	self deny: (cart includes: testsObjects item1SellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'DDO 6/9/2022 00:08:45'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := testsObjects createCart.
	
	cart add: 2 of: testsObjects item1SellByTheStore.
	self assert: (cart occurrencesOf: testsObjects item1SellByTheStore) = 2! !


!CartTest methodsFor: 'setUp' stamp: 'DDO 6/8/2022 21:39:25'!
setUp

	testsObjects := TestsObjects new.! !


!classDefinition: #CheckOutTest category: 'TusLibros'!
TestCase subclass: #CheckOutTest
	instanceVariableNames: 'testsObjects'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CheckOutTest methodsFor: 'testing' stamp: 'ddo 6/12/2022 23:22:39'!
test01canNotCheckoutAnEmptyCart
	
	self 
		should: testsObjects createCashier
		checkout: testsObjects createCart
		debitOn: testsObjects createValidCreditCard 
		through: testsObjects merchantProcessor 
		on: testsObjects currentMonthAnYear 
		failWith:[Cashier canNotCheckoutEmptyCartErrorMessage] 
! !

!CheckOutTest methodsFor: 'testing' stamp: 'ddo 6/12/2022 23:22:39'!
test02checkoutCartWithABook
	"fusionado con test 08"
	
	self
		assertPrice: [testsObjects validBook1Price] 
		equalsCashier: testsObjects createCashier
		checkout: testsObjects createCartWithValidBook1 
		debitOn:  testsObjects createValidCreditCard 
		through: testsObjects merchantProcessor 
		on: testsObjects currentMonthAnYear 
! !

!CheckOutTest methodsFor: 'testing' stamp: 'ddo 6/12/2022 23:22:39'!
test03checkoutCartWithMultiplyBooks
	
	self
		assertPrice: [testsObjects validBook1PlusValidBook2Price] 
		equalsCashier:  testsObjects createCashier 
		checkout:  testsObjects createCartWithValidBook1AndValidBook2 
		debitOn:  testsObjects createValidCreditCard 
		through: testsObjects merchantProcessor 
		on: testsObjects currentMonthAnYear ! !

!CheckOutTest methodsFor: 'testing' stamp: 'ddo 6/12/2022 23:22:39'!
test04canNotCheckoutCartWithAnExpiredCreditCard
	
	self 
		should: testsObjects createCashier 
		checkout: testsObjects createCartWithValidBook1 
		debitOn: testsObjects createExpiredCreditCard 
		through: testsObjects merchantProcessor 
		on: testsObjects currentMonthAnYear 
		failWith:[Cashier canNotCheckoutExpiredCreditCardErrorMessage] .
! !

!CheckOutTest methodsFor: 'testing' stamp: 'ddo 6/12/2022 23:22:39'!
test05canNotCheckoutWrongNumberCreditCard
	
	self 
		should: testsObjects createCashier 
		checkout: testsObjects createCartWithValidBook1 
		debitOn: testsObjects createWrongNumberCreditCard 
		through: testsObjects merchantProcessor 
		on: testsObjects currentMonthAnYear 
		failWith: [Cashier canNotCheckoutWrongNumberCreditCardErrorMessage]! !

!CheckOutTest methodsFor: 'testing' stamp: 'ddo 6/12/2022 23:22:39'!
test06canNotCheckoutATooLongNameCreditCard
	
	self should:testsObjects createCashier
		checkout: testsObjects createCartWithValidBook1 
		debitOn: testsObjects createTooLengthNameCreditCard 
		through: testsObjects merchantProcessor 
		on: testsObjects currentMonthAnYear
		failWith:[Cashier canNotCheckoutTooLongNameCreditCardErrorMessage]
! !

!CheckOutTest methodsFor: 'testing' stamp: 'ddo 6/12/2022 23:22:39'!
test07canNotCheckoutANameWithSpacesCreditCard
	
	self 
		should: testsObjects createCashier 
		checkout: testsObjects createCartWithValidBook1 
		debitOn: testsObjects createNameWithSpacesCreditCard 
		through: testsObjects merchantProcessor 
		on:testsObjects currentMonthAnYear
		failWith:[Cashier canNotCheckoutNameWithSpacesCreditCardErrorMessage]! !

!CheckOutTest methodsFor: 'testing' stamp: 'ddo 6/12/2022 23:22:39'!
test09canNotCheckoutStolenCreditCardAndSellIsNotRegistred
	
	self 
		should: testsObjects createCashier 
		checkout: testsObjects createCartWithValidBook1 
		debitOn: testsObjects createStolenCreditCard 
		through: testsObjects merchantProcessor 
		on: testsObjects currentMonthAnYear
		failWith:[	testsObjects merchantProcessor class canNotSellWithStolenCreditCardErrorMessage ]! !

!CheckOutTest methodsFor: 'testing' stamp: 'ddo 6/12/2022 23:22:39'!
test10canNotCheckoutNotFundsCreditCardAndSellIsNotRegistred
	
	self 
		should: testsObjects createCashier 
		checkout: testsObjects createCartWithValidBook1 
		debitOn: testsObjects createNotFundsCreditCard 
		through: testsObjects merchantProcessor 
		on: testsObjects currentMonthAnYear
		failWith:[	testsObjects merchantProcessor class canNotSellNotFundsCreditCardErrorMessage]! !


!CheckOutTest methodsFor: 'setUp' stamp: 'DDO 6/6/2022 19:12:52'!
setUp

	testsObjects := TestsObjects new.! !


!CheckOutTest methodsFor: 'assertions' stamp: 'DDO 6/9/2022 02:13:49'!
assertPrice: aBlockPrice equalsCashier: aCashier checkout: aCart debitOn: aCreditCard through: merchantProcessor on:aMonth

	self 
		assert:  aBlockPrice value
		equals: (aCashier checkout: aCart debitOn: aCreditCard through: merchantProcessor on: aMonth).
		
	self assert: aCart ticket equals: aCashier sellsBook first! !

!CheckOutTest methodsFor: 'assertions' stamp: 'DDO 6/9/2022 01:32:32'!
should:aCashier checkout: aCart debitOn: aCreditCard through: merchantProcessor on:aMonth failWith: anErrorMessageBlock
		
		self should:[aCashier checkout:aCart  debitOn: aCreditCard through: merchantProcessor on: aMonth]
		raise:  	Error - MessageNotUnderstood 
		withExceptionDo: [:anError|
			self assert: anErrorMessageBlock value equals: anError messageText.
			self assert: aCashier sellsBook isEmpty]! !


!classDefinition: #FacadeTest category: 'TusLibros'!
TestCase subclass: #FacadeTest
	instanceVariableNames: 'testsObjects clock'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!FacadeTest methodsFor: 'testing' stamp: 'ddo 6/13/2022 16:07:54'!
should: aFacade failOn: aBlock WithError: anErrorMessageBlock

	self
		should: [aBlock value: aFacade ]
		raise:  Error - MessageNotUnderstood 
		withExceptionDo: [:anError|
			self assert: anErrorMessageBlock value equals: anError messageText]! !

!FacadeTest methodsFor: 'testing' stamp: 'ddo 6/13/2022 16:17:51'!
shouldntFail: aBlock
	
	self
	shouldnt: aBlock
	raise: 	Error- MessageNotUnderstood 
	

	! !

!FacadeTest methodsFor: 'testing' stamp: 'ddo 6/13/2022 16:13:22'!
test01canNotCreateACartWithAnInvalidUserName

	self
	should: testsObjects createDefaultFacade
	failOn:[:aFacade| aFacade createACartWith: testsObjects invalidUser key: testsObjects validPass]
	WithError: [Facade invalidCredentialsErrorMessage]
	! !

!FacadeTest methodsFor: 'testing' stamp: 'ddo 6/13/2022 16:16:17'!
test02canNotCreatACartWithAnInvalidPassword

	self 
	should: testsObjects createDefaultFacade
	failOn: [:aFacade| aFacade createACartWith: testsObjects validUser key: testsObjects invalidPass]
	WithError: [Facade invalidCredentialsErrorMessage]! !

!FacadeTest methodsFor: 'testing' stamp: 'ddo 6/13/2022 16:19:43'!
test03createAnEmptyCartWithValidCredentials

	
	self
		shouldntFail:[testsObjects createDefaultFacade 
					createACartWith: testsObjects validUser 	 key: testsObjects validPass] 
	

	! !

!FacadeTest methodsFor: 'testing' stamp: 'ddo 6/13/2022 16:21:03'!
test04canAddItemsToCreatedCart

	|facade aValidCartID |
	
	facade := testsObjects createDefaultFacade.
	aValidCartID := facade createACartWith: testsObjects validUser key: testsObjects validPass.
	
	self
		shouldntFail:[ facade add: 4 of: 'validBook1' toCart: aValidCartID]

	! !

!FacadeTest methodsFor: 'testing' stamp: 'ddo 6/13/2022 16:23:24'!
test05canNotAddItemsToNotCreatedCart

	self
		should: testsObjects createDefaultFacade
		failOn:[:aFacade| aFacade add: 4 of: 'validBook1' toCart: testsObjects invalidCartId]
		WithError:[Facade invalidCartIDErrorDescription]	! !

!FacadeTest methodsFor: 'testing' stamp: 'ddo 6/13/2022 16:29:10'!
test06canNotAddItemsNotincludedInCatalog

	|facade validCartId |
	
	facade := testsObjects createDefaultFacade.
	validCartId:= facade createACartWith: testsObjects validUser key: testsObjects validPass.
	
	self
		should: facade
		failOn:[:aFacade| aFacade add: 4 of: 'invalidBook' toCart: validCartId]
		WithError:[Facade invalidItemErrorDescription]	

	! !

!FacadeTest methodsFor: 'testing' stamp: 'ddo 6/13/2022 14:48:54'!
test07canListCartWithMultiplyBooksIsOk

	|facade listCart cartId |
	
	facade := testsObjects createDefaultFacade.
	cartId := testsObjects createCartWith4ValidBook1And3ValidBook2: facade.
	listCart := facade listCartFor:cartId.

	self assert: 4 equals: (listCart occurrencesOf: 'validBook1').
	self assert: 3 equals: (listCart occurrencesOf: 'validBook2').

	! !

!FacadeTest methodsFor: 'testing' stamp: 'ddo 6/13/2022 16:31:23'!
test08canNotListCartForNotCreatedCart

	self 
		should: testsObjects createDefaultFacade 
		failOn:[:aFacade| aFacade listCartFor: testsObjects invalidCartId] 
		WithError: [Facade invalidCartIDErrorDescription]! !

!FacadeTest methodsFor: 'testing' stamp: 'ddo 6/13/2022 16:34:44'!
test09canCheckOutValidCartIdAndValidCreditCard

	|facade cartId aCreditCardHolder |
	
	facade := testsObjects createDefaultFacade.
	cartId := testsObjects createCartWith4ValidBook1And3ValidBook2: facade.
	aCreditCardHolder := 'Pepe'. 
	
	self shouldntFail: [
			facade checkOut: cartId  
			from: 1234567891234569 
			expiredOn: testsObjects nextMonth 
			holder:aCreditCardHolder]! !

!FacadeTest methodsFor: 'testing' stamp: 'ddo 6/13/2022 16:37:12'!
test10canNotCheckOutInvalidCartId

	self 
	should: testsObjects createDefaultFacade 
	failOn: [:aFacade| aFacade checkOut: testsObjects invalidCartId 
			from: 1234567891234569 expiredOn: testsObjects nextMonth holder:'Pepe']
	WithError: [Facade invalidCartIDErrorDescription] ! !

!FacadeTest methodsFor: 'testing' stamp: 'ddo 6/13/2022 16:39:53'!
test11canNotCheckOutEmptyCart

	|facade cartId |
	
	facade := testsObjects createDefaultFacade.
	cartId := facade createACartWith: testsObjects validUser key: testsObjects validPass.
	
	self should: facade
	failOn: [:aFacade| aFacade checkOut: cartId 
			from: 1234567891234569 expiredOn: testsObjects nextMonth holder:'Pepe']
	WithError: [Facade canNotCheckOutEmptyCartErrorMessage]! !

!FacadeTest methodsFor: 'testing' stamp: 'ddo 6/13/2022 13:20:41'!
test12canListPurchasesOfClientNotBuyYet

	|facade listPurchases |
	
	facade := testsObjects createDefaultFacade.

	listPurchases := facade listPurchasesOf: testsObjects validUser withPass: testsObjects validPass.
	
	self assert: listPurchases isEmpty 
	! !

!FacadeTest methodsFor: 'testing' stamp: 'ddo 6/13/2022 16:43:50'!
test13canNotListPurchasesWithInvalidClientId

	self should: testsObjects createDefaultFacade 
		failOn: [:aFacade| aFacade listPurchasesOf: testsObjects invalidUser withPass: testsObjects validPass] 
		WithError: [Facade invalidCredentialsErrorMessage]
	
	! !

!FacadeTest methodsFor: 'testing' stamp: 'ddo 6/13/2022 16:42:53'!
test14canNotListPurchasesWithInvalidPass

	self should: testsObjects createDefaultFacade 
		failOn: [:aFacade| aFacade listPurchasesOf: testsObjects validUser withPass: testsObjects invalidPass] 
		WithError: [Facade invalidCredentialsErrorMessage]
	
	! !

!FacadeTest methodsFor: 'testing' stamp: 'ddo 6/13/2022 16:47:22'!
test15canNotCheckoutWithExpiredCreditCard

	self should: testsObjects createDefaultFacade 
		failOn: [:aFacade| aFacade 
			checkOut: (testsObjects createCartWith4ValidBook1And3ValidBook2: aFacade)
			from: 1234567891234569 
			expiredOn: testsObjects previousMonth 
			holder: 'Pepe' ]
		WithError: [Facade canNotCheckoutExpiredCreditCardErrorMessage]
	
	! !

!FacadeTest methodsFor: 'testing' stamp: 'ddo 6/13/2022 16:49:02'!
test16canNotAddItemsToCartWithSessionExpired

	|facade cartId |
	
	facade := testsObjects createDefaultFacade.
	cartId := facade createACartWith: testsObjects validUser key: testsObjects validPass.
	testsObjects advance31minutes.
	
	self should: facade
		failOn: [:aFacade| aFacade add: 2 of: 'validBook1' toCart: cartId] 
		WithError: [Facade sessionExpiredErrorMessage]
	
	! !

!FacadeTest methodsFor: 'testing' stamp: 'ddo 6/13/2022 16:51:42'!
test17canNotListCartWithSessionExpired

	|facade cartId|
	
	facade := testsObjects createDefaultFacade.
	cartId:= facade createACartWith: testsObjects validUser key: testsObjects validPass.
	testsObjects advance31minutes.
	
	self should:facade
		failOn: [:aFacade| aFacade listCartFor: cartId] 
		WithError: [Facade sessionExpiredErrorMessage]
	
	! !

!FacadeTest methodsFor: 'testing' stamp: 'ddo 6/13/2022 16:53:53'!
test18canNotCheckoutCartWithSessionExpired

	|facade cartId |
	
	facade := testsObjects createDefaultFacade.
	cartId:= testsObjects createCartWith4ValidBook1And3ValidBook2: facade.
	testsObjects advance31minutes.
	
	self should: facade
		failOn: [:aFacade| aFacade checkOut: cartId from: 1234567891234569 expiredOn: testsObjects nextMonth holder:'Pepe' ] 
		WithError: [Facade sessionExpiredErrorMessage]
	
	! !


!FacadeTest methodsFor: 'setUp' stamp: 'ddo 6/13/2022 00:11:18'!
setUp

	testsObjects := TestsObjects new.
	clock := ClockTest now: DateAndTime now.! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'catalog items'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidItemErrorMessage
	
	^'Item is not in catalog'! !

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidQuantityErrorMessage
	
	^'Invalid number of items'! !


!Cart methodsFor: 'assertions' stamp: 'DDO 6/6/2022 19:49:00'!
assertIsValidItem: anItem

	(catalog includesKey: anItem) ifFalse: [ self error: self invalidItemErrorMessage ]! !

!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 17:51'!
assertIsValidQuantity: aQuantity

	aQuantity strictlyPositive ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 17:48'!
initializeAcceptingItemsOf: aCatalog

	catalog := aCatalog.
	items := OrderedCollection new.! !


!Cart methodsFor: 'queries' stamp: 'juan 6/12/2022 16:19:39'!
items
	
	^items copy.! !

!Cart methodsFor: 'queries' stamp: 'HernanWilkinson 6/17/2013 17:45'!
occurrencesOf: anItem

	^items occurrencesOf: anItem  ! !

!Cart methodsFor: 'queries' stamp: 'DDO 6/8/2022 12:12:49'!
ticket
	
	^Array with: items copy with: self total.! !

!Cart methodsFor: 'queries' stamp: 'DDO 6/6/2022 21:17:16'!
total
	
	^items sum:[:item | (catalog at: item) * (self occurrencesOf: item)]! !


!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
includes: anItem

	^items includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
isEmpty
	
	^items isEmpty ! !


!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:44'!
add: anItem

	^ self add: 1 of: anItem ! !

!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:51'!
add: aQuantity of: anItem

	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.

	1 to: aQuantity do: [ :aNumber | items add: anItem ]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 17:48'!
acceptingItemsOf: aCatalog

	^self new initializeAcceptingItemsOf: aCatalog ! !


!classDefinition: #Cashier category: 'TusLibros'!
Object subclass: #Cashier
	instanceVariableNames: 'cart creditCard sellsBook'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'signals' stamp: 'j 6/7/2022 19:03:47'!
signalCanNotCheckoutAWrongNumberCreditCard

	^self error: self class canNotCheckoutWrongNumberCreditCardErrorMessage .! !

!Cashier methodsFor: 'signals' stamp: 'DDO 6/6/2022 18:56:14'!
signalCanNotCheckoutAnEmptyCart

	^self error: self class canNotCheckoutEmptyCartErrorMessage.! !

!Cashier methodsFor: 'signals' stamp: 'juan 6/7/2022 14:23:20'!
signalCanNotCheckoutAnExpiredCreditCard

	^self error: self class canNotCheckoutExpiredCreditCardErrorMessage.! !

!Cashier methodsFor: 'signals' stamp: 'DDO 6/7/2022 20:04:48'!
signalCanNotCheckoutNameWithSpacesCreditCard

	^self error: self class canNotCheckoutNameWithSpacesCreditCardErrorMessage! !

!Cashier methodsFor: 'signals' stamp: 'DDO 6/8/2022 23:10:15'!
signalCanNotCheckoutTooLongNameCreditCard

	^self error: self class canNotCheckoutTooLongNameCreditCardErrorMessage! !


!Cashier methodsFor: 'accesing' stamp: 'DDO 6/8/2022 21:10:19'!
sellsBook
	^ sellsBook copy! !


!Cashier methodsFor: 'assertions' stamp: 'DDO 6/7/2022 23:25:40'!
assertCreditCard: aCreditCard isNotExpiredOn: aDate

	^ (aCreditCard isExpiredOn: aDate) ifTrue:[self signalCanNotCheckoutAnExpiredCreditCard]! !

!Cashier methodsFor: 'assertions' stamp: 'DDO 6/7/2022 23:19:22'!
assertIsNotAnEmptyCart: aCart

	^ aCart isEmpty ifTrue:[self signalCanNotCheckoutAnEmptyCart]! !

!Cashier methodsFor: 'assertions' stamp: 'DDO 6/7/2022 23:23:12'!
assertNameCreditCardNotIncludesSpaces: aCreditCard

	^ (aCreditCard name includesSubString: ' ') ifTrue:[ self signalCanNotCheckoutNameWithSpacesCreditCard]! !

!Cashier methodsFor: 'assertions' stamp: 'DDO 6/8/2022 23:09:28'!
assertNameLengthCreditCardIsOk: aCreditCard

	^ (aCreditCard name size > 30) ifTrue:[ self signalCanNotCheckoutTooLongNameCreditCard]! !

!Cashier methodsFor: 'assertions' stamp: 'DDO 6/7/2022 23:21:02'!
assertNumberLengthCreditCardIsOk: aCreditCard

	^ (aCreditCard number decimalDigitLength = 16) ifFalse:[self signalCanNotCheckoutAWrongNumberCreditCard]! !


!Cashier methodsFor: 'checkout' stamp: 'DDO 6/9/2022 10:06:00'!
checkout: aCart debitOn: aCreditCard through: merchantProcessor on: aMonth
	
	self assertIsNotAnEmptyCart: aCart.
	self assertNumberLengthCreditCardIsOk: aCreditCard.
	self assertNameLengthCreditCardIsOk: aCreditCard.
	self assertNameCreditCardNotIncludesSpaces: aCreditCard.	
	self assertCreditCard: aCreditCard isNotExpiredOn: aMonth .
	
	merchantProcessor debitAnAmount: aCart total from: aCreditCard.
	sellsBook add: aCart ticket.
	^ aCart total.! !


!Cashier methodsFor: 'instance creation' stamp: 'DDO 6/8/2022 21:10:19'!
initialize

	sellsBook:= OrderedCollection new! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: 'TusLibros'!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'error-messege' stamp: 'DDO 6/6/2022 18:57:02'!
canNotCheckoutEmptyCartErrorMessage

	^'Can not checkout an empty Cart'! !

!Cashier class methodsFor: 'error-messege' stamp: 'juan 6/7/2022 14:22:31'!
canNotCheckoutExpiredCreditCardErrorMessage

	^'Can not checkout an expired credit card'! !

!Cashier class methodsFor: 'error-messege' stamp: 'DDO 6/7/2022 20:02:37'!
canNotCheckoutNameWithSpacesCreditCardErrorMessage
	
	^'credit card name has spaces'! !

!Cashier class methodsFor: 'error-messege' stamp: 'DDO 6/8/2022 23:10:15'!
canNotCheckoutTooLongNameCreditCardErrorMessage
	
	^'credit card has a too length name'! !

!Cashier class methodsFor: 'error-messege' stamp: 'j 6/7/2022 19:02:30'!
canNotCheckoutWrongNumberCreditCardErrorMessage
	
	^'credit card has a wrong number'! !


!classDefinition: #ClockTest category: 'TusLibros'!
Object subclass: #ClockTest
	instanceVariableNames: 'clock'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!ClockTest methodsFor: 'intance creation' stamp: 'ddo 6/13/2022 01:35:06'!
advance: aDuration

	^clock := clock + aDuration ! !

!ClockTest methodsFor: 'intance creation' stamp: 'ddo 6/13/2022 00:14:53'!
initializeNow: aTime	

	clock:= aTime! !

!ClockTest methodsFor: 'intance creation' stamp: 'ddo 6/13/2022 01:19:29'!
now

	^clock! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'ClockTest class' category: 'TusLibros'!
ClockTest class
	instanceVariableNames: 'clock'!

!ClockTest class methodsFor: 'instance creation' stamp: 'ddo 6/13/2022 00:14:13'!
now: aTime

	^self new  initializeNow: aTime! !


!classDefinition: #CreditCard category: 'TusLibros'!
Object subclass: #CreditCard
	instanceVariableNames: 'name number date'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'instance creation' stamp: 'DDO 6/8/2022 21:52:32'!
initializeHolder: aName number: aCreditCardNumber expiredOn: aDate 

	name := aName.
	number := aCreditCardNumber.
	date := aDate.! !


!CreditCard methodsFor: 'queries' stamp: 'DDO 6/7/2022 17:18:09'!
isExpiredOn: aDate

	^date < aDate
	! !

!CreditCard methodsFor: 'queries' stamp: 'DDO 6/7/2022 23:08:34'!
name

	^name copy! !

!CreditCard methodsFor: 'queries' stamp: 'DDO 6/8/2022 21:52:32'!
number

	^number copy! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: 'TusLibros'!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'instance creation' stamp: 'DDO 6/6/2022 21:44:39'!
holder: aName number: aCreditCardNumber expiredOn: aDate 
	
	^self new initializeHolder: aName number: aCreditCardNumber expiredOn: aDate ! !


!classDefinition: #Facade category: 'TusLibros'!
Object subclass: #Facade
	instanceVariableNames: 'clientDataBase catalog carts id listPurchases cashier merchantProcessor currentMonthAndYear sessions clock'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Facade methodsFor: 'cart' stamp: 'ddo 6/13/2022 15:35:27'!
add: anAmountToBuy of: aBook toCart: aCartID
	
	self assertIsValidBook: aBook .
	self assertCartIdIsValid: aCartID.
	(self sessionStartedAt:(sessions at: aCartID) isExpiredAt: clock now) ifTrue:[self signalSessionExpired].
	
	(carts at: aCartID) add: anAmountToBuy of: aBook ! !

!Facade methodsFor: 'cart' stamp: 'ddo 6/13/2022 01:13:51'!
assertIsValidBook:aBook

	(catalog includesKey: aBook) ifFalse:[self signalCanNotAddInvalidBook]! !

!Facade methodsFor: 'cart' stamp: 'ddo 6/13/2022 15:40:35'!
createACartWith: aClientId key: aPassword

	| cartID |
	(clientDataBase includesKey: aClientId) ifFalse: [self signalInvalidCredentials].
	((clientDataBase at: aClientId) = aPassword) ifFalse: [self signalInvalidCredentials].
	
	cartID := self generateCartId.
	carts at: cartID put:(Cart acceptingItemsOf: catalog).
	sessions at: cartID put: DateAndTime now.
	
	^cartID
	
	
	
	
	! !

!Facade methodsFor: 'cart' stamp: 'ddo 6/12/2022 23:23:02'!
currentMonthAnYear

	^currentMonthAndYear
	! !

!Facade methodsFor: 'cart' stamp: 'DDO 6/12/2022 14:29:55'!
generateCartId
	

	id := id + 1.
	^id
	
	
	! !

!Facade methodsFor: 'cart' stamp: 'ddo 6/13/2022 14:03:15'!
listCartFor: aCartId 
	
	|cart|
	
	(carts includesKey: aCartId) ifFalse:[self signalInvalidCartID].
	(self sessionStartedAt:(sessions at: aCartId) isExpiredAt: clock now) ifTrue:[self signalSessionExpired].
	cart := carts at:aCartId.
	^cart items. ! !

!Facade methodsFor: 'cart' stamp: 'DDO 6/12/2022 18:10:39'!
purchasesRegistered

	^	Dictionary newFromPairs: {
		22222 . OrderedCollection new.
		33333 . OrderedCollection new.
		44444 . OrderedCollection new.
		55555 . OrderedCollection new.
		}
	! !

!Facade methodsFor: 'cart' stamp: 'ddo 6/13/2022 01:58:07'!
sessionStartedAt: aTime isExpiredAt: aCurrentTime
	
	^(aTime + (Duration minutes:30)) < aCurrentTime
! !


!Facade methodsFor: 'signals' stamp: 'juan 6/12/2022 16:03:18'!
signalCanNotAddInvalidBook

	self error: self class invalidItemErrorDescription! !

!Facade methodsFor: 'signals' stamp: 'ddo 6/12/2022 22:45:19'!
signalCanNotCheckOutAnEmptyCart

	self error: self class canNotCheckOutEmptyCartErrorMessage! !

!Facade methodsFor: 'signals' stamp: 'DDO 6/12/2022 14:34:53'!
signalInvalidCartID

	self error: self class invalidCartIDErrorDescription! !

!Facade methodsFor: 'signals' stamp: 'DDO 6/12/2022 18:20:05'!
signalInvalidCredentials

	self error: self class invalidCredentialsErrorMessage.! !

!Facade methodsFor: 'signals' stamp: 'ddo 6/13/2022 14:05:03'!
signalSessionExpired

	self error: self class sessionExpiredErrorMessage ! !


!Facade methodsFor: 'check-out' stamp: 'ddo 6/13/2022 15:58:23'!
checkOut: aCartId from: aCreditCardNumber expiredOn: aDate holder: aCreditCardHolder 
	
	| cart creditCard |
	
	self assertCartIdIsValid: aCartId.
	self assertCartIsNotEmpty: aCartId.
	(self sessionStartedAt:(sessions at: aCartId) isExpiredAt: clock now) ifTrue:[self signalSessionExpired].
	
	cart := carts at:aCartId.
	creditCard := CreditCard holder: aCreditCardHolder number: aCreditCardNumber expiredOn: aDate. 
	cashier checkout: cart debitOn: creditCard through: merchantProcessor on: currentMonthAndYear 
	! !


!Facade methodsFor: 'listPurchased' stamp: 'DDO 6/12/2022 18:27:10'!
listPurchasesOf: aClientId withPass: aPass
	
	(clientDataBase includesKey: aClientId) ifFalse:[self signalInvalidCredentials ].
	((clientDataBase at: aClientId) = aPass) ifFalse:[self signalInvalidCredentials ].
	^listPurchases at: aClientId 
	! !


!Facade methodsFor: 'intance creation' stamp: 'ddo 6/13/2022 00:56:28'!
initializeWithUsers: aRegister and: aCatalog debitingThrought: aMerchantProcessor measuringTimeWith: aClock  

	clientDataBase := aRegister.
	catalog := aCatalog.
	carts := Dictionary new.
	sessions := Dictionary new.
	clock := aClock.
	id := 0.
	listPurchases := self purchasesRegistered.
	cashier := Cashier new.
	merchantProcessor := aMerchantProcessor.
	currentMonthAndYear := GregorianMonthOfYear current.
	! !


!Facade methodsFor: 'assertions' stamp: 'ddo 6/12/2022 22:38:59'!
assertCartIdIsValid:aCartId

	(carts includesKey: aCartId ) ifFalse:[self signalInvalidCartID]! !

!Facade methodsFor: 'assertions' stamp: 'ddo 6/13/2022 15:39:19'!
assertCartIsNotEmpty: aCartId
	
	((carts at:aCartId) isEmpty) ifTrue:[self signalCanNotCheckOutAnEmptyCart]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Facade class' category: 'TusLibros'!
Facade class
	instanceVariableNames: ''!

!Facade class methodsFor: 'error message' stamp: 'ddo 6/12/2022 22:45:19'!
canNotCheckOutEmptyCartErrorMessage

	^'Can not checkout an empty cart'! !

!Facade class methodsFor: 'error message' stamp: 'ddo 6/12/2022 22:45:52'!
canNotCheckoutExpiredCreditCardErrorMessage
	
	^ Cashier canNotCheckoutExpiredCreditCardErrorMessage! !

!Facade class methodsFor: 'error message' stamp: 'DDO 6/12/2022 14:35:53'!
invalidCartIDErrorDescription

	^'Invalid cart ID'! !

!Facade class methodsFor: 'error message' stamp: 'DDO 6/12/2022 18:16:57'!
invalidCredentialsErrorMessage

	^'Invalid credentials'! !

!Facade class methodsFor: 'error message' stamp: 'juan 6/12/2022 16:00:05'!
invalidItemErrorDescription
	^'Book not included in catalog'! !

!Facade class methodsFor: 'error message' stamp: 'ddo 6/13/2022 14:05:03'!
sessionExpiredErrorMessage
	
	^'Session Expired'! !


!Facade class methodsFor: 'instance creation' stamp: 'ddo 6/13/2022 00:56:47'!
withUsers: aRegister and: aCatalog debitingThrought: aMerchantProcessor measuringTimeWith: aClock  

	^ self new initializeWithUsers: aRegister and: aCatalog debitingThrought: aMerchantProcessor measuringTimeWith: aClock! !


!classDefinition: #TestsObjects category: 'TusLibros'!
Object subclass: #TestsObjects
	instanceVariableNames: 'currentMonth stolenCreditCards fundsOfCreditCards clock'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TestsObjects methodsFor: 'credit card' stamp: 'DDO 6/8/2022 00:58:17'!
createExpiredCreditCard
	
	^CreditCard holder:'Juan_Perez' number:1111111111111111 expiredOn: self previousMonth! !

!TestsObjects methodsFor: 'credit card' stamp: 'DDO 6/8/2022 00:57:03'!
createNameWithSpacesCreditCard
	
	^CreditCard holder:'Juan Perez' number:1234567891234560 expiredOn: self nextMonth ! !

!TestsObjects methodsFor: 'credit card' stamp: 'DDO 6/9/2022 10:28:31'!
createNotFundsCreditCard
	
	^CreditCard holder:'Juan_Poor' number:1234567891234566 expiredOn: self nextMonth ! !

!TestsObjects methodsFor: 'credit card' stamp: 'DDO 6/8/2022 13:01:54'!
createStolenCreditCard
	
	^CreditCard holder:'Juan_Perez' number:1234567891234567 expiredOn: self nextMonth ! !

!TestsObjects methodsFor: 'credit card' stamp: 'DDO 6/9/2022 10:35:20'!
createTooLengthNameCreditCard
	
	^CreditCard holder:'JuaaaaaaaaaaaaaaaaaaaanPerzzzzzzz' number:1234567891234568 expiredOn: self nextMonth ! !

!TestsObjects methodsFor: 'credit card' stamp: 'DDO 6/9/2022 10:36:38'!
createValidCreditCard
	
	^CreditCard holder:'Juan_Perez' number:1234567891234569 expiredOn: self nextMonth ! !

!TestsObjects methodsFor: 'credit card' stamp: 'DDO 6/8/2022 00:55:51'!
createWrongNumberCreditCard
	
	^CreditCard holder:'Juan_Perez' number:123 expiredOn: self nextMonth ! !

!TestsObjects methodsFor: 'credit card' stamp: 'DDO 6/9/2022 10:41:54'!
creditCardFunds: aCreditCard

	^fundsOfCreditCards at: aCreditCard number asString! !


!TestsObjects methodsFor: 'date' stamp: 'ddo 6/12/2022 23:22:39'!
currentMonthAnYear

	^currentMonth! !

!TestsObjects methodsFor: 'date' stamp: 'DDO 6/8/2022 21:49:35'!
nextMonth

	^currentMonth next! !

!TestsObjects methodsFor: 'date' stamp: 'DDO 6/8/2022 21:49:47'!
previousMonth

	^currentMonth previous ! !


!TestsObjects methodsFor: 'merchant processor' stamp: 'DDO 6/9/2022 10:50:27'!
assertCreditCard: aCreditCard hasFundsForAmount: anAmountToCkeckout

	^((self creditCardFunds:aCreditCard) < anAmountToCkeckout) 
			ifTrue:[self error: self signalcanNotSellWithNotFundsCreditCard]
	! !

!TestsObjects methodsFor: 'merchant processor' stamp: 'DDO 6/9/2022 10:45:28'!
assertIsNotAnStolenCreditCard: aCreditCard

	^ (stolenCreditCards includes: aCreditCard number) ifTrue:[ self error: self signalcanNotSellWithStolenCreditCard]! !

!TestsObjects methodsFor: 'merchant processor' stamp: 'DDO 6/9/2022 10:37:36'!
dataBase

	^	{
		'1111111111111111' . 5555* peso.
		'1234567891234560' . 4400* peso.
		'1234567891234567' . 4000* peso.
		'1234567891234566' . 0* peso.
		'1234567891234567' . 3333*peso.
		'1234567891234568'. 8888*peso.
		'1234567891234569'. 9999*peso.
		}
	! !

!TestsObjects methodsFor: 'merchant processor' stamp: 'DDO 6/9/2022 10:51:14'!
debitAnAmount: anAmountToCheckout from: aCreditCard  
	
	
	self assertIsNotAnStolenCreditCard: aCreditCard.
	self assertCreditCard: aCreditCard hasFundsForAmount: 	anAmountToCheckout! !

!TestsObjects methodsFor: 'merchant processor' stamp: 'DDO 6/9/2022 09:57:43'!
merchantProcessor

	^self
	! !

!TestsObjects methodsFor: 'merchant processor' stamp: 'DDO 6/9/2022 10:19:27'!
signalcanNotSellWithNotFundsCreditCard
	
	self error: self class canNotSellNotFundsCreditCardErrorMessage ! !

!TestsObjects methodsFor: 'merchant processor' stamp: 'DDO 6/9/2022 10:04:00'!
signalcanNotSellWithStolenCreditCard
	
	self error: self class canNotSellWithStolenCreditCardErrorMessage! !


!TestsObjects methodsFor: 'cart' stamp: 'j 6/7/2022 18:39:55'!
catalogSellByTheStore
	
	^ {'validBook1' . 10* peso. 'validBook2'. 15*peso}! !

!TestsObjects methodsFor: 'cart' stamp: 'DDO 6/6/2022 19:02:33'!
createCart
	
	^Cart acceptingItemsOf: self defaultCatalog! !

!TestsObjects methodsFor: 'cart' stamp: 'DDO 6/9/2022 00:08:45'!
createCartWithValidBook1
	
	| aCart |
	aCart := self createCart.
	^aCart add: self item1SellByTheStore
	! !

!TestsObjects methodsFor: 'cart' stamp: 'DDO 6/9/2022 01:40:00'!
createCartWithValidBook1AndValidBook2
	
	| aCart |
	aCart := self createCart.
	aCart add: self item1SellByTheStore.
	^aCart add: self item2SellByTheStore.
	
	! !

!TestsObjects methodsFor: 'cart' stamp: 'j 6/7/2022 18:40:05'!
defaultCatalog
	
	^ Dictionary newFromPairs: self catalogSellByTheStore ! !

!TestsObjects methodsFor: 'cart' stamp: 'ddo 6/13/2022 13:09:27'!
invalidCartId
	
	^nil! !

!TestsObjects methodsFor: 'cart' stamp: 'DDO 6/9/2022 00:08:45'!
item1SellByTheStore
	
	^ 'validBook1'! !

!TestsObjects methodsFor: 'cart' stamp: 'DDO 6/9/2022 00:10:20'!
item2SellByTheStore
	
	^ 'validBook2'! !

!TestsObjects methodsFor: 'cart' stamp: 'DDO 6/6/2022 19:02:33'!
itemNotSellByTheStore
	
	^'invalidBook'! !

!TestsObjects methodsFor: 'cart' stamp: 'DDO 6/9/2022 00:11:15'!
validBook1PlusValidBook2Price
	
	^ 25*peso! !

!TestsObjects methodsFor: 'cart' stamp: 'DDO 6/9/2022 00:02:42'!
validBook1Price
	
	^ 10*peso! !


!TestsObjects methodsFor: 'instance creation' stamp: 'ddo 6/13/2022 12:32:40'!
initialize

	currentMonth := GregorianMonthOfYear current.
	stolenCreditCards := OrderedCollection with:1234567891234567.
	fundsOfCreditCards := Dictionary newFromPairs: self dataBase.
	clock := ClockTest now: (DateAndTime now).! !


!TestsObjects methodsFor: 'cashier' stamp: 'DDO 6/9/2022 02:23:19'!
createCashier

	^Cashier new! !


!TestsObjects methodsFor: 'facade' stamp: 'ddo 6/13/2022 14:50:55'!
createCartWith4ValidBook1And3ValidBook2: aFacade
	
	| cartId |
	cartId := aFacade createACartWith: self validUser key: self validPass.
	aFacade add: 4 of: 'validBook1' toCart: cartId.
	aFacade add: 3 of: 'validBook2' toCart: cartId.
	
	^cartId! !

!TestsObjects methodsFor: 'facade' stamp: 'ddo 6/13/2022 12:27:15'!
createDefaultFacade
	
	clock := ClockTest now:(DateAndTime now).
	^ Facade 
		withUsers: self 		userRegister 
		and: self defaultCatalog 
		debitingThrought: self merchantProcessor 
		measuringTimeWith: clock! !

!TestsObjects methodsFor: 'facade' stamp: 'ddo 6/13/2022 13:04:34'!
invalidPass

	^'00000'! !

!TestsObjects methodsFor: 'facade' stamp: 'ddo 6/13/2022 13:03:53'!
invalidUser

	^1111! !

!TestsObjects methodsFor: 'facade' stamp: 'juan 6/11/2022 17:16:59'!
userRegister

	^	Dictionary newFromPairs: {
		22222 . '12345'.
		33333 . '33333'.
		44444 . '44444'.
		55555 . '44444'.
		}
	! !

!TestsObjects methodsFor: 'facade' stamp: 'ddo 6/13/2022 13:02:54'!
validPass

	^'12345'! !

!TestsObjects methodsFor: 'facade' stamp: 'ddo 6/13/2022 13:02:31'!
validUser

	^22222! !


!TestsObjects methodsFor: 'clock' stamp: 'ddo 6/13/2022 12:27:47'!
advance31minutes.
	
	clock advance:(Duration minutes:31)! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TestsObjects class' category: 'TusLibros'!
TestsObjects class
	instanceVariableNames: ''!

!TestsObjects class methodsFor: 'message error' stamp: 'DDO 6/9/2022 10:04:21'!
canNotSellWithStolenCreditCardErrorMessage
	
	^'can not sell with stolen credit card'! !


!TestsObjects class methodsFor: 'error handling' stamp: 'DDO 6/9/2022 10:17:33'!
canNotSellNotFundsCreditCardErrorMessage
	
	^'can not sell not funds credit card'! !
