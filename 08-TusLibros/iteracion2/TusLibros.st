!classDefinition: #CartTest category: 'TusLibros'!
TestCase subclass: #CartTest
	instanceVariableNames: 'testsObjects'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CartTest methodsFor: 'tests' stamp: 'DDO 6/8/2022 01:10:07'!
test01NewCartsAreCreatedEmpty

	self assert: testsObjects createCart isEmpty! !

!CartTest methodsFor: 'tests' stamp: 'DDO 6/8/2022 01:10:36'!
test02CanNotAddItemsThatDoNotBelongToStore

	| cart |
	
	cart := testsObjects createCart.
	
	self 
		should: [ cart add: testsObjects itemNotSellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'DDO 6/9/2022 00:08:45'!
test03AfterAddingAnItemTheCartIsNotEmptyAnymore

	| cart |
	
	cart := testsObjects createCart.
	
	cart add: testsObjects item1SellByTheStore.
	self deny: cart isEmpty ! !

!CartTest methodsFor: 'tests' stamp: 'DDO 6/9/2022 00:08:45'!
test04CanNotAddNonPositiveNumberOfItems

	| cart |
	
	cart := testsObjects createCart.
	
	self 
		should: [cart add: 0 of: testsObjects item1SellByTheStore ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidQuantityErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'DDO 6/8/2022 01:11:37'!
test05CanNotAddMoreThanOneItemNotSellByTheStore

	| cart |
	
	cart := testsObjects createCart.
	
	self 
		should: [cart add: 2 of: testsObjects itemNotSellByTheStore  ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError |
			self assert: anError messageText = cart invalidItemErrorMessage.
			self assert: cart isEmpty ]! !

!CartTest methodsFor: 'tests' stamp: 'DDO 6/9/2022 00:08:45'!
test06CartRemembersAddedItems

	| cart |
	
	cart := testsObjects createCart.
	
	cart add: testsObjects item1SellByTheStore.
	self assert: (cart includes: testsObjects item1SellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'DDO 6/9/2022 00:08:45'!
test07CartDoesNotHoldNotAddedItems

	| cart |
	
	cart := testsObjects createCart.
	
	self deny: (cart includes: testsObjects item1SellByTheStore)! !

!CartTest methodsFor: 'tests' stamp: 'DDO 6/9/2022 00:08:45'!
test08CartRemembersTheNumberOfAddedItems

	| cart |
	
	cart := testsObjects createCart.
	
	cart add: 2 of: testsObjects item1SellByTheStore.
	self assert: (cart occurrencesOf: testsObjects item1SellByTheStore) = 2! !


!CartTest methodsFor: 'setUp' stamp: 'DDO 6/8/2022 21:39:25'!
setUp

	testsObjects := TestsObjects new.! !


!classDefinition: #CheckOutTest category: 'TusLibros'!
TestCase subclass: #CheckOutTest
	instanceVariableNames: 'testsObjects'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CheckOutTest methodsFor: 'testing' stamp: 'DDO 6/9/2022 09:58:03'!
test01canNotCheckoutAnEmptyCart
	
	self 
		should: testsObjects createCashier
		checkout: testsObjects createCart
		debitOn: testsObjects createValidCreditCard 
		through: testsObjects merchantProcessor 
		on: testsObjects currentMonth 
		failWith:[Cashier canNotCheckoutEmptyCartErrorMessage] 
! !

!CheckOutTest methodsFor: 'testing' stamp: 'DDO 6/9/2022 09:58:22'!
test02checkoutCartWithABook
	"fusionado con test 08"
	
	self
		assertPrice: [testsObjects validBook1Price] 
		equalsCashier: testsObjects createCashier
		checkout: testsObjects createCartWithValidBook1 
		debitOn:  testsObjects createValidCreditCard 
		through: testsObjects merchantProcessor 
		on: testsObjects currentMonth 
! !

!CheckOutTest methodsFor: 'testing' stamp: 'DDO 6/9/2022 09:58:40'!
test03checkoutCartWithMultiplyBooks
	
	self
		assertPrice: [testsObjects validBook1PlusValidBook2Price] 
		equalsCashier:  testsObjects createCashier 
		checkout:  testsObjects createCartWithValidBook1AndValidBook2 
		debitOn:  testsObjects createValidCreditCard 
		through: testsObjects merchantProcessor 
		on: testsObjects currentMonth ! !

!CheckOutTest methodsFor: 'testing' stamp: 'DDO 6/9/2022 09:58:56'!
test04canNotCheckoutCartWithAnExpiredCreditCard
	
	self 
		should: testsObjects createCashier 
		checkout: testsObjects createCartWithValidBook1 
		debitOn: testsObjects createExpiredCreditCard 
		through: testsObjects merchantProcessor 
		on: testsObjects currentMonth 
		failWith:[Cashier canNotCheckoutExpiredCreditCardErrorMessage] .
! !

!CheckOutTest methodsFor: 'testing' stamp: 'DDO 6/9/2022 09:59:13'!
test05canNotCheckoutWrongNumberCreditCard
	
	self 
		should: testsObjects createCashier 
		checkout: testsObjects createCartWithValidBook1 
		debitOn: testsObjects createWrongNumberCreditCard 
		through: testsObjects merchantProcessor 
		on: testsObjects currentMonth 
		failWith: [Cashier canNotCheckoutWrongNumberCreditCardErrorMessage]! !

!CheckOutTest methodsFor: 'testing' stamp: 'DDO 6/9/2022 09:59:30'!
test06canNotCheckoutATooLongNameCreditCard
	
	self should:testsObjects createCashier
		checkout: testsObjects createCartWithValidBook1 
		debitOn: testsObjects createTooLengthNameCreditCard 
		through: testsObjects merchantProcessor 
		on: testsObjects currentMonth
		failWith:[Cashier canNotCheckoutTooLongNameCreditCardErrorMessage]
! !

!CheckOutTest methodsFor: 'testing' stamp: 'DDO 6/9/2022 09:59:54'!
test07canNotCheckoutANameWithSpacesCreditCard
	
	self 
		should: testsObjects createCashier 
		checkout: testsObjects createCartWithValidBook1 
		debitOn: testsObjects createNameWithSpacesCreditCard 
		through: testsObjects merchantProcessor 
		on:testsObjects currentMonth
		failWith:[Cashier canNotCheckoutNameWithSpacesCreditCardErrorMessage]! !

!CheckOutTest methodsFor: 'testing' stamp: 'DDO 6/9/2022 10:13:34'!
test09canNotCheckoutStolenCreditCardAndSellIsNotRegistred
	
	self 
		should: testsObjects createCashier 
		checkout: testsObjects createCartWithValidBook1 
		debitOn: testsObjects createStolenCreditCard 
		through: testsObjects merchantProcessor 
		on: testsObjects currentMonth
		failWith:[	testsObjects merchantProcessor class canNotSellWithStolenCreditCardErrorMessage ]! !

!CheckOutTest methodsFor: 'testing' stamp: 'DDO 6/9/2022 10:18:35'!
test10canNotCheckoutNotFundsCreditCardAndSellIsNotRegistred
	
	self 
		should: testsObjects createCashier 
		checkout: testsObjects createCartWithValidBook1 
		debitOn: testsObjects createNotFundsCreditCard 
		through: testsObjects merchantProcessor 
		on: testsObjects currentMonth
		failWith:[	testsObjects merchantProcessor class canNotSellNotFundsCreditCardErrorMessage]! !


!CheckOutTest methodsFor: 'setUp' stamp: 'DDO 6/6/2022 19:12:52'!
setUp

	testsObjects := TestsObjects new.! !


!CheckOutTest methodsFor: 'assertions' stamp: 'DDO 6/9/2022 02:13:49'!
assertPrice: aBlockPrice equalsCashier: aCashier checkout: aCart debitOn: aCreditCard through: merchantProcessor on:aMonth

	self 
		assert:  aBlockPrice value
		equals: (aCashier checkout: aCart debitOn: aCreditCard through: merchantProcessor on: aMonth).
		
	self assert: aCart ticket equals: aCashier sellsBook first! !

!CheckOutTest methodsFor: 'assertions' stamp: 'DDO 6/9/2022 01:32:32'!
should:aCashier checkout: aCart debitOn: aCreditCard through: merchantProcessor on:aMonth failWith: anErrorMessageBlock
		
		self should:[aCashier checkout:aCart  debitOn: aCreditCard through: merchantProcessor on: aMonth]
		raise:  	Error - MessageNotUnderstood 
		withExceptionDo: [:anError|
			self assert: anErrorMessageBlock value equals: anError messageText.
			self assert: aCashier sellsBook isEmpty]! !


!classDefinition: #Cart category: 'TusLibros'!
Object subclass: #Cart
	instanceVariableNames: 'catalog items'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidItemErrorMessage
	
	^'Item is not in catalog'! !

!Cart methodsFor: 'error messages' stamp: 'HernanWilkinson 6/17/2013 17:45'!
invalidQuantityErrorMessage
	
	^'Invalid number of items'! !


!Cart methodsFor: 'assertions' stamp: 'DDO 6/6/2022 19:49:00'!
assertIsValidItem: anItem

	(catalog includesKey: anItem) ifFalse: [ self error: self invalidItemErrorMessage ]! !

!Cart methodsFor: 'assertions' stamp: 'HernanWilkinson 6/17/2013 17:51'!
assertIsValidQuantity: aQuantity

	aQuantity strictlyPositive ifFalse: [ self error: self invalidQuantityErrorMessage ]! !


!Cart methodsFor: 'initialization' stamp: 'HernanWilkinson 6/17/2013 17:48'!
initializeAcceptingItemsOf: aCatalog

	catalog := aCatalog.
	items := OrderedCollection new.! !


!Cart methodsFor: 'queries' stamp: 'HernanWilkinson 6/17/2013 17:45'!
occurrencesOf: anItem

	^items occurrencesOf: anItem  ! !

!Cart methodsFor: 'queries' stamp: 'DDO 6/8/2022 12:12:49'!
ticket
	
	^Array with: items copy with: self total.! !

!Cart methodsFor: 'queries' stamp: 'DDO 6/6/2022 21:17:16'!
total
	
	^items sum:[:item | (catalog at: item) * (self occurrencesOf: item)]! !


!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
includes: anItem

	^items includes: anItem ! !

!Cart methodsFor: 'testing' stamp: 'HernanWilkinson 6/17/2013 17:44'!
isEmpty
	
	^items isEmpty ! !


!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:44'!
add: anItem

	^ self add: 1 of: anItem ! !

!Cart methodsFor: 'adding' stamp: 'HernanWilkinson 6/17/2013 17:51'!
add: aQuantity of: anItem

	self assertIsValidQuantity: aQuantity.
	self assertIsValidItem: anItem.

	1 to: aQuantity do: [ :aNumber | items add: anItem ]! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cart class' category: 'TusLibros'!
Cart class
	instanceVariableNames: ''!

!Cart class methodsFor: 'instance creation' stamp: 'HernanWilkinson 6/17/2013 17:48'!
acceptingItemsOf: aCatalog

	^self new initializeAcceptingItemsOf: aCatalog ! !


!classDefinition: #Cashier category: 'TusLibros'!
Object subclass: #Cashier
	instanceVariableNames: 'cart creditCard sellsBook'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!Cashier methodsFor: 'signals' stamp: 'j 6/7/2022 19:03:47'!
signalCanNotCheckoutAWrongNumberCreditCard

	^self error: self class canNotCheckoutWrongNumberCreditCardErrorMessage .! !

!Cashier methodsFor: 'signals' stamp: 'DDO 6/6/2022 18:56:14'!
signalCanNotCheckoutAnEmptyCart

	^self error: self class canNotCheckoutEmptyCartErrorMessage.! !

!Cashier methodsFor: 'signals' stamp: 'juan 6/7/2022 14:23:20'!
signalCanNotCheckoutAnExpiredCreditCard

	^self error: self class canNotCheckoutExpiredCreditCardErrorMessage.! !

!Cashier methodsFor: 'signals' stamp: 'DDO 6/7/2022 20:04:48'!
signalCanNotCheckoutNameWithSpacesCreditCard

	^self error: self class canNotCheckoutNameWithSpacesCreditCardErrorMessage! !

!Cashier methodsFor: 'signals' stamp: 'DDO 6/8/2022 23:10:15'!
signalCanNotCheckoutTooLongNameCreditCard

	^self error: self class canNotCheckoutTooLongNameCreditCardErrorMessage! !


!Cashier methodsFor: 'accesing' stamp: 'DDO 6/8/2022 21:10:19'!
sellsBook
	^ sellsBook copy! !


!Cashier methodsFor: 'assertions' stamp: 'DDO 6/7/2022 23:25:40'!
assertCreditCard: aCreditCard isNotExpiredOn: aDate

	^ (aCreditCard isExpiredOn: aDate) ifTrue:[self signalCanNotCheckoutAnExpiredCreditCard]! !

!Cashier methodsFor: 'assertions' stamp: 'DDO 6/7/2022 23:19:22'!
assertIsNotAnEmptyCart: aCart

	^ aCart isEmpty ifTrue:[self signalCanNotCheckoutAnEmptyCart]! !

!Cashier methodsFor: 'assertions' stamp: 'DDO 6/7/2022 23:23:12'!
assertNameCreditCardNotIncludesSpaces: aCreditCard

	^ (aCreditCard name includesSubString: ' ') ifTrue:[ self signalCanNotCheckoutNameWithSpacesCreditCard]! !

!Cashier methodsFor: 'assertions' stamp: 'DDO 6/8/2022 23:09:28'!
assertNameLengthCreditCardIsOk: aCreditCard

	^ (aCreditCard name size > 30) ifTrue:[ self signalCanNotCheckoutTooLongNameCreditCard]! !

!Cashier methodsFor: 'assertions' stamp: 'DDO 6/7/2022 23:21:02'!
assertNumberLengthCreditCardIsOk: aCreditCard

	^ (aCreditCard number decimalDigitLength = 16) ifFalse:[self signalCanNotCheckoutAWrongNumberCreditCard]! !


!Cashier methodsFor: 'checkout' stamp: 'DDO 6/9/2022 10:06:00'!
checkout: aCart debitOn: aCreditCard through: merchantProcessor on: aMonth
	
	self assertIsNotAnEmptyCart: aCart.
	self assertNumberLengthCreditCardIsOk: aCreditCard.
	self assertNameLengthCreditCardIsOk: aCreditCard.
	self assertNameCreditCardNotIncludesSpaces: aCreditCard.	
	self assertCreditCard: aCreditCard isNotExpiredOn: aMonth .
	
	merchantProcessor debitAnAmount: aCart total from: aCreditCard.
	sellsBook add: aCart ticket.
	^ aCart total.! !


!Cashier methodsFor: 'instance creation' stamp: 'DDO 6/8/2022 21:10:19'!
initialize

	sellsBook:= OrderedCollection new! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Cashier class' category: 'TusLibros'!
Cashier class
	instanceVariableNames: ''!

!Cashier class methodsFor: 'error-messege' stamp: 'DDO 6/6/2022 18:57:02'!
canNotCheckoutEmptyCartErrorMessage

	^'Can not checkout an empty Cart'! !

!Cashier class methodsFor: 'error-messege' stamp: 'juan 6/7/2022 14:22:31'!
canNotCheckoutExpiredCreditCardErrorMessage

	^'Can not checkout an expired credit card'! !

!Cashier class methodsFor: 'error-messege' stamp: 'DDO 6/7/2022 20:02:37'!
canNotCheckoutNameWithSpacesCreditCardErrorMessage
	
	^'credit card name has spaces'! !

!Cashier class methodsFor: 'error-messege' stamp: 'DDO 6/8/2022 23:10:15'!
canNotCheckoutTooLongNameCreditCardErrorMessage
	
	^'credit card has a too length name'! !

!Cashier class methodsFor: 'error-messege' stamp: 'j 6/7/2022 19:02:30'!
canNotCheckoutWrongNumberCreditCardErrorMessage
	
	^'credit card has a wrong number'! !


!classDefinition: #CreditCard category: 'TusLibros'!
Object subclass: #CreditCard
	instanceVariableNames: 'name number date'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!CreditCard methodsFor: 'instance creation' stamp: 'DDO 6/8/2022 21:52:32'!
initializeHolder: aName number: aCreditCardNumber expiredOn: aDate 

	name := aName.
	number := aCreditCardNumber.
	date := aDate.! !


!CreditCard methodsFor: 'queries' stamp: 'DDO 6/7/2022 17:18:09'!
isExpiredOn: aDate

	^date < aDate
	! !

!CreditCard methodsFor: 'queries' stamp: 'DDO 6/7/2022 23:08:34'!
name

	^name copy! !

!CreditCard methodsFor: 'queries' stamp: 'DDO 6/8/2022 21:52:32'!
number

	^number copy! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'CreditCard class' category: 'TusLibros'!
CreditCard class
	instanceVariableNames: ''!

!CreditCard class methodsFor: 'instance creation' stamp: 'DDO 6/6/2022 21:44:39'!
holder: aName number: aCreditCardNumber expiredOn: aDate 
	
	^self new initializeHolder: aName number: aCreditCardNumber expiredOn: aDate ! !


!classDefinition: #TestsObjects category: 'TusLibros'!
Object subclass: #TestsObjects
	instanceVariableNames: 'currentMonth stolenCreditCards fundsOfCreditCards'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'TusLibros'!

!TestsObjects methodsFor: 'credit card' stamp: 'DDO 6/8/2022 00:58:17'!
createExpiredCreditCard
	
	^CreditCard holder:'Juan_Perez' number:1111111111111111 expiredOn: self previousMonth! !

!TestsObjects methodsFor: 'credit card' stamp: 'DDO 6/8/2022 00:57:03'!
createNameWithSpacesCreditCard
	
	^CreditCard holder:'Juan Perez' number:1234567891234560 expiredOn: self nextMonth ! !

!TestsObjects methodsFor: 'credit card' stamp: 'DDO 6/9/2022 10:28:31'!
createNotFundsCreditCard
	
	^CreditCard holder:'Juan_Poor' number:1234567891234566 expiredOn: self nextMonth ! !

!TestsObjects methodsFor: 'credit card' stamp: 'DDO 6/8/2022 13:01:54'!
createStolenCreditCard
	
	^CreditCard holder:'Juan_Perez' number:1234567891234567 expiredOn: self nextMonth ! !

!TestsObjects methodsFor: 'credit card' stamp: 'DDO 6/9/2022 10:35:20'!
createTooLengthNameCreditCard
	
	^CreditCard holder:'JuaaaaaaaaaaaaaaaaaaaanPerzzzzzzz' number:1234567891234568 expiredOn: self nextMonth ! !

!TestsObjects methodsFor: 'credit card' stamp: 'DDO 6/9/2022 10:36:38'!
createValidCreditCard
	
	^CreditCard holder:'Juan_Perez' number:1234567891234569 expiredOn: self nextMonth ! !

!TestsObjects methodsFor: 'credit card' stamp: 'DDO 6/8/2022 00:55:51'!
createWrongNumberCreditCard
	
	^CreditCard holder:'Juan_Perez' number:123 expiredOn: self nextMonth ! !

!TestsObjects methodsFor: 'credit card' stamp: 'DDO 6/9/2022 10:41:54'!
creditCardFunds: aCreditCard

	^fundsOfCreditCards at: aCreditCard number asString! !


!TestsObjects methodsFor: 'date' stamp: 'DDO 6/8/2022 21:49:22'!
currentMonth

	^currentMonth! !

!TestsObjects methodsFor: 'date' stamp: 'DDO 6/8/2022 21:49:35'!
nextMonth

	^currentMonth next! !

!TestsObjects methodsFor: 'date' stamp: 'DDO 6/8/2022 21:49:47'!
previousMonth

	^currentMonth previous ! !


!TestsObjects methodsFor: 'merchant processor' stamp: 'DDO 6/9/2022 10:50:27'!
assertCreditCard: aCreditCard hasFundsForAmount: anAmountToCkeckout

	^((self creditCardFunds:aCreditCard) < anAmountToCkeckout) 
			ifTrue:[self error: self signalcanNotSellWithNotFundsCreditCard]
	! !

!TestsObjects methodsFor: 'merchant processor' stamp: 'DDO 6/9/2022 10:45:28'!
assertIsNotAnStolenCreditCard: aCreditCard

	^ (stolenCreditCards includes: aCreditCard number) ifTrue:[ self error: self signalcanNotSellWithStolenCreditCard]! !

!TestsObjects methodsFor: 'merchant processor' stamp: 'DDO 6/9/2022 10:37:36'!
dataBase

	^	{
		'1111111111111111' . 5555* peso.
		'1234567891234560' . 4400* peso.
		'1234567891234567' . 4000* peso.
		'1234567891234566' . 0* peso.
		'1234567891234567' . 3333*peso.
		'1234567891234568'. 8888*peso.
		'1234567891234569'. 9999*peso.
		}
	! !

!TestsObjects methodsFor: 'merchant processor' stamp: 'DDO 6/9/2022 10:51:14'!
debitAnAmount: anAmountToCheckout from: aCreditCard  
	
	
	self assertIsNotAnStolenCreditCard: aCreditCard.
	self assertCreditCard: aCreditCard hasFundsForAmount: 	anAmountToCheckout! !

!TestsObjects methodsFor: 'merchant processor' stamp: 'DDO 6/9/2022 09:57:43'!
merchantProcessor

	^self
	! !

!TestsObjects methodsFor: 'merchant processor' stamp: 'DDO 6/9/2022 10:19:27'!
signalcanNotSellWithNotFundsCreditCard
	
	self error: self class canNotSellNotFundsCreditCardErrorMessage ! !

!TestsObjects methodsFor: 'merchant processor' stamp: 'DDO 6/9/2022 10:04:00'!
signalcanNotSellWithStolenCreditCard
	
	self error: self class canNotSellWithStolenCreditCardErrorMessage! !


!TestsObjects methodsFor: 'cart' stamp: 'j 6/7/2022 18:39:55'!
catalogSellByTheStore
	
	^ {'validBook1' . 10* peso. 'validBook2'. 15*peso}! !

!TestsObjects methodsFor: 'cart' stamp: 'DDO 6/6/2022 19:02:33'!
createCart
	
	^Cart acceptingItemsOf: self defaultCatalog! !

!TestsObjects methodsFor: 'cart' stamp: 'DDO 6/9/2022 00:08:45'!
createCartWithValidBook1
	
	| aCart |
	aCart := self createCart.
	^aCart add: self item1SellByTheStore
	! !

!TestsObjects methodsFor: 'cart' stamp: 'DDO 6/9/2022 01:40:00'!
createCartWithValidBook1AndValidBook2
	
	| aCart |
	aCart := self createCart.
	aCart add: self item1SellByTheStore.
	^aCart add: self item2SellByTheStore.
	
	! !

!TestsObjects methodsFor: 'cart' stamp: 'j 6/7/2022 18:40:05'!
defaultCatalog
	
	^ Dictionary newFromPairs: self catalogSellByTheStore ! !

!TestsObjects methodsFor: 'cart' stamp: 'DDO 6/9/2022 00:08:45'!
item1SellByTheStore
	
	^ 'validBook1'! !

!TestsObjects methodsFor: 'cart' stamp: 'DDO 6/9/2022 00:10:20'!
item2SellByTheStore
	
	^ 'validBook2'! !

!TestsObjects methodsFor: 'cart' stamp: 'DDO 6/6/2022 19:02:33'!
itemNotSellByTheStore
	
	^'invalidBook'! !

!TestsObjects methodsFor: 'cart' stamp: 'DDO 6/9/2022 00:11:15'!
validBook1PlusValidBook2Price
	
	^ 25*peso! !

!TestsObjects methodsFor: 'cart' stamp: 'DDO 6/9/2022 00:02:42'!
validBook1Price
	
	^ 10*peso! !


!TestsObjects methodsFor: 'instance creation' stamp: 'DDO 6/9/2022 10:30:28'!
initialize

	currentMonth := GregorianMonthOfYear current.
	stolenCreditCards := OrderedCollection with:1234567891234567.
	fundsOfCreditCards := Dictionary newFromPairs: self dataBase! !


!TestsObjects methodsFor: 'cashier' stamp: 'DDO 6/9/2022 02:23:19'!
createCashier

	^Cashier new! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'TestsObjects class' category: 'TusLibros'!
TestsObjects class
	instanceVariableNames: ''!

!TestsObjects class methodsFor: 'message error' stamp: 'DDO 6/9/2022 10:04:21'!
canNotSellWithStolenCreditCardErrorMessage
	
	^'can not sell with stolen credit card'! !


!TestsObjects class methodsFor: 'error handling' stamp: 'DDO 6/9/2022 10:17:33'!
canNotSellNotFundsCreditCardErrorMessage
	
	^'can not sell not funds credit card'! !
