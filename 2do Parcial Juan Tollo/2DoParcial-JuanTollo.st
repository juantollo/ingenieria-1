!classDefinition: #LaddersAndSlides3DTest category: '2DoParcial-JuanTollo'!
TestCase subclass: #LaddersAndSlides3DTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: '2DoParcial-JuanTollo'!

!LaddersAndSlides3DTest methodsFor: 'initial Position' stamp: 'juan 7/4/2022 21:41:37'!
test01StartingPositionIsIsOrigin
	
	|aMap finalFloor finalPosition game player1 player2 aStair aSlide ShortCutCollection|
	
	finalPosition:= 15@20.
	finalFloor:=  1.
	
	aStair := Stair from:2@2 floor:1 to:2@2 floor:2.
	
	aSlide := Slide from: 4@4 floor:2 to:3@2 floor:1.
	
	ShortCutCollection := OrderedCollection with: aStair with: aSlide.
	
	aMap:= OrderedCollection with:(Floor x:4 y:15 number:1 shortCuts: ShortCutCollection).

	player1 := Player new.

	player2 := Player new.
		
	game := Game finalPosition: finalPosition finalFloor: finalFloor map: aMap with: player1 with: player2.

	self assert: 1@1 equals: (game positionPlayer: player1).
	self assert: 1 equals: (game floorPlayer: player1).
	! !

!LaddersAndSlides3DTest methodsFor: 'initial Position' stamp: 'juan 7/4/2022 21:43:23'!
test02GameKnowsItsInitialAndFinalPosition

	|game player1 player2 finalPosition aMap finalFloor aStair aSlide ShortCutCollection|
	
	player1 := Player new.
	player2 := Player new.
	
	finalPosition:= 2@2.
	finalFloor:= 1.
	
	aStair := Stair from:2@2 floor:1 to:2@2 floor:2.
	
	aSlide := Slide from: 4@4 floor:2 to:3@2 floor:1.
	
	ShortCutCollection := OrderedCollection with: aStair with: aSlide.
	
	aMap:= OrderedCollection with:(Floor x:4 y:15 number:1 shortCuts: ShortCutCollection).
	game := Game finalPosition: finalPosition finalFloor: finalFloor map: aMap with: player1 with: player2.
		
	self assert: finalPosition equals: game finalPosition.
	self assert: finalFloor equals: game finalFloor.

! !


!LaddersAndSlides3DTest methodsFor: 'playing' stamp: 'juan 7/4/2022 21:40:59'!
test03GivenTwoDiceValuePlayerMovesOnMapToAValidPosition
	
	|aMap finalFloor finalPosition game player1 player2 aStair  aSlide ShortCutCollection|
	
	finalPosition:= 15@20.
	finalFloor:=  1.
	
	aStair := Stair from:2@2 floor:1 to:2@2 floor:2.
	
	aSlide := Slide from: 4@4 floor:2 to:3@2 floor:1.
	
	ShortCutCollection := OrderedCollection with: aStair with: aSlide.
	
	aMap:= OrderedCollection with:(Floor x:4 y:15 number:1 shortCuts: ShortCutCollection).

	player1 := Player new.

	player2 := Player new.
		
	game := Game finalPosition: finalPosition finalFloor: finalFloor map: aMap with: player1 with: player2.
.	
	game player: player1 diceX:7 diceY:7.
	
	self assert: (2@2) equals: (player1 isAt).
	self assert: (1) equals: (player1 floor).
	! !

!LaddersAndSlides3DTest methodsFor: 'playing' stamp: 'juan 7/4/2022 21:40:29'!
test04aPlayerCanNotGoToAnInvalidPosition	
	
	|aMap finalFloor finalPosition game player1 player2 aStair aSlide ShortCutCollection|
	
	finalPosition:= 15@20.
	finalFloor:=  1.
	
	finalFloor:=  1.
	
	aStair := Stair from:2@2 floor:1 to:2@2 floor:2.
	
	aSlide := Slide from: 4@4 floor:2 to:3@2 floor:1.
	
	ShortCutCollection := OrderedCollection with: aStair with: aSlide.
	
	aMap:= OrderedCollection with:(Floor x:10 y:15 number:1 shortCuts: ShortCutCollection).

	player1 := Player new.

	player2 := Player new.
		
	game := Game finalPosition: finalPosition finalFloor: finalFloor map: aMap with: player1 with: player2.
.	
	game player: player1 diceX:1 diceY:1.
	
	self assert: 1@1 equals: (game positionPlayer: player1)

	! !

!LaddersAndSlides3DTest methodsFor: 'playing' stamp: 'juan 7/4/2022 21:39:16'!
test05aPlayerCanNotGoToAnInvalidPositionPositive	
	
	|aMap finalFloor finalPosition game player1 player2 aStair aSlide ShortCutCollection| 
	
	finalPosition:= 15@20.
	finalFloor:=  1.
	
	aStair := Stair from:2@2 floor:1 to:2@2 floor:2.
	
	aSlide := Slide from: 4@4 floor:2 to:3@2 floor:1.
	
	ShortCutCollection := OrderedCollection with: aStair with: aSlide.
	
	aMap:= OrderedCollection with:(Floor x:4 y:15 number:1 shortCuts: ShortCutCollection).

	player1 := Player new.

	player2 := Player new.
		
	game := Game finalPosition: finalPosition finalFloor: finalFloor map: aMap with: player1 with: player2.
.	
	game player: player1 diceX:12 diceY:7.
	
	self assert: 1@1 equals: (game positionPlayer: player1)

	! !


!LaddersAndSlides3DTest methodsFor: 'short cuts' stamp: 'juan 7/4/2022 21:54:15'!
test06shortCutsTobogan


	|floor1 ShortCutCollection aStair aSlide floor2 floors player1 player2 game finalPosition finalFloor|
	
	aStair := Stair from:2@2 floor:1 to:2@2 floor:2.
	
	aSlide := Slide from: 4@4 floor:2 to:3@2 floor:1.
	
	ShortCutCollection := OrderedCollection with: aStair with: aSlide.
		
	floor1 := Floor x:15  y:15  number:1  shortCuts: ShortCutCollection.
	
	floor2 := Floor x: 15 y: 15 number: 2 shortCuts: ShortCutCollection.
	
	floors:= OrderedCollection with:floor1 with: floor2.
	
	player1 := Player new.

	player2 := Player new.
	
	finalPosition:= 15@20.
	
	finalFloor:=  1.
		
	game := Game finalPosition: finalPosition finalFloor: finalFloor map: floors with: player1 with: player2.
	
	game player: player1  diceX:7  diceY:7.
	
	self assert:  2@2 equals: (game positionPlayer: player1).
	self assert:  2 equals: (game floorPlayer: player1)

	
	

	
	! !


!classDefinition: #Floor category: '2DoParcial-JuanTollo'!
Object subclass: #Floor
	instanceVariableNames: 'floorNumber x y'
	classVariableNames: ''
	poolDictionaries: ''
	category: '2DoParcial-JuanTollo'!

!Floor methodsFor: 'category-name' stamp: 'juan 7/4/2022 20:02:33'!
floorNumber

	^floorNumber copy ! !

!Floor methodsFor: 'category-name' stamp: 'juan 7/4/2022 20:00:57'!
initializeX: cellNumbersX y: cellNumberY number: aFloorNumber 
	
	x := cellNumbersX.
	y := cellNumberY.
	floorNumber := aFloorNumber.! !

!Floor methodsFor: 'category-name' stamp: 'juan 7/4/2022 20:02:04'!
x

	^x copy! !

!Floor methodsFor: 'category-name' stamp: 'juan 7/4/2022 20:02:13'!
y

	^y copy! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Floor class' category: '2DoParcial-JuanTollo'!
Floor class
	instanceVariableNames: ''!

!Floor class methodsFor: 'instance creation' stamp: 'juan 7/4/2022 21:34:40'!
x: cellNumbersX y: cellNumberY number: aFloorNumber shortCuts: aShortCutsCollection  
	
	^self new initializeX: cellNumbersX y: cellNumberY number: aFloorNumber! !


!classDefinition: #Game category: '2DoParcial-JuanTollo'!
Object subclass: #Game
	instanceVariableNames: 'arrayPositionAndFloor player1 player2 finalPosition players map finalFloor'
	classVariableNames: ''
	poolDictionaries: ''
	category: '2DoParcial-JuanTollo'!

!Game methodsFor: 'initialization' stamp: 'juan 7/4/2022 19:13:26'!
initializeFinalPosition: anArrayPositionAndFloor finalFloor: aFinalFloor map:aMap with: aPlayer1 with: aPlayer2 
	
	finalFloor:= aFinalFloor.
	finalPosition := anArrayPositionAndFloor.
	players:= OrderedCollection with: aPlayer1 with: aPlayer2.
	map:= aMap.
! !

!Game methodsFor: 'initialization' stamp: 'juan 7/4/2022 21:09:39'!
player: aPlayer diceX:x diceY:y

	| thePlayer newTemptativePosition currentFloor firstCondition secondCondition|
	
	thePlayer := players detect:[:player | player = aPlayer].
	
	newTemptativePosition := thePlayer diceX:x diceY:y.
	
	currentFloor := map detect:[:floor | (floor floorNumber) = (thePlayer floor)].
	
	firstCondition := 	 (0 > (newTemptativePosition) x) or: [0> (newTemptativePosition) y].
	
	secondCondition := ((newTemptativePosition) x > (currentFloor x)) or: [(newTemptativePosition) y > (currentFloor y)].
	
	(firstCondition or: [secondCondition ]) ifFalse: [thePlayer position: newTemptativePosition] 

! !


!Game methodsFor: 'accessing' stamp: 'juan 7/4/2022 19:14:07'!
finalFloor
	
	^finalFloor copy.! !

!Game methodsFor: 'accessing' stamp: 'juan 7/4/2022 18:10:16'!
finalPosition
	
	^finalPosition copy.! !

!Game methodsFor: 'accessing' stamp: 'juan 7/4/2022 21:15:00'!
floorPlayer: aPlayer 
	
	| thePlayer |
	
	thePlayer := players detect:[:player | player= aPlayer].
	
	^thePlayer floor.! !

!Game methodsFor: 'accessing' stamp: 'juan 7/4/2022 19:46:21'!
positionPlayer: aPlayer 
	
	| thePlayer |
	
	thePlayer := players detect:[:player | player= aPlayer].
	
	^thePlayer isAt.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Game class' category: '2DoParcial-JuanTollo'!
Game class
	instanceVariableNames: ''!

!Game class methodsFor: 'instance creation' stamp: 'juan 7/4/2022 19:13:36'!
finalPosition: anArrayPositionAndFloor finalFloor: aFinalFloor map: aMap  with: aPlayer1 with: aPlayer2 
	
	^self new initializeFinalPosition: anArrayPositionAndFloor finalFloor: aFinalFloor map: aMap with: aPlayer1 with: aPlayer2 ! !


!classDefinition: #Player category: '2DoParcial-JuanTollo'!
Object subclass: #Player
	instanceVariableNames: 'position floor'
	classVariableNames: ''
	poolDictionaries: ''
	category: '2DoParcial-JuanTollo'!


!Player methodsFor: 'accessing' stamp: 'juan 7/4/2022 19:07:05'!
floor
	
	^ floor copy.! !

!Player methodsFor: 'accessing' stamp: 'juan 7/4/2022 18:48:58'!
isAt
	
	^ position value copy.! !


!Player methodsFor: 'playing' stamp: 'juan 7/4/2022 20:04:38'!
diceX: x diceY: y 
	
	^ (self isAt) + ((x-6)@(y-6))! !

!Player methodsFor: 'playing' stamp: 'juan 7/4/2022 20:22:20'!
position: aPosition 
	
	position := aPosition.! !

!Player methodsFor: 'playing' stamp: 'juan 7/4/2022 19:06:12'!
position: aPosition atFloor: aFloor 
	
	position := aPosition.
	floor:= aFloor.! !


!Player methodsFor: 'initialize' stamp: 'juan 7/4/2022 19:07:56'!
initialize

	position := 1@1.
	floor := 1.
	! !


!classDefinition: #ShortCut category: '2DoParcial-JuanTollo'!
Object subclass: #ShortCut
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: '2DoParcial-JuanTollo'!


!classDefinition: #Slide category: '2DoParcial-JuanTollo'!
ShortCut subclass: #Slide
	instanceVariableNames: 'initialposition initialFloor finalPosition finalFloor'
	classVariableNames: ''
	poolDictionaries: ''
	category: '2DoParcial-JuanTollo'!

!Slide methodsFor: 'initialization' stamp: 'juan 7/4/2022 21:45:59'!
initializeFrom: anInitialposition floor: anInitialFloor to: aFinalPosition floor: aFinalFloor 

	initialposition := anInitialposition.
	initialFloor := anInitialFloor.
	finalPosition := aFinalPosition.
	finalFloor := aFinalFloor.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Slide class' category: '2DoParcial-JuanTollo'!
Slide class
	instanceVariableNames: ''!

!Slide class methodsFor: 'instance creation' stamp: 'juan 7/4/2022 21:29:43'!
from: anInitialposition floor: anInitialFloor to: aFinalPosition floor: aFinalFloor 
	
	^self new initializeFrom: anInitialposition floor: anInitialFloor to: aFinalPosition floor: aFinalFloor ! !


!classDefinition: #Stair category: '2DoParcial-JuanTollo'!
ShortCut subclass: #Stair
	instanceVariableNames: 'initialposition initialFloor finalPosition finalFloor'
	classVariableNames: ''
	poolDictionaries: ''
	category: '2DoParcial-JuanTollo'!

!Stair methodsFor: 'initialize' stamp: 'juan 7/4/2022 21:32:32'!
initializeFrom: anInitialPosition floor: aninitialFloor to: aFinalposition floor: aFinalFloor 
	
	initialposition := anInitialPosition.
	initialFloor := aninitialFloor.
	finalPosition := aFinalposition.
	finalFloor := aFinalFloor.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Stair class' category: '2DoParcial-JuanTollo'!
Stair class
	instanceVariableNames: ''!

!Stair class methodsFor: 'instance creation' stamp: 'juan 7/4/2022 21:28:02'!
from: anInitialPosition floor: aninitialFloor to: aFinalposition floor: aFinalFloor 
	
	^self new initializeFrom: anInitialPosition floor: aninitialFloor to: aFinalposition floor: aFinalFloor ! !
