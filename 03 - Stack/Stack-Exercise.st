!classDefinition: #OOStackTest category: 'Stack-Exercise'!
TestCase subclass: #OOStackTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStackTest methodsFor: 'test' stamp: 'HernanWilkinson 5/7/2012 11:30'!
test01StackShouldBeEmptyWhenCreated

	| stack |
	
	stack := OOStack new.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:29:55'!
test02PushAddElementsToTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	
	self deny: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:01'!
test03PopRemovesElementsFromTheStack

	| stack |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self assert: stack isEmpty! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:09'!
test04PopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	stack push: pushedObject.
	
	self assert: stack pop = pushedObject! !

!OOStackTest methodsFor: 'test' stamp: 'NR 9/16/2021 17:40:17'!
test05StackBehavesLIFO

	| stack firstPushedObject secondPushedObject |
	
	stack := OOStack new.
	firstPushedObject := 'firstSomething'.
	secondPushedObject := 'secondSomething'.
	
	stack push: firstPushedObject.
	stack push: secondPushedObject.
	
	self assert: stack pop = secondPushedObject.
	self assert: stack pop = firstPushedObject.
	self assert: stack isEmpty 
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:20'!
test06TopReturnsLastPushedObject

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack top = pushedObject.
	! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:24'!
test07TopDoesNotRemoveObjectFromStack

	| stack pushedObject |
	
	stack := OOStack new.
	pushedObject := 'something'.
	
	stack push: pushedObject.
	
	self assert: stack size = 1.
	stack top.
	self assert: stack size = 1.
	! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:26'!
test08CanNotPopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'NR 5/13/2020 13:30:31'!
test09CanNotPopWhenThereAreNoObjectsInTheStackAndTheStackHadObjects

	| stack  |
	
	stack := OOStack new.
	stack push: 'something'.
	stack pop.
	
	self
		should: [ stack pop ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !

!OOStackTest methodsFor: 'test' stamp: 'HAW 4/14/2017 22:48:44'!
test10CanNotTopWhenThereAreNoObjectsInTheStack

	| stack  |
	
	stack := OOStack new.
	self
		should: [ stack top ]
		raise: Error - MessageNotUnderstood 
		withExceptionDo: [ :anError |
			self assert: anError messageText = OOStack stackEmptyErrorDescription ]
		
! !


!classDefinition: #SentenceFinderByPrefixTest category: 'Stack-Exercise'!
TestCase subclass: #SentenceFinderByPrefixTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefixTest methodsFor: 'as yet unclassified' stamp: 'j 4/27/2022 23:38:43'!
test01CaseSensitive

	| stack prefix|
	
	prefix:= 'Something'.
	stack := OOStack new.
	stack push: 'something'.
	stack push: 'sometimes if feel'.
	stack push: 'sommer times'.
	
	self asert: (SentenceFinderByPrefix InitializeWith: stack finding: prefix) = 'something'.
	
	! !

!SentenceFinderByPrefixTest methodsFor: 'as yet unclassified' stamp: 'j 4/28/2022 00:04:00'!
test02AnEmptyStackCantNotHaveAnyPrefix

| stack prefix|
	
	prefix:= ''.
	stack := OOStack new.
	
	! !


!classDefinition: #OOStack category: 'Stack-Exercise'!
Object subclass: #OOStack
	instanceVariableNames: 'elements'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!OOStack methodsFor: 'accessing' stamp: 'ddo 4/25/2022 20:10:58'!
isEmpty
	
	^ elements isEmpty.! !

!OOStack methodsFor: 'accessing' stamp: 'ddo 4/26/2022 22:44:51'!
pop
	| top |
	
	top := self top.
	elements removeLast.
	
	^ top! !

!OOStack methodsFor: 'accessing' stamp: 'ddo 4/26/2022 22:50:40'!
push: anElement
	
	elements add: anElement.! !

!OOStack methodsFor: 'accessing' stamp: 'ddo 4/25/2022 20:41:59'!
size
	^elements size.! !

!OOStack methodsFor: 'accessing' stamp: 'ddo 4/27/2022 00:06:32'!
status

	^StackStatus statusFor: self.! !

!OOStack methodsFor: 'accessing' stamp: 'ddo 4/27/2022 00:03:41'!
top
	
	^self status top.! !

!OOStack methodsFor: 'accessing' stamp: 'ddo 4/26/2022 22:46:43'!
topFromANotEmptyStack

	^elements last.! !

!OOStack methodsFor: 'accessing' stamp: 'ddo 4/26/2022 22:47:08'!
topFromAnEmptyStack

	^self error: self class stackEmptyErrorDescription! !


!OOStack methodsFor: 'instance creation' stamp: 'ddo 4/26/2022 22:42:25'!
initialize

	elements := OrderedCollection new.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'OOStack class' category: 'Stack-Exercise'!
OOStack class
	instanceVariableNames: ''!

!OOStack class methodsFor: 'error descriptions' stamp: 'NR 9/16/2021 17:39:43'!
stackEmptyErrorDescription
	
	^ 'stack is empty!!!!!!'! !


!classDefinition: #SentenceFinderByPrefix category: 'Stack-Exercise'!
Object subclass: #SentenceFinderByPrefix
	instanceVariableNames: 'orderedCollection this stackAux stack prefixToFind sentencesWithThatPrefix'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!SentenceFinderByPrefix methodsFor: 'as yet unclassified' stamp: 'j 4/27/2022 23:42:04'!
InitializeWith: aStack finding: aPrefix
	
	sentencesWithThatPrefix := OrderedCollection new.
	stackAux := OrderedCollection new.
	stack := aStack.
	prefixToFind := aPrefix.! !

!SentenceFinderByPrefix methodsFor: 'as yet unclassified' stamp: 'j 4/27/2022 23:43:54'!
sentenceFinder
	
	|sentence|
	
	 [stack size > 0] whileTrue:[
		sentence := stack pop.
		stackAux push:	sentence. 
		[sentence beginsWith: prefixToFind] ifTrue: [sentencesWithThatPrefix push: sentence]].

	stack := stackAux.! !


!classDefinition: #StackStatus category: 'Stack-Exercise'!
Object subclass: #StackStatus
	instanceVariableNames: 'stack'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!StackStatus methodsFor: 'as yet unclassified' stamp: 'ddo 4/26/2022 23:57:07'!
initializeFor: aStack

	stack := aStack.! !

!StackStatus methodsFor: 'as yet unclassified' stamp: 'ddo 4/27/2022 00:24:31'!
top

	^self subclassResponsibility.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'StackStatus class' category: 'Stack-Exercise'!
StackStatus class
	instanceVariableNames: ''!

!StackStatus class methodsFor: 'as yet unclassified' stamp: 'ddo 4/26/2022 23:11:28'!
canHandle: anStack

	self subclassResponsibility ! !

!StackStatus class methodsFor: 'as yet unclassified' stamp: 'ddo 4/27/2022 00:17:34'!
for: aStack

	^self new initializeFor: aStack.! !

!StackStatus class methodsFor: 'as yet unclassified' stamp: 'ddo 4/27/2022 00:06:49'!
statusFor: aStack
	
	^(StackStatus allSubclasses detect:[:anStatus | anStatus canHandle: aStack]) for: aStack.! !


!StackStatus class methodsFor: 'instance creation' stamp: 'ddo 4/27/2022 00:16:04'!
initializeFor: anStack 

	^self new InitializeFor: anStack ! !


!classDefinition: #Empty category: 'Stack-Exercise'!
StackStatus subclass: #Empty
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!Empty methodsFor: 'as yet unclassified' stamp: 'ddo 4/26/2022 23:59:23'!
top

	 ^ stack topFromAnEmptyStack.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Empty class' category: 'Stack-Exercise'!
Empty class
	instanceVariableNames: ''!

!Empty class methodsFor: 'as yet unclassified' stamp: 'ddo 4/26/2022 23:12:37'!
canHandle: anStack

	^anStack size = 0.! !


!classDefinition: #NotEmpty category: 'Stack-Exercise'!
StackStatus subclass: #NotEmpty
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Stack-Exercise'!

!NotEmpty methodsFor: 'as yet unclassified' stamp: 'ddo 4/27/2022 00:24:46'!
top

	^stack topFromANotEmptyStack.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'NotEmpty class' category: 'Stack-Exercise'!
NotEmpty class
	instanceVariableNames: ''!

!NotEmpty class methodsFor: 'as yet unclassified' stamp: 'ddo 4/26/2022 23:14:55'!
canHandle: anStack

	^anStack size > 0.! !
