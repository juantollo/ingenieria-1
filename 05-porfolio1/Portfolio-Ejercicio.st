!classDefinition: #ReceptiveAccountTest category: 'Portfolio-Ejercicio'!
TestCase subclass: #ReceptiveAccountTest
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccountTest methodsFor: 'portfolio tests' stamp: 'ddo 5/23/2022 16:32:05'!
accountWithA100Deposit
	
	|account|
	
	account := self anEmptyAccount.
	
	Deposit register: 100 on: account.
	
	^account! !

!ReceptiveAccountTest methodsFor: 'portfolio tests' stamp: 'ddo 5/23/2022 16:32:05'!
anEmptyAccount

	^ ReceptiveAccount new! !

!ReceptiveAccountTest methodsFor: 'portfolio tests' stamp: 'Juan 5/21/2022 19:01:02'!
anEmptyPortfolio

	^ PortfolioAccount new! !

!ReceptiveAccountTest methodsFor: 'portfolio tests' stamp: 'ddo 5/23/2022 16:40:57'!
raiseErrorCanNotAddAnAccountWhen: aBlock

	self
		should: aBlock 
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError | self assert: anError messageText = PortfolioAccount canNotAddAnAccountErrorDescription ]
	
	! !

!ReceptiveAccountTest methodsFor: 'portfolio tests' stamp: 'j 5/18/2022 12:13:33'!
test08PortfolioHasZeroBalanceWhenCreated

	|porfolio|
	
	porfolio := self anEmptyPortfolio.
	
	self assert: porfolio balance equals: 0.! !

!ReceptiveAccountTest methodsFor: 'portfolio tests' stamp: 'ddo 5/23/2022 16:32:05'!
test09PortfolioCanIncludeAccounts

	|portfolio account|
	
	portfolio := self anEmptyPortfolio.
	
	account := self anEmptyAccount.
	
	portfolio add: account.
	
	self assert: portfolio includes: account. ! !

!ReceptiveAccountTest methodsFor: 'portfolio tests' stamp: 'j 5/18/2022 12:29:52'!
test10PortfolioBalanceEqualsToItsAccountsBalance

	|portfolio aNonEmptyAccount |

	portfolio := self anEmptyPortfolio.
		
	aNonEmptyAccount := self accountWithA100Deposit.
	
	portfolio add: aNonEmptyAccount.

	self assert: portfolio balance equals: aNonEmptyAccount balance
	
! !

!ReceptiveAccountTest methodsFor: 'portfolio tests' stamp: 'ddo 5/23/2022 16:42:00'!
test11PortfolioCanNotAddAnOwnAccount

	|portfolio account|
	
	portfolio := self anEmptyPortfolio.
	
	account := self anEmptyAccount.
	
	portfolio add: account.
	
	self raiseErrorCanNotAddAnAccountWhen: [ portfolio add: account ]

	"
	self
		should: [ portfolio add: account ]
		raise: Error - MessageNotUnderstood
		withExceptionDo: [ :anError | self assert: anError messageText = PortfolioAccount canNotAddAnAccountErrorDescription ]
	"
	! !

!ReceptiveAccountTest methodsFor: 'portfolio tests' stamp: 'DDO 5/22/2022 23:18:23'!
test12EmptyPortfolioHasAnyTransaction

	|portfolio deposit |
	
	portfolio := self anEmptyPortfolio.
	deposit := Deposit for:100.
	
	self deny: (portfolio hasRegistered: deposit)
	! !

!ReceptiveAccountTest methodsFor: 'portfolio tests' stamp: 'DDO 5/22/2022 23:20:02'!
test12bisEmptyPortfolioHasAnyTransaction

	|portfolio|
	
	portfolio := self anEmptyPortfolio.

	self assert: (portfolio transactions isEmpty)
	! !

!ReceptiveAccountTest methodsFor: 'portfolio tests' stamp: 'ddo 5/23/2022 16:32:05'!
test13PortfolioTransactionsAreItsAccountsTransactions

	|portfolio account1 deposit |
	
	portfolio := self anEmptyPortfolio.
	
	account1 := self anEmptyAccount.
	
	deposit := Deposit register: 50 on: account1.
		
	portfolio add: account1.
	 			
	self assert: (portfolio hasRegistered: deposit).

	! !

!ReceptiveAccountTest methodsFor: 'portfolio tests' stamp: 'ddo 5/23/2022 16:32:05'!
test13bisPortfolioTransactionsAreItsAccountsTransactions

	|portfolio account1 deposit |
	
	portfolio := self anEmptyPortfolio.
	
	account1 := self anEmptyAccount.
	
	deposit := Deposit register: 50 on: account1.
		
	portfolio add: account1.
	 			
	self assert: (portfolio transactions includes:deposit).

	! !

!ReceptiveAccountTest methodsFor: 'portfolio tests' stamp: 'Juan 5/21/2022 17:56:31'!
test14PortfolioCanAddPortfolios

	|portfolio portfolio2|
	
	portfolio := self anEmptyPortfolio.
	
 	portfolio2 := self anEmptyPortfolio.
	
	portfolio add: portfolio2.
		
	self assert: portfolio includes: portfolio2

	! !

!ReceptiveAccountTest methodsFor: 'portfolio tests' stamp: 'ddo 5/23/2022 16:43:09'!
test15APortfolioCanNotAddItSelf

	|portfolio|
	
	portfolio := self anEmptyPortfolio.
	
	self raiseErrorCanNotAddAnAccountWhen: [ portfolio add: portfolio ].
	! !

!ReceptiveAccountTest methodsFor: 'portfolio tests' stamp: 'ddo 5/23/2022 16:43:44'!
test16APortfolioWithAnAccountCanNotAddAnotherPortfolioWithSameAccount

	|portfolio1 portfolio2 account|
	
	portfolio1 := self anEmptyPortfolio.
	
	account := self accountWithA100Deposit.
	
	portfolio1 add: account.
	
	portfolio2 := self anEmptyPortfolio.
	
	portfolio2 add: account.
	
	self raiseErrorCanNotAddAnAccountWhen: [ portfolio1 add: portfolio2 ]
	! !

!ReceptiveAccountTest methodsFor: 'portfolio tests' stamp: 'ddo 5/23/2022 16:44:19'!
test17APortfolioCanNotAddAccountWhichIsAParentAccount

	|portfolio1 portfolio2 account|
	
	portfolio1 := self anEmptyPortfolio.
		
	portfolio2 := self anEmptyPortfolio.
	
	account := self accountWithA100Deposit..
	
	portfolio1  add: account.
	
	portfolio1 add: portfolio2.
	
	self raiseErrorCanNotAddAnAccountWhen: [portfolio2 add: account ]
	! !

!ReceptiveAccountTest methodsFor: 'portfolio tests' stamp: 'ddo 5/23/2022 16:44:43'!
test18APortfolioCanNotAddAccountWhichIsAmongItsParentAccount

	|portfolio1 portfolio2 portfolio3 account|
	
	portfolio1 := self anEmptyPortfolio.
		
	portfolio2 := self anEmptyPortfolio.
		
	portfolio3 := self anEmptyPortfolio.
	
	account := self accountWithA100Deposit..
	
	portfolio1  add: account.
	
	portfolio1 add: portfolio3.

	portfolio2 add: portfolio3.

	self raiseErrorCanNotAddAnAccountWhen: [portfolio3 add: account ]
	! !


!ReceptiveAccountTest methodsFor: 'receptive account tests' stamp: 'ddo 5/23/2022 16:32:05'!
test01ReceptiveAccountHaveZeroAsBalanceWhenCreated 

	| account |
	
	account := self anEmptyAccount.

	self assert: 0 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'receptive account tests' stamp: 'NR 11/2/2020 17:13:48'!
test02DepositIncreasesBalanceOnTransactionValue 

	| account |
	
	account := ReceptiveAccount  new.
	Deposit register: 100 on: account.
		
	self assert: 100 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'receptive account tests' stamp: 'ddo 5/23/2022 16:32:05'!
test03WithdrawDecreasesBalanceOnTransactionValue 

	| account |
	
	account := self anEmptyAccount.
	Deposit register: 100 on: account.
	Withdraw register: 50 on: account.
		
	self assert: 50 equals: account balance.
! !

!ReceptiveAccountTest methodsFor: 'receptive account tests' stamp: 'ddo 5/23/2022 16:32:05'!
test04WithdrawValueMustBePositive 

	| account withdrawValue |
	
	account := self anEmptyAccount.
	withdrawValue := 50.
	
	self assert: withdrawValue equals: (Withdraw register: withdrawValue on: account) value
! !

!ReceptiveAccountTest methodsFor: 'receptive account tests' stamp: 'ddo 5/23/2022 16:32:05'!
test05ReceptiveAccountKnowsRegisteredTransactions 

	| account deposit withdraw |
	
	account := self anEmptyAccount.
	deposit := Deposit register: 100 on: account.
	withdraw := Withdraw register: 50 on: account.
		
	self assert: (account hasRegistered: deposit).
	self assert: (account hasRegistered: withdraw).
! !

!ReceptiveAccountTest methodsFor: 'receptive account tests' stamp: 'ddo 5/23/2022 16:32:05'!
test06ReceptiveAccountDoNotKnowNotRegisteredTransactions

	| deposit withdraw account |
	
	account := self anEmptyAccount.
	deposit :=  Deposit for: 200.
	withdraw := Withdraw for: 50.
	
	self deny: (account hasRegistered: deposit).
	self deny: (account hasRegistered:withdraw).
! !

!ReceptiveAccountTest methodsFor: 'receptive account tests' stamp: 'ddo 5/23/2022 16:32:05'!
test07AccountKnowsItsTransactions 

	| account1 deposit1 |
	
	account1 := self anEmptyAccount.
	
	deposit1 := Deposit register: 50 on: account1.
		
	self assert: 1 equals: account1 transactions size.
	self assert: (account1 transactions includes: deposit1).
! !


!classDefinition: #Account category: 'Portfolio-Ejercicio'!
Object subclass: #Account
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!


!classDefinition: #PortfolioAccount category: 'Portfolio-Ejercicio'!
Account subclass: #PortfolioAccount
	instanceVariableNames: 'accounts parents'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!


!PortfolioAccount methodsFor: 'accessing' stamp: 'DDO 5/22/2022 20:41:10'!
add: anAccountToAdd
	
	self assertCanAdd: anAccountToAdd.

	accounts add: anAccountToAdd.
	anAccountToAdd sonOf: self! !

!PortfolioAccount methodsFor: 'accessing' stamp: 'DDO 5/22/2022 22:54:25'!
anyRootParentsIncludes: accountToAdd

	^self rootParents anySatisfy: [ :aParent | aParent includes: accountToAdd]
! !

!PortfolioAccount methodsFor: 'accessing' stamp: 'DDO 5/22/2022 22:56:57'!
assertCanAdd: anAccountToAdd

	^(self anyRootParentsIncludes: anAccountToAdd) ifTrue:[self signalCanNotAddAnAccount]! !

!PortfolioAccount methodsFor: 'accessing' stamp: 'DDO 5/22/2022 20:41:10'!
balance

	^accounts sum: [ :anAccount | anAccount balance ] ifEmpty: [ 0 ]! !

!PortfolioAccount methodsFor: 'accessing' stamp: 'DDO 5/22/2022 20:41:10'!
hasRegistered: aDeposit 
	
	^accounts anySatisfy: [:anAccount| anAccount hasRegistered: aDeposit]! !

!PortfolioAccount methodsFor: 'accessing' stamp: 'DDO 5/22/2022 20:41:53'!
includes: anAccount
	
	^ self = anAccount or: [ 
		accounts anySatisfy: [ :internalAccount | (internalAccount includes: anAccount) 
											or: [ anAccount includes: internalAccount ]]]

! !

!PortfolioAccount methodsFor: 'accessing' stamp: 'DDO 5/22/2022 22:45:58'!
injectRootParents: rootParents

	
	(self isRoot)
		ifTrue: [ rootParents add: self ] 
		ifFalse: [ parents do: [ :aParent | aParent injectRootParents: rootParents ]]! !

!PortfolioAccount methodsFor: 'accessing' stamp: 'DDO 5/22/2022 22:46:36'!
isRoot

	^parents isEmpty! !

!PortfolioAccount methodsFor: 'accessing' stamp: 'DDO 5/22/2022 22:41:25'!
rootParents
	
	| rootParents |
	
	rootParents := Set new.
	self injectRootParents: rootParents.
	^ rootParents! !

!PortfolioAccount methodsFor: 'accessing' stamp: 'DDO 5/22/2022 22:49:56'!
sonOf: anAccount
	
	parents add: anAccount ! !

!PortfolioAccount methodsFor: 'accessing' stamp: 'DDO 5/22/2022 23:27:55'!
transactions

	^accounts 
		inject: OrderedCollection new
		into: [ :currentTransactions :anAccount | currentTransactions addAll:(anAccount transactions) ]! !


!PortfolioAccount methodsFor: 'initializing' stamp: 'DDO 5/22/2022 21:31:01'!
initialize

	accounts := OrderedCollection new.
	parents := OrderedCollection new.! !


!PortfolioAccount methodsFor: 'signals' stamp: 'DDO 5/22/2022 23:05:47'!
signalCanNotAddAnAccount

	^ self 	error: self class canNotAddAnAccountErrorDescription! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'PortfolioAccount class' category: 'Portfolio-Ejercicio'!
PortfolioAccount class
	instanceVariableNames: ''!

!PortfolioAccount class methodsFor: 'errors' stamp: 'DDO 5/22/2022 23:08:02'!
canNotAddAnAccountErrorDescription
	^'can Not Add An Account'! !


!classDefinition: #ReceptiveAccount category: 'Portfolio-Ejercicio'!
Account subclass: #ReceptiveAccount
	instanceVariableNames: 'transactions parent'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!ReceptiveAccount methodsFor: 'initialization' stamp: 'NR 10/17/2019 15:06:56'!
initialize

	transactions := OrderedCollection new.! !


!ReceptiveAccount methodsFor: 'transactions' stamp: 'Juan 5/21/2022 18:02:23'!
includes: anAccount

	^self = anAccount

	! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
register: aTransaction

	transactions add: aTransaction 
! !

!ReceptiveAccount methodsFor: 'transactions' stamp: 'HernanWilkinson 7/13/2011 18:37'!
transactions 

	^ transactions copy! !


!ReceptiveAccount methodsFor: 'balance' stamp: 'ddo 5/16/2022 20:09:57'!
balance

	^transactions sum: [ :aTransaction | aTransaction addToBalance ] ifEmpty: [ 0 ]! !


!ReceptiveAccount methodsFor: 'testing' stamp: 'NR 10/17/2019 03:28:43'!
hasRegistered: aTransaction

	^ transactions includes: aTransaction 
! !


!ReceptiveAccount methodsFor: 'accessing' stamp: 'DDO 5/22/2022 23:00:38'!
sonOf: aPortfolioAccount ! !


!classDefinition: #AccountTransaction category: 'Portfolio-Ejercicio'!
Object subclass: #AccountTransaction
	instanceVariableNames: ''
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!AccountTransaction methodsFor: 'value' stamp: 'HernanWilkinson 9/12/2011 12:25'!
value 

	self subclassResponsibility ! !


!AccountTransaction methodsFor: 'balance' stamp: 'ddo 5/16/2022 20:13:12'!
addToBalance

	self subclassResponsibility ! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'AccountTransaction class' category: 'Portfolio-Ejercicio'!
AccountTransaction class
	instanceVariableNames: ''!

!AccountTransaction class methodsFor: 'instance creation' stamp: 'NR 10/17/2019 03:22:00'!
register: aValue on: account

	| transaction |
	
	transaction := self for: aValue.
	account register: transaction.
		
	^ transaction! !


!classDefinition: #Deposit category: 'Portfolio-Ejercicio'!
AccountTransaction subclass: #Deposit
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Deposit methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:45'!
initializeFor: aValue

	value := aValue ! !


!Deposit methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:38'!
value

	^ value! !


!Deposit methodsFor: 'balance' stamp: 'ddo 5/16/2022 20:10:56'!
addToBalance
	^self value.! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Deposit class' category: 'Portfolio-Ejercicio'!
Deposit class
	instanceVariableNames: ''!

!Deposit class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:38'!
for: aValue

	^ self new initializeFor: aValue ! !


!classDefinition: #Withdraw category: 'Portfolio-Ejercicio'!
AccountTransaction subclass: #Withdraw
	instanceVariableNames: 'value'
	classVariableNames: ''
	poolDictionaries: ''
	category: 'Portfolio-Ejercicio'!

!Withdraw methodsFor: 'initialization' stamp: 'HernanWilkinson 7/13/2011 18:46'!
initializeFor: aValue

	value := aValue ! !


!Withdraw methodsFor: 'value' stamp: 'HernanWilkinson 7/13/2011 18:33'!
value

	^ value! !


!Withdraw methodsFor: 'balance' stamp: 'ddo 5/16/2022 20:12:00'!
addToBalance
	^self value *(-1)! !

"-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- "!

!classDefinition: 'Withdraw class' category: 'Portfolio-Ejercicio'!
Withdraw class
	instanceVariableNames: ''!

!Withdraw class methodsFor: 'instance creation' stamp: 'HernanWilkinson 7/13/2011 18:33'!
for: aValue

	^ self new initializeFor: aValue ! !
